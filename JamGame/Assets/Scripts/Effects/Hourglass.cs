﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SimpleAnimator))]
public class Hourglass : MonoBehaviour {

	private SimpleAnimator animator;

	void Awake () {
		animator = GetComponent<SimpleAnimator>();
		GameController.Instance.timeLoopController.OnStartLoop += delegate {
			animator.PlayAnimation("Hourglass");
		};
	}
}
