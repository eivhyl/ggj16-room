﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer))]
public class BloodPool : MonoBehaviour {
	public List<Sprite> sprites;
	private SpriteRenderer spriteRenderer;

	void Awake () {
		spriteRenderer = GetComponent<SpriteRenderer>();
		spriteRenderer.sprite = sprites.Random();
	}

	void Update () {
	
	}
}
