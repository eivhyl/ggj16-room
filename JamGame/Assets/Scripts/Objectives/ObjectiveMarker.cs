﻿using UnityEngine;
using System.Collections;

public class ObjectiveMarker : MonoBehaviour {
	public Transform target;
	public AnimationCurve bobCurve;

	void Start () {
	
	}

	void Update () {
		if(target == null) {
			Destroy(gameObject);
			return;
		}
		transform.position = target.position + new Vector3(0, target.localScale.y * 0.5f, 0) + new Vector3(0, 3, 0);
	}
}

