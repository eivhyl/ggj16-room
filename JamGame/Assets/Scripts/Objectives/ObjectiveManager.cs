﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Objectives;
using System.Collections.Generic;

public class ObjectiveManager : MonoBehaviour {

    public static List<KeyValuePair<Objective,int>> AllowedObjectives;

	public static Objective GetObjective(CharacterType characterType, TimeLoop timeLoop)
    {
		switch (characterType)
		{
		    case CharacterType.Thief:
				ReachDestinationObjective objective = new ReachDestinationObjective(timeLoop, "Diamond");
		        objective.description = "STEAL THE DIAMOND!";
		        return objective;
		    case CharacterType.Baby:
		        return new KillCharacterTypeObjective(timeLoop, new List<CharacterType>() { CharacterType.Cthulu, CharacterType.Businessman, CharacterType.Knight, CharacterType.Occultist, CharacterType.Sheep}, 1);
		    case CharacterType.Knight:
		        return new KillCharacterTypeObjective(timeLoop, new List<CharacterType>() {CharacterType.Occultist, CharacterType.Cthulu}, 1);
		    case CharacterType.Businessman:
		        return new KillCharacterTypeObjective(timeLoop, new List<CharacterType>() {EnumX.Random<CharacterType>()}, 1);
		    case CharacterType.Cthulu:
		        return new KillCharacterTypeObjective(timeLoop, new List<CharacterType>() {EnumX.Random<CharacterType>()}, 1);
		}
		Debug.LogWarning("Default objective!");
		return new KillCharacterTypeObjective(timeLoop, new List<CharacterType>() {EnumX.Random<CharacterType>()}, 1);
    }

    //This is going to generate return a K/V pair
//    private static void GenerateAllowedList(CharacterType characterType, int id)
//    {
//        switch (characterType)
//        {
//            case CharacterType.Knight:
//                Objective newObjective = new KillX(2,id);
//                int bias = 1;
//                KeyValuePair<Objective, int> name = new KeyValuePair<Objective, int>(newObjective, bias);
//                AllowedObjectives.Add(name);
//                break;
//            case CharacterType.Baby:
//                AllowedObjectives.AddMany<Objective>();
//                break;
//            case CharacterType.Businessman:
//                AllowedObjectives.AddMany<Objective>();
//                break;
//            case CharacterType.Cthulu:
//                AllowedObjectives.AddMany<Objective>();
//                break
//        }
//    }
}
