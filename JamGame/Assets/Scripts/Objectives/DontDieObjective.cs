﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Objectives
{
    public class DontDieObjective : Objective {
		public override string description { 
			get {
				return "STAY ALIVE";
			} set {
				
			}
		}
        public DontDieObjective(TimeLoop timeLoop) : base(timeLoop)
        {
            this.timeLoop = timeLoop;
            GameController.Instance.timeLoopController.OnCompleteLoop += OnCompleteLoop;
        }

        void OnCompleteLoop(TimeLoop dontuse)
        {
            if (timeLoop.characterPrefab.alive)
                ProgressOnGoal();
        }
    }
}
