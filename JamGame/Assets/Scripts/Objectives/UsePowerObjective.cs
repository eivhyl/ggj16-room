﻿using UnityEngine;
using System.Collections;

public class UsePowerObjective : Objective {
    public UsePowerObjective(TimeLoop timeLoop) : base(timeLoop)
    {

    }

    public UsePowerObjective(TimeLoop timeLoop, int goal) : base(timeLoop, goal)
    {

    }

    public override string description { get; set; }
}
