using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class KillCharacterTypeObjective : Objective {

	public override string description {
		get {
			string typesString = "KILL "+goal+" ";
			for(int i = 0; i < types.Count; i++) {
				typesString += types[i].ToString();
				if(i != types.Count-1) {
					typesString += "/";
				}
			}
			typesString += goal > 1 ? "s" : "";
			return typesString.ToUpper();
		} set {}
    }

	private List<CharacterType> types;

	private List<BaseCharacter> targetCharacters {
		get {
			return GameController.Instance.characters.Where(x => x != timeLoop.characterController.character && types.Contains(x.type)).ToList();
		}
	}

		public KillCharacterTypeObjective(TimeLoop timeLoop, List<CharacterType> types, int goal) : base(timeLoop, goal)
        {
			this.types = types;
            ObjectiveController.Instance.OnKillCharacter += OnKillCharacter;
        }
		
		void OnKillCharacter (BaseCharacter killer, BaseCharacter victim) {
			if(killer.characterController == timeLoop.characterController) {
				if (targetCharacters.Contains(victim)) {
					ProgressOnGoal();
				}
			}
		}

	protected override void BeginObjective () {
		List<BaseCharacter> targets = targetCharacters;
		foreach(BaseCharacter character in targets) {
			ObjectiveMarker objectiveMarker = ObjectX.InstantiateAsChild<ObjectiveMarker>(PrefabDatabase.Instance.objectiveMarker, GameController.Instance.level.transform);
			objectiveMarker.target = character.transform;
		}
		base.BeginObjective ();
	}
}