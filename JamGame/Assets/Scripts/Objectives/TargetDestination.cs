﻿using UnityEngine;
using System.Collections;

public class TargetDestination : MonoBehaviour {
	public void OnTriggerEnter2D (Collider2D collider) {
		BaseCharacter character = collider.GetComponent<BaseCharacter>();
		if(character != null) {
			ObjectiveController.Instance.ReachDestination(character, this);
		}
	}
}
