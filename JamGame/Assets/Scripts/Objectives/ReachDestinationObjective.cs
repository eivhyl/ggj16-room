﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ReachDestinationObjective : Objective {

	public override string description {
		get {
			return _description;
		} set {
			_description = value;
		}
    }

	private string targetDestinationName;
	private TargetDestination targetDestination {
		get {
			return GameController.Instance.level.targetDestinations.targetDestinations[targetDestinationName];
		}
	}

	public ReachDestinationObjective(TimeLoop timeLoop, string targetDestinationName, int goal = 1) : base(timeLoop, goal) {
		description = "REACH THE TARGET".ToUpper();
		this.targetDestinationName = targetDestinationName;
		ObjectiveController.Instance.OnReachDestination += OnReachDestination;
    }

	void OnReachDestination (BaseCharacter character, TargetDestination destination) {
		if(destination.gameObject.name == targetDestination.gameObject.name)
			CompleteObjective();
	}

	protected override void BeginObjective () {
		ObjectiveMarker objectiveMarker = ObjectX.InstantiateAsChild<ObjectiveMarker>(PrefabDatabase.Instance.objectiveMarker, GameController.Instance.level.transform);
		objectiveMarker.target = targetDestination.transform;
		base.BeginObjective ();
	}
}