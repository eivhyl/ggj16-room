﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.Controllers;

public abstract class Objective {
	public bool completed;
    protected int progress;
    protected int goal;
	protected TimeLoop timeLoop;
    //protected abstract string displayName { get; }
	protected string _description;
    public abstract string description { get; set; }

	public delegate void OnCompleteEvent ();
	public event OnCompleteEvent OnComplete;

	protected Objective(TimeLoop timeLoop) {
        progress = 0;
        this.timeLoop = timeLoop;
        this.goal = 1;

		GameController.Instance.timeLoopController.OnStartLoop += OnStartLoop;
    }

    protected Objective(TimeLoop timeLoop, int goal) {
        progress = 0;
        this.timeLoop = timeLoop;
        this.goal = goal;

		GameController.Instance.timeLoopController.OnStartLoop += OnStartLoop;
    }

    void OnStartLoop (TimeLoop loop) {
		BeginObjective();
    }

	protected virtual void BeginObjective () {
		progress = 0;
		completed = false;
    }

	protected virtual void CompleteObjective () {
		DebugX.Log("REACH! "+completed);
		if(completed) return;

		completed = true;
		ObjectiveController.Instance.UpdateObjective(timeLoop, true);
		if(OnComplete != null) {
			OnComplete();
		}
    }

    protected void ProgressOnGoal () {
		progress++;
        if (progress >= goal) {
			CompleteObjective();
        }
    }

    public override string ToString() {
        if (description != null)
            return description;
        return "DESCRIPTION NOT FOUND";
    }
}