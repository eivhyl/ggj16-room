﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Level : MonoBehaviour {
	public SpawnPoints spawnPoints;
	public Transform characters;
	public OccultLevelCharacterPrefabs characterPrefabs;
	public LevelTargetDestinationManager targetDestinations;

	void Awake () {
		spawnPoints.GetComponent<SpawnPoints>();
		targetDestinations = GetComponentInChildren<LevelTargetDestinationManager>();
	}

	void Update () {
	
	}

	public BaseCharacter GetNextCharacterPrefab (int loopIndex) {
		return characterPrefabs.characters.GetRepeating(loopIndex);
//		return prefabs.baby;
	}
}