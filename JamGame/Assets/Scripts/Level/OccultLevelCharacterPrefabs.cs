using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OccultLevelCharacterPrefabs : ScriptableObject {
	public CthuluCharacter cthulu;
	public BabyCharacter baby;
	public OccultistCharacter occultist;
	public BusinessmanCharacter businessman;
	public KnightCharacter knight;
	public SheepCharacter sheep;
	public ThiefCharacter thief;

	public List<BaseCharacter> characters {
		get {
			return new List<BaseCharacter>() {
				thief,
				occultist,
				cthulu,
				knight,
				baby,
				businessman,
				sheep,
			};
		}
	}
}
