﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelTargetDestinationManager : MonoBehaviour {
	public Dictionary<string, TargetDestination> targetDestinations;


	private void Awake () {
		targetDestinations = new Dictionary<string, TargetDestination>();
		foreach(TargetDestination targetDestination in GetComponentsInChildren<TargetDestination>()) {
			targetDestinations.Add(targetDestination.gameObject.name, targetDestination);
		}
	}
}
