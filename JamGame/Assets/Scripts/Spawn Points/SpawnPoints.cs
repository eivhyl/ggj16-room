﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnPoints : MonoBehaviour {
	public List<Transform> spawnPoints;

	void Awake () {
		spawnPoints = GetComponentsInChildren<Transform>().ToList();
	}
}
