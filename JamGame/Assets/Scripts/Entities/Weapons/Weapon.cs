using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {
	[HideInInspector]
	public BaseCharacter owner;
	public float useTime = 0.25f;

	protected virtual void OnTriggerEnter2D (Collider2D collider) {
		CharacterHitArea characterHitArea = collider.GetComponent<CharacterHitArea>();
		if(characterHitArea == null || characterHitArea.character == owner || characterHitArea.character.GetComponent<IKnifeable>() == null) return;
		else {
			Use(characterHitArea.character);
		}
	}

	protected virtual void Use (BaseCharacter character) {
		character.Die(owner);
	}
}
