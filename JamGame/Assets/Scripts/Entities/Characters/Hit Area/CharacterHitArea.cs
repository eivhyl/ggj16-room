﻿using UnityEngine;
using System.Collections;

public class CharacterHitArea : MonoBehaviour {
	[HideInInspector]
	public BaseCharacter character;

	private void Awake () {
		character = GetComponentInParent<BaseCharacter>();
	}
}
