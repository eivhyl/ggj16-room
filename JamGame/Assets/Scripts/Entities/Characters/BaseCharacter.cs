﻿using UnityEngine;
using System;
using System.Collections;
using Assets.Scripts.Objectives;

public enum Side {
	Left,
	Right
}

public abstract class BaseCharacter : MonoBehaviour
{
    public abstract AudioController.SpecialEffects sfx { get; }
	public abstract CharacterType type {get;}

	public bool alive = true;
	public float moveSpeed = 5;
	[HideInInspector]
	public Vector3 moveDirection;

	public BaseCharacterController characterController;

	[HideInInspector]
	public Side sideFacing = Side.Left;

	[HideInInspector]
	public SpriteRenderer spriteRenderer;
	[HideInInspector]
	public new Rigidbody2D rigidbody2D;
	[HideInInspector]
	public new Collider2D collider2D;
	[HideInInspector]
	public SimpleAnimator animator;

	public Transform headAnchor;

	public bool canMove = true;
	protected virtual void Awake () {
		spriteRenderer = GetComponentInChildren<SpriteRenderer>();
		rigidbody2D = GetComponent<Rigidbody2D>();
		collider2D = GetComponent<Collider2D>();
		animator = GetComponentInChildren<SimpleAnimator>();
	}

	protected virtual void Start () {
//		if(objective != null)
			
	}

	public virtual void OnBodyTriggerEnter (Collider2D collider) {
		Weapon weapon = collider.GetComponent<Weapon>();
		Debug.Log(weapon+" "+ collider.transform.HierarchyPath());
	}

	protected virtual void FixedUpdate () {
		transform.SetLocalScaleX(sideFacing == Side.Left ? -1 : 1);
		transform.SetLocalPositionZ(transform.position.y);

		if(characterController != null)
			characterController.Update();
	}

	public virtual void Move (Vector2 inputVector) {
		if(!canMove) return;
		if(inputVector == Vector2.zero) {
			animator.PlayAnimation("Idle");
		} else {
			animator.PlayAnimation("Walk");
		}
		if(inputVector.x != 0) {
			sideFacing = inputVector.x < 0 ? Side.Left : Side.Right;
		}
		rigidbody2D.MovePosition(rigidbody2D.position + inputVector * moveSpeed * Time.fixedDeltaTime);
	}

	public virtual void PressTriggerKey () {
//		Debug.Log("TRIGGER "+gameObject.name+" AT "+GameController.Instance.timeLoopController.loopTimer.currentTime);
//		rigidbody2D.AddForce(new Vector2(1000, 0), ForceMode2D.Force);
//		Debug.Log("THINGGGG "+rigidbody2D.velocity);
	}

	public virtual void Die (BaseCharacter murderer) {
		if(murderer != null)
			ObjectiveController.Instance.KillCharacter(murderer, this);
		Die();
	}

	public virtual void Die () {
		alive = false;
		foreach(Collider2D collider in GetComponentsInChildren<Collider2D>()) {
			collider.enabled = false;
		}
		rigidbody2D.velocity = Vector3.zero;
		spriteRenderer.sortingLayerName = "Details";
		if(!animator.Contains("Die")) {
			spriteRenderer.color = spriteRenderer.color.SetAlpha(0.25f);
		} else {
			animator.PlayAnimation("Die");
		}
		ObjectX.InstantiateAsChild<BloodPool>(PrefabDatabase.Instance.bloodPool, GameController.Instance.level.transform, transform.position, Quaternion.identity);
	}

	protected virtual void OnCollisionEnter2D (Collision2D collision) {
//		rigidbody2D.AddForce(collision.relativeVelocity, ForceMode2D.Impulse);
	}

	protected virtual void OnCollisionStay2D (Collision2D collision) {
		
	}

	protected virtual void OnCollisionExit2D (Collision2D collision) {
		
	}

    public void playEffect()
    {
        AudioController.Instance.playEffect(sfx);
    }
}
