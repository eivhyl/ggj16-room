﻿using UnityEngine;
using System.Collections;
using System;

public class ActiveCharacterController : BaseCharacterController {

    public ActiveCharacterController (BaseCharacter character) : base (character) {
		stateDictionary = new GenericDictionary();
		stateDictionary.Add("position", new PropertyCurve<Vector3?>());
		stateDictionary.Add("vector", new PropertyCurve<Vector2>());
		stateDictionary.Add("trigger", new PropertyCurve<bool>());

		stateDictionary.GetValue<PropertyCurve<Vector3?>>("position").AddKey(0, character.transform.position);
		stateDictionary.GetValue<PropertyCurve<Vector3?>>("position").AddKey(0.01f, null);

		Record(0);
	}

	public void OnConverted () {
		objective.OnComplete -= OnCompleteObjective;
	}

	public override void Update () {
		base.Update();
		Record(GameController.Instance.timeLoopController.loopTimer.currentTime);
	}

	private void Record (float time) {
		if(!character.alive) return;
	    if (GameController.state != GameController.GameState.Playing) return;


		Vector2 inputVector = InputX.Instance.keyboardInput.GetCombinedDirectionFromArrowKeys();
		bool triggerKeyDown = Input.GetKeyDown(KeyCode.Space);

		if(stateDictionary.GetValue<PropertyCurve<Vector2>>("vector").keys.IsEmpty() || stateDictionary.GetValue<PropertyCurve<Vector2>>("vector").Evaluate(time, PropertyCurveEvaluateMode.Unsmoothed) != inputVector)
			stateDictionary.GetValue<PropertyCurve<Vector2>>("vector").AddKey(time, inputVector);
		if(stateDictionary.GetValue<PropertyCurve<bool>>("trigger").keys.IsEmpty() || stateDictionary.GetValue<PropertyCurve<bool>>("trigger").Evaluate(time, PropertyCurveEvaluateMode.Unsmoothed) != triggerKeyDown)
			stateDictionary.GetValue<PropertyCurve<bool>>("trigger").AddKey(time, triggerKeyDown);
		character.Move(inputVector);
		if(triggerKeyDown)
			character.PressTriggerKey();
	}
}
