﻿using UnityEngine;
using System.Collections;

public class PassiveCharacterController : BaseCharacterController {
	
	public PassiveCharacterController (BaseCharacter character, ActiveCharacterController activeCharacterController) : base (character) {
		stateDictionary = activeCharacterController.stateDictionary;
		objective = activeCharacterController.objective;
	}

	public override void Update () {
		Rewind(GameController.Instance.timeLoopController.loopTimer.currentTime);
		base.Update();
	}

	public void Rewind (float time) {
		if(!character.alive) return;
		Vector3? position = stateDictionary.GetValue<PropertyCurve<Vector3?>>("position").Evaluate(time, PropertyCurveEvaluateMode.Unsmoothed);
		Vector2 inputVector = stateDictionary.GetValue<PropertyCurve<Vector2>>("vector").Evaluate(time, PropertyCurveEvaluateMode.Unsmoothed);
		bool triggerKeyDown = stateDictionary.GetValue<PropertyCurve<bool>>("trigger").Evaluate(time, PropertyCurveEvaluateMode.Unsmoothed);

		if(position != null) {
			character.transform.position = (Vector3)position;
		}
		character.Move(inputVector);
		if(triggerKeyDown)
			character.PressTriggerKey();
	}

}
