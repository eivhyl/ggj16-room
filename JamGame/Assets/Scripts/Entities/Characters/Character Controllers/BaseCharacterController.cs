using UnityEngine;
using System.Collections;

public class BaseCharacterController {

	public BaseCharacter character;
	public TimeLoop timeLoop {get;set;}
	public GenericDictionary stateDictionary {get;protected set;}

	private Objective _objective;
    public Objective objective {
    	get {
    		return _objective;
    	} set {
    		_objective = value;
			_objective.OnComplete += OnCompleteObjective;
    	}
    }

	public BaseCharacterController (BaseCharacter character) {
		this.character = character;
	}

	public virtual void Update () {
		
	}


	protected virtual void OnCompleteObjective () {
		Debug.Log("COMPLETE! "+this.character);
		CompletedObjectiveMarker completedObjectiveMarker = ObjectX.InstantiateAsChild<CompletedObjectiveMarker>(PrefabDatabase.Instance.completedObjectiveMarker, character.headAnchor);
	}
}
