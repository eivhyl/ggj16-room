﻿using UnityEngine;
using System.Collections;

public class OccultistCharacter : BaseCharacter, IKillable, IKnifeable {
    public override AudioController.SpecialEffects sfx
    {
        get { return AudioController.SpecialEffects.SERVANT; }
    }

    public override CharacterType type {
		get {
			return CharacterType.Occultist;
		}
	}

	private OccultistKnife knife;

	protected override void Awake () {
		knife = GetComponentInChildren<OccultistKnife>(true);
		knife.owner = this;
		base.Awake();
	}

	public override void PressTriggerKey () {
		base.PressTriggerKey();
		StartCoroutine(FlashKnife());
	}

	private IEnumerator FlashKnife () {
		canMove = false;
		animator.PlayAnimation("Action");
		knife.gameObject.SetActive(true);
		yield return new WaitForSeconds(knife.useTime);
		knife.gameObject.SetActive(false);
		canMove = true;
        AudioController.Instance.playEffect(AudioController.SpecialEffects.STAB);
    }

	public void Kill() {}

	protected override void OnCollisionEnter2D (Collision2D collision) {
		base.OnCollisionEnter2D(collision);
	}

	protected override void OnCollisionStay2D (Collision2D collision) {
		base.OnCollisionStay2D(collision);
	}

	protected override void OnCollisionExit2D (Collision2D collision) {
		base.OnCollisionExit2D(collision);
	}
}
