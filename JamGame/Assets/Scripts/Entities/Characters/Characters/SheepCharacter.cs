﻿using UnityEngine;
using System.Collections;

public class SheepCharacter : BaseCharacter, IKillable, IKnifeable {
    public override AudioController.SpecialEffects sfx
    {
        get { return AudioController.SpecialEffects.SHEEP; }
    }

    public override CharacterType type {
		get {
			return CharacterType.Sheep;
		}
	}

	public void Kill() {}

	protected override void OnCollisionEnter2D (Collision2D collision) {
		base.OnCollisionEnter2D(collision);
	}

	protected override void OnCollisionStay2D (Collision2D collision) {
		base.OnCollisionStay2D(collision);
	}

	protected override void OnCollisionExit2D (Collision2D collision) {
		base.OnCollisionExit2D(collision);
	}
}
