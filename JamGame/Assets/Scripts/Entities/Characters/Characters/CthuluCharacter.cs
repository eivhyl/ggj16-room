﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CthuluCharacter : BaseCharacter, IKillable {

    public override AudioController.SpecialEffects sfx
    {
        get { return AudioController.SpecialEffects.CTHULU; }
    }

    public override CharacterType type
    {
        get
        {
            return CharacterType.Cthulu;
        }
    }

    private bool isGrabbing;
    Transform grabbedPos;

    protected override void Awake()
    {
        base.Awake();
        isGrabbing = false;
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
    }

    public override void PressTriggerKey()
    {
        if (!isGrabbing)
        {
            AudioController.Instance.playEffect(AudioController.SpecialEffects.TOUCH);
            foreach (var obj in FindObjectsOfType<BaseCharacter>())
            {
                if ((obj.rigidbody2D.position - rigidbody2D.position).magnitude.Abs() < 3)
                {
                    if (sideFacing == Side.Left)
                    {
                        if (obj.rigidbody2D.position.x < rigidbody2D.position.x)
                        {
                            grabbedPos = obj.transform;
                            isGrabbing = true;
                            break;
                        }
                    }
                    else
                    {
                        if (obj.rigidbody2D.position.x > rigidbody2D.position.x)
                        {
                            grabbedPos = obj.transform;
                            isGrabbing = true;
                            break;
                        }
                    }
                }
            }
        } else {
            isGrabbing = false;
        }

    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if (isGrabbing)
        {
            float offset = (0.5f - (int)sideFacing);
            grabbedPos.position = new Vector2(rigidbody2D.position.x - offset*2.2f, rigidbody2D.position.y);
        }
    }

    public void Kill()
    {
        Die();
    }
}