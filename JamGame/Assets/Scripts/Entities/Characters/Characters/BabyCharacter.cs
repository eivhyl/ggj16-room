﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class BabyCharacter : BaseCharacter, IKillable, IKnifeable {
    public override AudioController.SpecialEffects sfx
    {
        get { return AudioController.SpecialEffects.BABY; }
    }

    public override CharacterType type {
        get
        {
			return CharacterType.Baby;
		}
	}
    /*
    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
        Kill();
    }*/

    public override void PressTriggerKey()
    {
        Kill();
    }

    private void Explode()
    {
        ObjectX.InstantiateAsChild(PrefabDatabase.Instance.Explosion, GameController.Instance.level.transform, transform.position, Quaternion.identity);
        foreach (var obj in FindObjectsOfType<BaseCharacter>())
        {
            if ((obj.rigidbody2D.position - rigidbody2D.position).magnitude.Abs() < 2)
            {
                if(obj != this)
				    obj.Die(this);
            }
        }

        AudioController.Instance.playEffect(AudioController.SpecialEffects.EXPLOSION);
       
    }

    public void Kill()
    {
        Explode();
        Die();
    }
}