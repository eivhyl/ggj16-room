﻿using UnityEngine;
using System.Collections;

public class KnightCharacter : BaseCharacter, IKillable, IKnifeable {

    public override AudioController.SpecialEffects sfx
    {
        get { return AudioController.SpecialEffects.HERO; }
    }

    public override CharacterType type {
		get {
			return CharacterType.Knight;
		}
	}

	private KnightSword sword;

	protected override void Awake () {
		sword = GetComponentInChildren<KnightSword>(true);
		sword.owner = this;
		base.Awake();
	}

	public override void PressTriggerKey () {
		base.PressTriggerKey();
		StartCoroutine(SwingSword());
	}

	private IEnumerator SwingSword () {
		canMove = false;
		animator.PlayAnimation("Action");
		sword.gameObject.SetActive(true);
		yield return new WaitForSeconds(sword.useTime);
		sword.gameObject.SetActive(false);
		canMove = true;
        AudioController.Instance.playEffect(AudioController.SpecialEffects.SWORD);
	}

	public void Kill() {}

	protected override void OnCollisionEnter2D (Collision2D collision) {
		base.OnCollisionEnter2D(collision);
	}

	protected override void OnCollisionStay2D (Collision2D collision) {
		base.OnCollisionStay2D(collision);
	}

	protected override void OnCollisionExit2D (Collision2D collision) {
		base.OnCollisionExit2D(collision);
	}
}
