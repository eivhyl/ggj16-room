﻿using UnityEngine;
using System.Collections;

public class BusinessmanCharacter : BaseCharacter, IKillable, IKnifeable {
    public override AudioController.SpecialEffects sfx
    {
        get { return AudioController.SpecialEffects.RANDOMPERSON; }
    }

    public override CharacterType type {
		get {
			return CharacterType.Businessman;
		}
	}

	public override void PressTriggerKey () {
		base.PressTriggerKey();
	}

	public void Kill() {}

	protected override void OnCollisionEnter2D (Collision2D collision) {
		base.OnCollisionEnter2D(collision);
	}

	protected override void OnCollisionStay2D (Collision2D collision) {
		base.OnCollisionStay2D(collision);
	}

	protected override void OnCollisionExit2D (Collision2D collision) {
		base.OnCollisionExit2D(collision);
	}
}