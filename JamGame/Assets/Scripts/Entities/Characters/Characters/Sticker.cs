﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Sticker : BaseCharacter, IKillable
{
    public override AudioController.SpecialEffects sfx
    {
        get { return AudioController.SpecialEffects.NONE; }
    }

    public override CharacterType type {
		get {
			return CharacterType.Sticker;
		}
	}

    private bool isGrabbing;
    Vector2 grabbedPos;

    protected override void Awake()
    {
        base.Awake();
        isGrabbing = false;
    }

    protected override void OnCollisionEnter2D(Collision2D collision)
    {
        base.OnCollisionEnter2D(collision);
    }

    public override void PressTriggerKey()
    {
        if (!isGrabbing)
            foreach (var obj in FindObjectsOfType<BaseCharacter>())
            {
                if ((obj.rigidbody2D.position - rigidbody2D.position).magnitude.Abs() < 2)
                {
                    if (sideFacing == Side.Left)
                        if (obj.rigidbody2D.position.x < rigidbody2D.position.x)
                        {
                            grabbedPos = obj.rigidbody2D.position;
                            isGrabbing = true;
                            break;
                        }
                        else if (obj.rigidbody2D.position.x > rigidbody2D.position.x)
                        {
                            grabbedPos = obj.rigidbody2D.position;
                            isGrabbing = true;
                            break;
                        }
                }
            }
        else
            isGrabbing = false;
    }

    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        if (isGrabbing)
        {
            grabbedPos.SetX(rigidbody2D.position.x - (0.5f + (int)sideFacing));
            grabbedPos.SetY(rigidbody2D.position.y);
        }
    }

    public void Kill()
    {
        Die();
    }
}