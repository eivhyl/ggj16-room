using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SimpleAnimation {
	public string name = "Animation Name";
	public float frameTime = 0.2f;
	public List<SimpleAnimationFrame> frames;
}