﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer))]
public class SimpleAnimator : MonoBehaviour {
	[HideInInspector]
	public SpriteRenderer spriteRenderer;
	public string defaultAnimation;
    public bool destroyOnFinish;
    public List<SimpleAnimation> animations;
	public Dictionary<string, SimpleAnimation> animationDictionary;
	[HideInInspector]
	public SimpleAnimation currentAnimation;


	private Timer frameTimer = new Timer();
	private int animationIndex = 0;

	public delegate void OnCompleteAnimationEvent();
	public event OnCompleteAnimationEvent OnCompleteAnimation;

	public delegate void OnCompleteAnimationFrameEvent();
	public event OnCompleteAnimationFrameEvent OnCompleteAnimationFrame;

	void Awake () {
		if(animations.Count == 0) return;

		spriteRenderer = GetComponent<SpriteRenderer>();
		frameTimer.OnComplete += FrameTimerComplete;

		animationDictionary = new Dictionary<string, SimpleAnimation>();
		foreach(SimpleAnimation animation in animations) {
			if(animation.frames.Count > 0)
				animationDictionary.Add(animation.name, animation);
		}

		if(defaultAnimation != "") {
			PlayAnimation(animationDictionary[defaultAnimation]);
		}
	}

	public void PlayAnimation (string animationName) {
		if(animationName == currentAnimation.name) return;
		SimpleAnimation animation;
		if(animationDictionary.TryGetValue(animationName, out animation)) {
			PlayAnimation(animation);
		} else {
			Debug.LogWarning("Animation '"+animationName+"' not found on "+transform.HierarchyPath());
		}
	}

	public bool Contains (string animationName) {
		return animationDictionary.ContainsKey(animationName);
	}

	private void PlayAnimation (SimpleAnimation animation) {
		currentAnimation = animation;
		animationIndex = 0;
		SetAnimationFrame(animationIndex);
		StartFrameTimer ();
	}

	void StartFrameTimer () {
		if(currentAnimation.frames.GetRepeating(animationIndex).overrideFrameTime) {
			frameTimer.Set(currentAnimation.frames.GetRepeating(animationIndex).frameTime);
		} else {
			frameTimer.Set(currentAnimation.frameTime);
		}
		frameTimer.Start();
	}

	void Update () {
		frameTimer.Loop();
	}

	void FrameTimerComplete () {
		if(OnCompleteAnimationFrame != null) OnCompleteAnimationFrame();
        animationIndex++;

		if (animationIndex % currentAnimation.frames.Count == 0) {
		    if (OnCompleteAnimation != null) OnCompleteAnimation();
            if (destroyOnFinish)
                Destroy(gameObject);
        }
		
		SetAnimationFrame(animationIndex);

		StartFrameTimer();
	}

	void SetAnimationFrame (int index) {
		spriteRenderer.sprite = currentAnimation.frames.GetRepeating(index).sprite;
	}
}