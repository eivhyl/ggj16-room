using UnityEngine;
using UnityEditor;
using Rotorz.ReorderableList;

[CustomPropertyDrawer(typeof(SimpleAnimationFrame))]
public class SimpleAnimationFramePropertyDrawer : PropertyDrawer {
	public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
		return EditorGUIUtility.singleLineHeight * 2 + EditorGUIUtility.standardVerticalSpacing;
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
//		EditorGUI.BeginProperty (position, label, property);

		// Draw label
//		position = EditorGUI.PrefixLabel (position, GUIUtility.GetControlID (FocusType.Passive), label);
		
		// Don't make child fields be indented
//		var indent = EditorGUI.indentLevel;
//		EditorGUI.indentLevel = 0;

		var spriteRect = new Rect (position.x, position.y, position.width, EditorGUIUtility.singleLineHeight);
		var overrideLabelRect = new Rect (position.x, position.y+EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing, 115, EditorGUIUtility.singleLineHeight);
		var overrideFrameRect = new Rect (position.x + 120, position.y+EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing, 20, EditorGUIUtility.singleLineHeight);
		var frameTimeRect = new Rect (position.x+145, position.y+EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing, position.width-145, EditorGUIUtility.singleLineHeight);

		EditorGUI.PropertyField(spriteRect, property.FindPropertyRelative("sprite"));
		EditorGUI.LabelField(overrideLabelRect, "Override Frame Time");
		EditorGUI.PropertyField(overrideFrameRect, property.FindPropertyRelative("overrideFrameTime"), GUIContent.none);

		if(!property.FindPropertyRelative("overrideFrameTime").boolValue) GUI.enabled = false;
		EditorGUI.PropertyField(frameTimeRect, property.FindPropertyRelative("frameTime"), GUIContent.none);
		GUI.enabled = true;

//		 Set indent back to what it was
//		EditorGUI.indentLevel = indent;

//		EditorGUI.EndProperty ();
	}
}
