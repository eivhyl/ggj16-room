﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using Rotorz.ReorderableList;

[CustomEditor(typeof(SimpleAnimator))]
public class SimpleAnimatorEditor : BaseEditor<SimpleAnimator> {
	public override void OnInspectorGUI() {
		EditorGUILayout.PropertyField(serializedObject.FindProperty("defaultAnimation"));
		EditorGUILayout.PropertyField(serializedObject.FindProperty("destroyOnFinish"));
		ReorderableListGUI.Title("Animations");
		ReorderableListGUI.ListField(serializedObject.FindProperty("animations"));

		serializedObject.ApplyModifiedProperties();
	}
}
