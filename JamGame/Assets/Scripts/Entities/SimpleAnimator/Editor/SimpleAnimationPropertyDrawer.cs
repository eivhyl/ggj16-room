﻿using UnityEngine;
using UnityEditor;
using Rotorz.ReorderableList;

[CustomPropertyDrawer(typeof(SimpleAnimation))]
public class SimpleAnimationPropertyDrawer : PropertyDrawer {
	public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
		float nameHeight = 20;
		float arrayHeight = property.FindPropertyRelative("frames").arraySize * 20 * 2;
		float arrayNewButtonHeight = 20;
		return nameHeight + arrayHeight + arrayNewButtonHeight;
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		EditorGUI.BeginProperty (position, label, property);

		// Draw label
		position = EditorGUI.PrefixLabel (position, GUIUtility.GetControlID (FocusType.Passive), label);
		
		// Don't make child fields be indented
		var indent = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;

		var actionTypeRect = new Rect (position.x, position.y, 65, EditorGUIUtility.singleLineHeight);
		var secondFieldRect = new Rect (position.x+70, position.y, position.width-70, EditorGUIUtility.singleLineHeight);
		var listRect = new Rect (position.x, position.y+20, position.width, 20 + (property.FindPropertyRelative("frames").arraySize * 19 * 2));

		EditorGUI.PropertyField(actionTypeRect, property.FindPropertyRelative("name"), GUIContent.none);
		EditorGUI.PropertyField(secondFieldRect, property.FindPropertyRelative("frameTime"), GUIContent.none);
		ReorderableListGUI.ListFieldAbsolute(listRect, property.FindPropertyRelative("frames"));

//		 Set indent back to what it was
		EditorGUI.indentLevel = indent;

		EditorGUI.EndProperty ();
	}
}