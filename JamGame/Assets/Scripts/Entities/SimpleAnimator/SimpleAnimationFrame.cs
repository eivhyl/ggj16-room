using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SimpleAnimationFrame {
	public Sprite sprite;
	public bool overrideFrameTime = false;
	public float frameTime = 0.2f;

	public SimpleAnimationFrame (Sprite sprite) {
		this.sprite = sprite;
	}

	public SimpleAnimationFrame (Sprite sprite, float frameTime) {
		this.sprite = sprite;
		overrideFrameTime = true;
		this.frameTime = frameTime;
	}
}