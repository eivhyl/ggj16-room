﻿using UnityEngine;
using System.Collections;

public class TimeController : MonoBehaviour {
	public static TimeController inst;

	[SerializeField]
	private float _time = 0;
	public float time {
		get {
			return _time;
		} set {
			_time = value;
			if(_time <= 0){
				_time = 0;
				if(state == TimeState.Rewinding){
					state = TimeState.Paused;
				}
			}
		}
	}

	[SerializeField]
	private float _timeScale = 1;
	public float timeScale {
		get {
			return _timeScale;
		} set {
			_timeScale = value;
			if(_timeScale == 0){
				state = TimeState.Paused;
			} else if(_timeScale > 0){
				state = TimeState.Playing;
			} else if(_timeScale < 0 && time > 0){
				state = TimeState.Rewinding;
			}
		}
	}

	public float lastTime = 0;
	public float deltaTime = 0;

	public TimeState _state = TimeState.Playing;
	public TimeState state {
		get {
			return _state;
		} set {
			_state = value;
			if(OnTimeStateChange != null){
				OnTimeStateChange(_state);
			}
		}
	}
	public delegate void OnTimeStateChangeEvent(TimeState state);
	public event OnTimeStateChangeEvent OnTimeStateChange;

	void Awake(){
		inst = this;
	}
	
	void Update () {
		time += timeScale * Time.deltaTime;
		deltaTime = time - lastTime;
		lastTime = time;

		if(Input.GetKeyDown("r")){
			if(state == TimeState.Rewinding){
				timeScale = 1;
			} else if(state == TimeState.Playing){
				timeScale = -1;
			} else if(state == TimeState.Paused){
				timeScale = 1;
			}
		}
	}
}
