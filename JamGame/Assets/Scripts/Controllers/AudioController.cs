﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoSingleton<AudioController>
{
    public AudioClip defaultMusic, filteredMusic, menuMusic, plink;
    private AudioSource defaultMusicSrc, filteredMusicSrc, menuMusicSrc, plinkSrc;
    public AudioClip baby;
    private AudioSource babySrc;
    public AudioClip devil;
    private AudioSource devilSrc;
    public AudioClip explode;
    private AudioSource explodeSrc;
    public AudioClip hero;
    private AudioSource heroSrc;
    public AudioClip randomPerson;
    private AudioSource randomPersonSrc;
    public AudioClip servant;
    private AudioSource servantSrc;
    public AudioClip sheep;
    private AudioSource sheepSrc;
    public AudioClip stab;
    private AudioSource stabSrc;
    public AudioClip sword;
    private AudioSource swordSrc;
    public AudioClip touch;
    private AudioSource touchSrc;
    public AudioClip cthulu;
    private AudioSource cthuluSrc;
    private bool playingGame, waitScreen;
    private float timeToStart = 0.0f;

    // Use this for initialization
    void Start()
    {
        playingGame = waitScreen;
        waitScreen = true;
        defaultMusicSrc = gameObject.AddComponent<AudioSource>();
        defaultMusicSrc.clip = defaultMusic;
        filteredMusicSrc = gameObject.AddComponent<AudioSource>();
        filteredMusicSrc.clip = filteredMusic;
        menuMusicSrc = gameObject.AddComponent<AudioSource>();
        menuMusicSrc.clip = menuMusic;
        plinkSrc = gameObject.AddComponent<AudioSource>();
        plinkSrc.clip = plink;
        babySrc = gameObject.AddComponent<AudioSource>();
        babySrc.clip = baby;
        devilSrc = gameObject.AddComponent<AudioSource>();
        devilSrc.clip = devil;
        explodeSrc = gameObject.AddComponent<AudioSource>();
        explodeSrc.clip = explode;
        heroSrc = gameObject.AddComponent<AudioSource>();
        heroSrc.clip = hero;
        randomPersonSrc = gameObject.AddComponent<AudioSource>();
        randomPersonSrc.clip = randomPerson;
        servantSrc = gameObject.AddComponent<AudioSource>();
        servantSrc.clip = servant;
        sheepSrc = gameObject.AddComponent<AudioSource>();
        sheepSrc.clip = sheep;
        stabSrc = gameObject.AddComponent<AudioSource>();
        stabSrc.clip = stab;
        swordSrc = gameObject.AddComponent<AudioSource>();
        swordSrc.clip = sword;
        touchSrc = gameObject.AddComponent<AudioSource>();
        touchSrc.clip = touch;
        cthuluSrc = gameObject.AddComponent<AudioSource>();
        cthuluSrc.clip = cthulu;

        filteredMusicSrc.loop = true;
        defaultMusicSrc.loop = true;
        menuMusicSrc.loop = true;
        setState(false);
    }

    void FixedUpdate()
    {
        if (playingGame)
        {
           
            if (!waitScreen && timeToStart > 0)
            {
                timeToStart -= Time.deltaTime;
                defaultMusicSrc.volume = Mathf.Max(Mathf.Min(defaultMusicSrc.volume - Time.deltaTime, 0.3f), 0.0f);
                filteredMusicSrc.volume = Mathf.Max(Mathf.Min(filteredMusicSrc.volume + Time.deltaTime, 0.3f), 0.0f);
            } else
            {
                defaultMusicSrc.volume = Mathf.Max(Mathf.Min(defaultMusicSrc.volume + (waitScreen ? -1 : 1) * Time.deltaTime, 0.3f), 0.0f);
                filteredMusicSrc.volume = Mathf.Max(Mathf.Min(filteredMusicSrc.volume + (waitScreen ? 1 : -1) * Time.deltaTime, 0.3f), 0.0f);
                if (!plinkSrc.isPlaying)
                {
                    waitScreen = true;
                }
            }
            if (!waitScreen && timeToStart < 0 && !plinkSrc.isPlaying)
            {
                Debug.Log("trigger");
                plinkSrc.Stop();
                plinkSrc.Play();
            }
        } else
        {
            menuMusicSrc.volume = Mathf.Min(menuMusicSrc.volume + Time.deltaTime, 0.3f);
        }
    }

    public float triggerGamePhase()
    {
        plinkSrc.Stop();
        waitScreen = false;
        timeToStart = 4f - (defaultMusicSrc.time % 4f);
        
        return timeToStart;
    }

    public float gamePhaseTimeLeft()
    {
        if (plinkSrc.isPlaying)
        {
            return 8f - plinkSrc.time;
        }
        return -1f;
    }

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            triggerGamePhase();
        } else if (Input.GetKeyDown(KeyCode.O)) {
            setState(!playingGame);
        }
    }

    public void setState(bool state)
    {
        playingGame = state;
        defaultMusicSrc.Stop();
        filteredMusicSrc.Stop();
        menuMusicSrc.Pause();
        if (playingGame)
        {
            defaultMusicSrc.volume = 0f;
            filteredMusicSrc.volume = 0.3f;
            defaultMusicSrc.Play();
            filteredMusicSrc.Play();
        }
        else
        {
            menuMusicSrc.volume = 0;
            menuMusicSrc.Play();
        }
    }

    public enum SpecialEffects
    {
        EXPLOSION = 0,
        TOUCH,
        BABY,
        DEVIL,
        HERO,
        RANDOMPERSON,
        SERVANT,
        SHEEP,
        SWORD,
        STAB,
        CTHULU,
        NONE,
        UNDEFINED
    };

    public void playEffect(SpecialEffects sfx)
    {
        switch (sfx)
        {
            case SpecialEffects.EXPLOSION:
                explodeSrc.Play();
                break;
            case SpecialEffects.TOUCH:
                touchSrc.Play();
                break;
            case SpecialEffects.BABY:
                babySrc.Play();
                break;
            case SpecialEffects.DEVIL:
                devilSrc.Play();
                break;
            case SpecialEffects.HERO:
                heroSrc.Play();
                break;
            case SpecialEffects.RANDOMPERSON:
                randomPersonSrc.Play();
                break;
            case SpecialEffects.SERVANT:
                servantSrc.Play();
                break;
            case SpecialEffects.SHEEP:
                sheepSrc.Play();
                break;
            case SpecialEffects.SWORD:
                swordSrc.Play();
                break;
            case SpecialEffects.STAB:
                stabSrc.Play();
                break;
            case SpecialEffects.CTHULU:
                cthuluSrc.Play();
                break;
            case SpecialEffects.NONE:
                break;
            default:
                Debug.LogError("AUDIO SFX FAIL");
                break;
        }
    }
}