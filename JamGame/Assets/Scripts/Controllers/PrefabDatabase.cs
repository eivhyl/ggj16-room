﻿using UnityEngine;
using System.Collections;

public class PrefabDatabase : MonoSingleton<PrefabDatabase> {
	public GameObject currentPlayerArrow;
	public ObjectiveMarker objectiveMarker;
	public CompletedObjectiveMarker completedObjectiveMarker;
	public BloodPool bloodPool;
    public GameObject Explosion;
}