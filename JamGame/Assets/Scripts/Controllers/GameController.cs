using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Controllers;
using UnityEngine.SceneManagement;
using Assets.Scripts.Objectives;

public class GameController : MonoSingleton<GameController> {

	public enum GameState {
		ShowingObjective,
		Playing
	}
	public static GameState state;

	public Level level;
	public TimeLoopController timeLoopController;

	// The currently playing character
	public BaseCharacter activeCharacter;
	public List<BaseCharacter> characters;

	private Timer objectivePanelTimer;

	void Awake ()
	{
		timeLoopController.Awake();

		objectivePanelTimer = new Timer();
		objectivePanelTimer.OnComplete += CompleteObjectivePanelTimer;
        AudioController.Instance.setState(true);
	}

	void Start () {
		if(!SceneManager.GetSceneByName("Level").isLoaded) {
			SceneManager.LoadScene("Level", LoadSceneMode.Additive);
		}
		level = Object.FindObjectOfType<Level>();
		timeLoopController.Start();

		StartNextLoopAfterLoadingLevel();
	}

	void Update () {
		if(state == GameState.ShowingObjective) {
			objectivePanelTimer.Loop();
		} else if(state == GameState.Playing) {
			timeLoopController.Update();
		}
	}

	void ShowObjectivePanel () {
		state = GameState.ShowingObjective;

	    //objectivePanelTimer.Set(MainController.Instance.debugMode ? 0.1f : 3);
        objectivePanelTimer.Set(AudioController.Instance.triggerGamePhase()+3);
        objectivePanelTimer.Start();
        UIController.Instance.ShowPopup();
    }

	void CompleteObjectivePanelTimer () {
		UIController.Instance.HidePopup();
		StartLoop();
	}

	void StartLoop () {
		timeLoopController.StartLoop();
		state = GameState.Playing;
	}

	public void OnCompleteLoop () {
        ConvertActiveCharacterToPassive();
		SceneManager.UnloadScene(level.gameObject.scene.name);
		characters.Clear();
		if(timeLoopController.timeLoops.Count < timeLoopController.maxLoops) {
			PlayerController.NextPlayer();
			StartCoroutine(StartNextLoop());
		} else {
			UIController.Instance.ShowEnd();
		}
	}

	IEnumerator StartNextLoop () {
		AsyncOperation async = SceneManager.LoadSceneAsync("Level", LoadSceneMode.Additive);
        yield return async;
		level = Object.FindObjectOfType<Level>();

		StartNextLoopAfterLoadingLevel();
	}

	private void StartNextLoopAfterLoadingLevel () {
		LoadTimeLoop();

		BaseCharacter prefab = level.GetNextCharacterPrefab(timeLoopController.timeLoops.Count);
		TimeLoop timeLoop = new TimeLoop(timeLoopController.timeLoops.Count, PlayerController.CurrentPlayer);
		timeLoopController.timeLoops.Add(timeLoop);
		timeLoop.characterPrefab = prefab;
		ObjectiveController.Instance.NewRound();

		CreateActiveCharacter(prefab);
		timeLoop.characterController = activeCharacter.characterController;
		activeCharacter.characterController.objective = ObjectiveManager.GetObjective(activeCharacter.type, timeLoop);

		ShowObjectivePanel();
	}

	private void LoadTimeLoop () {
		for(int i = 0; i < timeLoopController.timeLoops.Count; i++) {
			BaseCharacter character = CreateCharacter(timeLoopController.timeLoops[i].characterPrefab);
			character.characterController = timeLoopController.timeLoops[i].characterController;
			character.characterController.timeLoop = timeLoopController.timeLoops[i];
			character.characterController.character = character;
		}

	}

	private void CreateActiveCharacter (BaseCharacter prefab) {
		activeCharacter = CreateCharacter(prefab);
		activeCharacter.transform.position = level.spawnPoints.spawnPoints.Random().transform.position;
		activeCharacter.characterController = new ActiveCharacterController(activeCharacter);
        activeCharacter.playEffect();

		activeCharacter.characterController.timeLoop = timeLoopController.currentLoop;


	}

	private BaseCharacter CreateCharacter (BaseCharacter prefab) {
		BaseCharacter character = ObjectX.InstantiateAsChild<BaseCharacter>(prefab, level.characters);
		character.name = prefab.name;
		characters.Add(character);
		return character;
	}

	void ConvertActiveCharacterToPassive () {
		activeCharacter.characterController = new PassiveCharacterController(activeCharacter, activeCharacter.characterController as ActiveCharacterController);

		timeLoopController.timeLoops.Last().characterController = activeCharacter.characterController;
	}
}