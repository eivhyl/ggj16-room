﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

    public static int TotalPlayers;
    public static int CurrentPlayer;

    void Start()
    {
        
    }

    public static void Load(int numberOfPlayers)
    {
        TotalPlayers = numberOfPlayers;
        CurrentPlayer = 1;
    }

    public static void NextPlayer()
    {
        if (CurrentPlayer != TotalPlayers)
        {
            CurrentPlayer++;
        }
        else
        {
            CurrentPlayer = 1;
        }
    }

    public static string ReturnCurrentPlayer()
    {
        switch (CurrentPlayer)
        {
            case 1:
                return "PLAYER ONE";
            case 2:
                return "PLAYER TWO";
            case 3:
                return "PLAYER THREE";
            case 4:
                return "PLAYER FOUR";
            case 5:
                return "PLAYER FIVE";
            default:
                return "HILARIOUS CRITICAL FAILURE";
        }
    }
}
