﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class UIController : MonoSingleton<UIController>
{
    public Text timeText;
    public Text debugText;

    public object popup;

    public GameObject popupImage;
    public Text characterText;
    public Text objectiveText;
    public Text playerText;

    public GameObject endObject;
    public Text winnerText;

    public Image pictogramHolder;

 
    void Start()
    {
        endObject.SetActive(false);
        GameController.Instance.timeLoopController.OnCompleteLoop += OnCompleteLoop;
        ObjectiveController.Instance.OnKillCharacter += OnKillCharacter;
    }

    void Update()
    {
        timeText.text = (GameController.Instance.timeLoopController.loopTimer.targetTime - GameController.Instance.timeLoopController.loopTimer.currentTime).RoundTo(2).ToString();
    }

    public void setPictogram(string objective)
    {
        switch (objective)
        {
            case "STEAL THE DIAMOND!":
                pictogramHolder.sprite = new Sprite();
                break;
            default:  Debug.Log(objective);
                break;
        }
        
    }


    public void ShowPopup()
    {
        string upcomingChar = GameController.Instance.activeCharacter.name.ToUpper();
        string adjectiveChar = GetAdjective(upcomingChar);
        popupImage.SetActive(true);
        characterText.text = adjectiveChar + " " + upcomingChar + " SERVES THY BIDDING";
        var objective = GameController.Instance.activeCharacter.characterController.objective.ToString();
        objectiveText.text = objective;
        setPictogram(objective);
        playerText.text = PlayerController.ReturnCurrentPlayer() + " IS NEXT";
    }

    private void OnKillCharacter(BaseCharacter killer, BaseCharacter victim)
    {
        runDebugList();
    }

    public void OnCompleteLoop(TimeLoop dontuse)
    {
        runDebugList();
    }

    private void runDebugList()
    {
        debugText.text = "Debug Objectives";
        foreach (var objective in ObjectiveController.Instance.completedObjectives)
        {
            
            debugText.text += "\n" + objective;
        }
    }

    private string GetAdjective(string upcomingChar)
    {
        int randomNumber = Random.Range(0, 10);

        switch (upcomingChar)
        {
            case "SHEEP":
                return randomNumber > 5 ? "A DIM" : "A VERY DIM";
            case "OCCULTIST":
                return randomNumber > 5 ? "A RABID" : "A FROTHING";
            case "KNIGHT":
                return randomNumber > 5 ? "A OVERCONFIDENT" : "A DEFEANINGLY LOUD";
            case "BUSINESSMAN":
                return randomNumber > 5 ? "A PUNCTUAL" : "A SOCIALLY AWKWARD";
            case "BABY":
                return randomNumber > 5 ? "A TOTALLY 100 PERCENT NORMAL" : "A QUIETLY TICKING";
            default:
                return "A SACRIFICIAL";
        }
    }

    public void HidePopup()
    {
        popupImage.SetActive(false);
    }

    public void ShowEnd()
    {
        endObject.SetActive(true);
        winnerText.text = "PLAYER ONE WINS";
    }

    public void EndRematch()
    {
        endObject.SetActive(false);
        SceneManager.UnloadScene("Main");
        SceneManager.UnloadScene("Game 2D");
        SceneManager.UnloadScene("Level");
        SceneManager.UnloadScene("UI");
        SceneManager.LoadScene("Main", LoadSceneMode.Additive);
        SceneManager.LoadSceneAsync("Game 2D", LoadSceneMode.Additive);
        SceneManager.LoadSceneAsync("Level", LoadSceneMode.Additive);
        SceneManager.LoadSceneAsync("UI", LoadSceneMode.Additive);
    }

    public void EndExit()
    {
        endObject.SetActive(false);

        SceneManager.UnloadScene("Main");
        SceneManager.UnloadScene("Game 2D");
        SceneManager.UnloadScene("Level");
        SceneManager.UnloadScene("UI");
        SceneManager.LoadScene("Menu", LoadSceneMode.Additive);
    }
}
