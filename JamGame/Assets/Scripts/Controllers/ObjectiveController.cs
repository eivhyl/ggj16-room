using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class ObjectiveController : MonoSingleton<ObjectiveController>
{
	public delegate void OnKillCharacterEvent (BaseCharacter killer, BaseCharacter victim);
	public event OnKillCharacterEvent OnKillCharacter;

	public delegate void OnReachDestinationEvent (BaseCharacter character, TargetDestination destination);
	public event OnReachDestinationEvent OnReachDestination;


    public List<bool> completedObjectives { get; private set; }

    void Awake() {
        completedObjectives = new List<bool>();
    }

    public void UpdateObjective(TimeLoop timeLoop, bool complete) {
		if(!completedObjectives[timeLoop.index]) {
			completedObjectives[timeLoop.index] = complete;
		}
    }

    public void NewRound()
    {
		completedObjectives.Clear();
        for (int i = 0; i < GameController.Instance.timeLoopController.timeLoops.Count; i++) {
            completedObjectives.Add(false);
        }
    }
	
	public void KillCharacter (BaseCharacter killer, BaseCharacter victim) {
		if(OnKillCharacter != null) {
			OnKillCharacter(killer, victim);
		}
    }

	public void ReachDestination (BaseCharacter character, TargetDestination destination) {
		if(OnReachDestination != null) {
			OnReachDestination(character, destination);
		}
    }
}
