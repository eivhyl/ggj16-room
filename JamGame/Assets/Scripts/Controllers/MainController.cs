﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class MainController : MonoSingleton<MainController> {
	public bool debugMode = false;

	public enum MainState {
		MainMenu,
		Playing
	}
	public MainState state;
    public AudioController audioController;

    void Start() {
//		if(!debugMode || !Application.isEditor)
        	SceneManager.LoadScene("Menu");
    }

}
