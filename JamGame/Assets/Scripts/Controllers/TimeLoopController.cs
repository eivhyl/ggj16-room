using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class TimeLoopController {
	public float loopTime = 5f;

	public int maxLoops = 20;
	public Timer loopTimer {get;private set;}

	public List<TimeLoop> timeLoops = new List<TimeLoop>();
	public TimeLoop currentLoop {
		get {
			return timeLoops.Last();
		}
	}

	public delegate void OnStartLoopEvent (TimeLoop loop);
	public event OnStartLoopEvent OnStartLoop;

	public delegate void OnCompleteLoopEvent (TimeLoop loop);
	public event OnCompleteLoopEvent OnCompleteLoop;

	public void StartLoop () {
		loopTimer.Set(loopTime);
		loopTimer.Start();

		if(OnStartLoop != null) OnStartLoop(currentLoop);
	}

	public void Awake () {
		loopTimer = new Timer(loopTime);
		loopTimer.OnComplete += OnCompleteLoopTimer;
	}


	public void Start () {
		
	}

	public void Update () {
		loopTimer.Loop();
	}

	void OnCompleteLoopTimer () {
		if(OnCompleteLoop != null) OnCompleteLoop(currentLoop);
		GameController.Instance.OnCompleteLoop();
	}
}

[System.Serializable]
public class TimeLoop {
	public int index;
	public int playerIndex;
	public BaseCharacter characterPrefab;
	public BaseCharacterController characterController;

	public TimeLoop (int index, int playerIndex) {
		this.index = index;
		this.playerIndex = playerIndex;
	}
	public TimeLoop (int index, int playerIndex, BaseCharacter characterPrefab, BaseCharacterController characterController) {
		this.index = index;
		this.playerIndex = playerIndex;
		this.characterPrefab = characterPrefab;
		this.characterController = characterController;
	}
}