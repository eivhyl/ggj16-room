﻿using UnityEngine;
using UnityEngine.SceneManagement;

using UnityEngine.UI;
using System.Collections;

namespace Assets.Scripts.Controllers
{
    public class MenuController : MonoBehaviour
    {
        public int Players;
        public GameObject Menu;
        public static Text PlayerText;
        public string PlayerOutput; 

        // Use this for initialization
        void Start ()
        {
            Players = 2;
            PlayerText = GameObject.Find("PlayersButton").GetComponentInChildren<Text>();

        }
	
        // Update is called once per frame
        void Update () {
	        
        }

        public void Begin()
        {
            //Don't mess with this
            Menu.SetActive(false);
            //AudioController.Instance.setState(true);

            SceneManager.LoadScene("Game 2D", LoadSceneMode.Additive);
            SceneManager.LoadScene("Level", LoadSceneMode.Additive);
            SceneManager.LoadScene("UI", LoadSceneMode.Additive);

            PlayerController.Load(Players);
            SceneManager.UnloadScene("Menu");

        }

        public void PlayerChange()
        {
            if (Players == 0 || Players > 4)
            {
                Players = 2;    
            }
            else
            {
                Players++;
            }

            switch (Players)
            {
                case 2:
                    PlayerOutput = "TWO ";
                    break;
                case 3:
                    PlayerOutput = "THREE ";
                    break;
                case 4:
                    PlayerOutput = "FOUR ";
                    break;
                case 5:
                    PlayerOutput = "FIVE ";
                    break;
                default:
                    PlayerOutput = "AN ERROR OF ";
                    break;
            }

            PlayerText.text = PlayerOutput + "PLAYERS";

        }

        public void Exit()
        {
            Debug.Log("Quit");
            Application.Quit();
        }
    }
}
