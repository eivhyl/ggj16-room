﻿using UnityEngine;
using System.Collections;

public class GUIController : MonoBehaviour {
	public GUISkin guiSkin;
	void OnGUI () {
		GUI.skin = guiSkin;
		GUI.Label(new Rect(Screen.width * 0.5f * 0.5f, Screen.height - 160, Screen.width * 0.5f, 40), "Time is " + TimeController.inst.time.RoundTo(2));
		GUI.Label(new Rect(Screen.width * 0.5f * 0.5f, Screen.height - 120, Screen.width * 0.5f, 40), "TimeScale is " + TimeController.inst.timeScale.RoundTo(1));
		GUI.Label(new Rect(Screen.width * 0.5f * 0.5f, Screen.height - 80, Screen.width * 0.5f, 40), "TimeState is " + TimeController.inst.state);
		float timeScale = GUI.HorizontalSlider(new Rect(Screen.width * 0.5f * 0.5f, Screen.height - 40, Screen.width * 0.5f, 40), TimeController.inst.timeScale, -5, 5);
		if (timeScale != TimeController.inst.timeScale) {
			// Set a bool or call a funciton
			TimeController.inst.timeScale = timeScale.RoundTo(1);
		}
	}
}
