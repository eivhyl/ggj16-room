﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;

[System.Serializable]
public class RewindableProperty <T> {
	public List<RewindablePropertyKey<T>> keys;

	public Type type {
        get {
            return typeof(T);
        }
    }

	public int length {
		get {
			return keys.Count;
		} set {}
	}

	public RewindableProperty() {
		keys = new List<RewindablePropertyKey<T>>();
    }

	public void AddKey(T _value, float time) {
		RewindablePropertyKey<T> propertyEvent = new RewindablePropertyKey<T>(_value, time);

		if(keys.Count == 0 || keys[keys.Count-1].time < propertyEvent.time){
			keys.Add(propertyEvent);
		} else {
			int closestIndex = ClosestIndexToTime(propertyEvent.time);
			if(keys[closestIndex].time == propertyEvent.time) {
				keys[closestIndex] = propertyEvent;
			} else if(keys[closestIndex].time > propertyEvent.time) {
				closestIndex -= 1;
			}
			keys.Insert(closestIndex, propertyEvent);
		}
    }

	private int ClosestIndexToTime(float target) {
		int low = 0;
		int high = keys.Count - 1;
		
		if (high < 0)
			Debug.LogError("The array cannot be empty");
		while (low <= high) {
			int mid = (int)((uint)(low + high) >> 1);
			
			float midVal = keys[mid].time;
			
			if (midVal < target)
				low = mid + 1;
			else if (midVal > target)
				high = mid - 1;
			else
				return mid; // key found
		}
		if(high < 0) {
			high = 0;
		}
		return high;
	}

    public void RemoveKey(int i){
		keys.RemoveAt(i);
    }

    public T GetValueAtTime(float time){
		if(keys.IsNullOrEmpty())
			return default(T);
			
		int closestIndex = ClosestIndexToTime(time);
		if(keys[closestIndex].time < time && keys.ContainsIndex(closestIndex+1)) {
			closestIndex += 1;
		}
		return keys[closestIndex].value;
    }

	public void RemoveKeysBeforeTime(float time){
		for(int i = 0; i < keys.Count; i++) {
    		if(keys[i].time < time){
    			RemoveKey(i);
    			i--;
    		}
    	}
    }

    public void RemoveKeysPastTime(float time){
		for(int i = keys.Count-1; i > 0; i--) {
    		if(keys[i].time > time){
    			RemoveKey(i);
    		}
    	}
    }
}

[System.Serializable]
public class RewindablePropertyKey <T> {
	public float time;
	public T value;

	public RewindablePropertyKey(T _value, float _time) {
		value = _value;
		time = _time;
    }
}