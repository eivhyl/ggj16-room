﻿using UnityEngine;
using System.Collections;

public interface IRewindable {
	float birthTime { get; set; }
	
	void Play();
	void Record(float time);
	void Rewind(float time);
}
