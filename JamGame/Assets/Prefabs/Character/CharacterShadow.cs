﻿using UnityEngine;
using System.Collections;

public class CharacterShadow : MonoBehaviour {
	[HideInInspector]
	public BaseCharacter character;
	public SpriteRenderer spriteRenderer;

	private void Awake () {
		character = GetComponentInParent<BaseCharacter>();
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	private void Start () {
		if(character.characterController.IsTypeOf<ActiveCharacterController>()) {
			spriteRenderer.color = Color.white;
		} else {
			spriteRenderer.color = ((Color)new HSLColor((character.characterController.timeLoop.playerIndex/PlayerController.TotalPlayers) * 360, 1, 0.5f)).SetAlpha(0.25f);
		}
	}
}
