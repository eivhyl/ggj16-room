﻿using UnityEngine;
using System;
using System.Collections;
using System.Reflection;

/// <summary>
/// Calls Init, Begin and Loop on scripts. Scripts using that pattern should have controllers, so this script should only be used for testing.
/// </summary>
public class ScriptExecutor : MonoBehaviour {
	
	public bool init = true;
	public bool begin = true;
	public bool loop = true;
	public bool lateLoop = true;
	public bool fixedLoop = true;
	public bool drawGUI = true;
	
	public UnityEngine.Object script;
	
	void Awake () {
		if(!Application.isEditor)
		Debug.LogWarning("Script Executor exists on "+transform.HierarchyPath(1)+". This script should only ever be used for testing.");
		
		if(isActiveAndEnabled && init && script != null) {
			MethodInfo method = script.GetType().GetMethod("Init", new Type[0]);
			if(method != null)
				method.Invoke(script, new object[0]);
		}
	}

	void Start () {
		if(isActiveAndEnabled && begin && script != null) {
			MethodInfo method = script.GetType().GetMethod("Begin", new Type[0]);
			if(method != null)
				method.Invoke(script, new object[0]);
		}
//			BroadcastMessage("Begin", SendMessageOptions.DontRequireReceiver);
	}
	
	void Update () {
		if(isActiveAndEnabled && loop && script != null) {
			MethodInfo method = script.GetType().GetMethod("Loop", new Type[0]);
			if(method != null)
				method.Invoke(script, new object[0]);
		}
	}

	void LateUpdate () {
		if(isActiveAndEnabled && lateLoop && script != null) {
			MethodInfo method = script.GetType().GetMethod("LateLoop", new Type[0]);
			if(method != null)
				method.Invoke(script, new object[0]);
		}
	}
	
	void FixedUpdate () {
		if(isActiveAndEnabled && fixedLoop && script != null) {
			MethodInfo method = script.GetType().GetMethod("FixedLoop", new Type[0]);
			if(method != null)
				method.Invoke(script, new object[0]);
		}
	}
	
	void OnGUI () {
		if(isActiveAndEnabled && drawGUI && script != null) {
			MethodInfo method = script.GetType().GetMethod("DrawGUI", new Type[0]);
			if(method != null)
				method.Invoke(script, new object[0]);
		}
	}
}
