﻿using UnityEngine;

public class KeyPressSequence : MonoBehaviour
{
    public float timeKey = 1f, timeCode = 10f;
    public GameObject receiver;
    public string message;

    public KeyCode[] keycodes;
    int index = 0;
    float timeSinceStartCode = 0f, timeSinceLastKey = 0f;

    void Awake() {
        /*keycodes = new KeyCode[] {
            KeyCode.UpArrow,
            KeyCode.UpArrow,
            KeyCode.DownArrow,
            KeyCode.DownArrow,
            KeyCode.LeftArrow,
            KeyCode.RightArrow,
            KeyCode.LeftArrow,
            KeyCode.RightArrow,
            KeyCode.B,
            KeyCode.A
        };*/
    }

    void OnEnable() {
        if (receiver == null)
            enabled = false;
    }

    void Update() {
        timeSinceLastKey += Time.deltaTime;
        timeSinceStartCode += Time.deltaTime;
        if (Input.anyKeyDown == false) return;
        if (Input.GetKeyDown(keycodes[index]) == false || timeSinceStartCode >= timeCode || timeSinceLastKey >= timeKey) {
            index = 0;
        }
        if (Input.GetKeyDown(keycodes[index])) {
            if (index == 0) {
                timeSinceStartCode = 0f;
            }
            timeSinceLastKey = 0f;
            index++;
            if (index >= keycodes.Length) {
                print(123);
                if (receiver != null)
                    receiver.SendMessage(message, SendMessageOptions.DontRequireReceiver);
                index = 0;
            }
        }
    }
}