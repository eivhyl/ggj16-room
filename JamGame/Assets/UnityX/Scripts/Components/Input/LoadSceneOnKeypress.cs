﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadSceneOnKeypress : MonoBehaviour {
	
	[SceneName]
	public string sceneName;
	public KeyCode key = KeyCode.Return;
	
	void LateUpdate () {
		if(Input.GetKeyDown(key)) SceneManager.LoadScene(sceneName);
	}
}
