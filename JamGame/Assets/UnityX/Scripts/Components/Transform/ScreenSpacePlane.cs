using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ScreenSpacePlane : MonoBehaviour {
	public new Camera camera;
	public float distance = 10;
	public bool x = true;
	public bool y = true;
	public bool width = true;
	public bool height = true;
	public Rect screenRect = new Rect(0,0,1,1);
	//If our gameobject is ever a cube with depth, this needs to be considered.
    float goDepth = 0;
    Vector3 worldTopLeft;
    Vector3 worldBottomRight;
 
    void Awake () {
		if(camera == null){
			camera = Camera.main;
		}

		FitBounds();
    }

	public void SetScreenRect (Rect newScreenRect) {
		screenRect = newScreenRect;
		FitBounds();
	}

	public void FitBounds () {
		goDepth = transform.localScale.z;
		if(camera.orthographic){
			//distance -= (goDepth*0.5f);
			worldTopLeft = camera.ViewportToWorldPoint(new Vector3(screenRect.x,((1-screenRect.y)-screenRect.height),distance));
			worldBottomRight = camera.ViewportToWorldPoint(new Vector3(screenRect.x+screenRect.width,((1-screenRect.y)-screenRect.height)+screenRect.height,distance));
			transform.localScale = new Vector3(width?(worldBottomRight.x-worldTopLeft.x).CeilTo(2):transform.localScale.x, height?(worldBottomRight.y-worldTopLeft.y).CeilTo(2):transform.localScale.y, goDepth);
	 		transform.position = new Vector3(x?(worldTopLeft.x + transform.localScale.x*0.5f).FloorTo(2):transform.position.x, y?(worldTopLeft.y + transform.localScale.y*0.5f).FloorTo(2):transform.position.y, distance) + new Vector3(x?camera.transform.position.x:0, y?camera.transform.position.y:0, camera.transform.position.z);
		} else {
			//distance -= (goDepth*0.5f);
			worldTopLeft = camera.ViewportToWorldPoint(new Vector3(screenRect.x,((1-screenRect.y)-screenRect.height),distance));
			worldBottomRight = camera.ViewportToWorldPoint(new Vector3(screenRect.x+screenRect.width,((1-screenRect.y)-screenRect.height)+screenRect.height,distance));
			transform.localScale = new Vector3(width?(worldBottomRight.x-worldTopLeft.x).CeilTo(2):transform.localScale.x, height?(worldBottomRight.y-worldTopLeft.y).CeilTo(2):transform.localScale.y, goDepth);
	 		transform.position = new Vector3(x?(worldTopLeft.x + transform.localScale.x*0.5f).FloorTo(2):transform.position.x, y?(worldTopLeft.y + transform.localScale.y*0.5f).FloorTo(2):transform.position.y, distance) + new Vector3(x?camera.transform.position.x:0, y?camera.transform.position.y:0, camera.transform.position.z);
		}
	}
}
