﻿using UnityEngine;
using System.Collections;

public enum TimeState {
	Paused,
	Playing,
	Rewinding
}
