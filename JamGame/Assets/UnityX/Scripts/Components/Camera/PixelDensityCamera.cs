//From http://youtu.be/HM17mAmLd7k?t=41m14s

using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PixelDensityCamera : MonoBehaviour {

	public float pixelsToUnits = 100;
	public float zoom = 1;

	void Update () {
		GetComponent<Camera>().orthographicSize = Screen.height / pixelsToUnits / 2 / zoom;
	}
}
