using UnityEngine;
using System.Collections;

public class ObliqueCameraProjection : MonoBehaviour {
	private Matrix4x4 originalProjection;
	public Vector2 slideViewport;
	public Vector2 slideFrustum;
	public Vector2 slideFarClip; // compound slideViewport and slideFrustum
	public Vector2 skew;
	public Vector2 scale;
	
	void Awake ()
	{
		originalProjection = GetComponent<Camera>().projectionMatrix;
	}
	
	void LateUpdate ()
	{
		CalculateProjection (originalProjection);
	}
	
	void OnPostRender ()
	{
		GetComponent<Camera>().projectionMatrix = originalProjection;
	}
	
	[ContextMenu ("Calculate Projection")]
	public void CalculateProjectionInEditor ()
	{
		originalProjection = GetComponent<Camera>().projectionMatrix;
		CalculateProjection (originalProjection);
	}
	
	void CalculateProjection (Matrix4x4 p)
	{
		//		Matrix4x4 p = originalProjection;
		p.m00 += scale.x;
		p.m11 += scale.y;
		
		p.m01 += skew.x;
		p.m10 += skew.y;
		
		p.m02 += slideViewport.x;
		p.m12 += slideViewport.y;
		
		p.m03 += slideFrustum.x;
		p.m13 += slideFrustum.y;
		
		p.m02 += slideFarClip.x;
		p.m12 += slideFarClip.y;
		p.m03 += slideFarClip.x / 2;
		p.m13 += slideFarClip.y / 2;
		
		GetComponent<Camera>().projectionMatrix = p;
	}
}