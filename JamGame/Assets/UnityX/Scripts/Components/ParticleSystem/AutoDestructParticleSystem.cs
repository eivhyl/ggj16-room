﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AutoDestructParticleSystem : MonoBehaviour {
	private ParticleSystem[] ps;
	
	public void Start() {
		ps = GetComponentsInChildren<ParticleSystem>();
	}
	
	public void Update() {
		bool alive = false;
		for(int i = 0; i < ps.Length; i++) {
			if(ps[i] && ps[i].IsAlive()) {
				alive  = true;
			}
		}
		
		if(!alive)
		Destroy(gameObject);
	}
}