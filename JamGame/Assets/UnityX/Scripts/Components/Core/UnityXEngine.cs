using UnityEngine;
using System.Collections;

public class UnityXEngine : MonoBehaviour {
	
	public bool selfActuating = false;
	
	private InputX input;
	private ScreenX screen;
	private ScreenFade screenFade;
	private QualityManager quality;
	private Localization localization;
	
	public void Init () {
		input = GetComponentInChildren<InputX>();
		screen = GetComponentInChildren<ScreenX>();
		screenFade = GetComponentInChildren<ScreenFade>();
		quality = GetComponentInChildren<QualityManager>();
		localization = GetComponentInChildren<Localization>();
		
		input.Init ();
		screen.Init ();
		screenFade.Init ();
		quality.Init ();
		localization.Init ();
	}
	
	public void Begin () {
		input.Begin ();
	}
	
	public void Loop () {
		input.Loop ();
		screen.Loop ();
		screenFade.Loop ();
	}
	
	public void LateLoop () {
		input.Loop ();
	}
	
	void Awake () {
		if(selfActuating) Init ();
	}
	
	void Start () {
		if(selfActuating) Begin ();
	}
	
	void Update () {
		if(selfActuating) Loop ();
	}
	
	void LateUpdate () {
		if(selfActuating) LateLoop ();
	}
}
