﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A blank MonoBehaviour that serves as a proxy to non-MonoBehaviours that wish to use MonoBehaviour functions, such as StartCoroutine or FindObjectOfType.
/// </summary>
public class MonoBehaviourProxy : MonoSingleton<MonoBehaviourProxy> {}