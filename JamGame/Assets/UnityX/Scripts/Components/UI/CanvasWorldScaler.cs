﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent(typeof(Canvas))]
public class CanvasWorldScaler : MonoBehaviour {
	
	public Vector2 worldScale;
	public Vector2 pixelScale;
	
	public enum ScaleMode {
		StretchToFill,
		ScaleToFitWorldScale,
		ScaleToFitPixelScale
	}
	public ScaleMode scaleMode;
	
	private RectTransform rectTransform;
	
	void OnEnable () {
		rectTransform = GetComponent<RectTransform>();
	}
	
	void Update () {
		Vector2 scaleAspect = Vector2.one;
		if(scaleMode == ScaleMode.ScaleToFitPixelScale) {
			if(pixelScale.x > pixelScale.y) {
				scaleAspect.y = Mathf.Min(pixelScale.x, pixelScale.y)/Mathf.Max(pixelScale.x, pixelScale.y);
			} else {
				scaleAspect.x = Mathf.Min(pixelScale.x, pixelScale.y)/Mathf.Max(pixelScale.x, pixelScale.y);
			}
		} else if(scaleMode == ScaleMode.ScaleToFitWorldScale) {
			if(worldScale.x > worldScale.y) {
				scaleAspect.x = Mathf.Min(worldScale.x, worldScale.y)/Mathf.Max(worldScale.x, worldScale.y);
			} else {
				scaleAspect.y = Mathf.Min(worldScale.x, worldScale.y)/Mathf.Max(worldScale.x, worldScale.y);
			}
		}
	
		rectTransform.SetWidth(pixelScale.x);
		rectTransform.SetHeight(pixelScale.y);
		float localScaleX = (worldScale.x / pixelScale.x) * scaleAspect.x;
		float localScaleY = (worldScale.y / pixelScale.y) * scaleAspect.y;
		rectTransform.localScale = new Vector3(localScaleX, localScaleY, 1);
	}
	
	void OnDrawGizmos () {
		if(scaleMode != ScaleMode.StretchToFill) {
			Color savedColor = Gizmos.color;
			Gizmos.color = Color.white;
			GizmosX.DrawWireRect(rectTransform.position, rectTransform.rotation, worldScale);
			Gizmos.color = savedColor;
		}
	}
}
