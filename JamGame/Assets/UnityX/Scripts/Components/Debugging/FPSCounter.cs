﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(GUIText))]
public class FPSCounter : MonoBehaviour {
	public static FPSCounter inst;
	public float frequency = 0.5f;
 
	public int fps { get; protected set; }

	private GUIText text;

	void Awake () {
		inst = this;
		text = GetComponentInChildren<GUIText>();
	}

	private void Start() {
		StartCoroutine(FPS());
	}

	private IEnumerator FPS() {
		for(;;){
			// Capture frame-per-second
			int lastFrameCount = Time.frameCount;
			float lastTime = Time.realtimeSinceStartup;
			yield return new WaitForSeconds(frequency);
			float timeSpan = Time.realtimeSinceStartup - lastTime;
			int frameCount = Time.frameCount - lastFrameCount;
			fps = Mathf.RoundToInt(frameCount / timeSpan);
		}
	}

	void OnGUI () {

			if(fps<8){
				text.color = Color.red;
			} else if(fps<14){
				text.color = Color.yellow;
			} else if(fps<18){
				text.color = Color.green;
			} else if(fps<24){
				text.color = Color.blue;
			} else {
				text.color = Color.white;
			}

			text.text = fps.ToString() + " fps";
	}
}