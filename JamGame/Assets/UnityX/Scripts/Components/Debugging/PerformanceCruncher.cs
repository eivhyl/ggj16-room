﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Tests performance by rapidly creating and destroying new GameObjects. 
///	Useful for checking your game still functions well under pressure.
/// </summary>
public class PerformanceCruncher : MonoBehaviour {
	/// <summary>
	/// The random slowdown, measured in create+destroys/frame (otherwise known as units of milliclusterfuck)
	/// </summary>
	[MinMax(0,100000)]
	public Vector2 randomSlowdown = new Vector2(0,5000);
	public AnimationCurve randomSlowdownChance = AnimationCurveX.EaseIn(1, 10000);
	
	
	void Update () {
		int r = randomSlowdown.Random().RoundToInt();
		r = randomSlowdownChance.Evaluate(Random.value).RoundToInt();
		for(int i = 0; i < r; i++) {
			GameObject go = new GameObject();
			Destroy(go);
		}
	}
}
