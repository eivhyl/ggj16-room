using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Text.RegularExpressions;

[System.Serializable]
public class TypedText {
	
	public enum SplitMode {
		Word,
		Character,
		Custom
	}

	[Tooltip("The text typing mode")]
	public SplitMode splitMode = SplitMode.Word;
	[Tooltip("The string used to divide TypedText in Custom TypedTextSplitMode")]
	public string customSplitString = ",";

	[HideInInspector]
	public string text;
	
	private string targetText;
	
	[HideInInspector]
	public bool typing = false;

	[Tooltip("Type delay for standard strings")]
	public TimeDelay defaultTypeDelay;

	[Tooltip("Type delay for custom strings")]
	public List<CustomStringTimeDelay> customTypeDelays;
	public Dictionary<string, TimeDelay> customTypeDelayDictionary;

	/// <summary>
	/// The custom rich text tags.
	/// Useful for custom markup.
	/// Currently only works in Character mode.
	/// </summary>
	public List<CustomRichTextTag> CustomRichTextTags;

	public float lineBreakTimeMultiplier = 2f;
	
	[Tooltip("Modifies type speed")]
	public float speedMultiplier = 1;

	private Timer nextStringTimer;

	public int currentSplitStringIndex;
	private string[] splitString;

	public string currentString {
		get {
			if(splitString.ContainsIndex(currentSplitStringIndex)) return splitString[currentSplitStringIndex];
			else return "";
		}
	}

	public delegate void OnTextChangeEvent(string fullText);
	public event OnTextChangeEvent OnTextChange;

	public delegate void OnTypeTextEvent(string newText);
	public event OnTypeTextEvent OnTypeText;

	public delegate void OnCompleteTypingEvent();
	public event OnCompleteTypingEvent OnCompleteTyping;

	public TypedText () {
		Init();
	}

	public TypedText (string myTargetText) {
		TypeText(myTargetText);
	}

	public TypedText (string myTargetText, float mySpeedMultiplier) {
		TypeText(myTargetText, mySpeedMultiplier);
	}

	public void TypeText (string myText) {
		TypeText(myText, speedMultiplier);
	}

	public void TypeText (string myText, float mySpeedMultiplier) {
		Init();
		if(string.IsNullOrEmpty(myText)) return;
		targetText += myText;
		speedMultiplier = mySpeedMultiplier;
		
		typing = true;
		currentSplitStringIndex = 0;
		splitString = GetSplitText(myText);
		TypeCurrentString();
	}

	public void Init () {
		if(defaultTypeDelay == null)
		defaultTypeDelay = new TimeDelay(0.2f);
		CreateCustomTypeDelayDictionary();
	}

	public void Loop () {
		if(!typing || nextStringTimer == null) return;
		nextStringTimer.Loop(Time.deltaTime * speedMultiplier);
	}

	public void ShowInstantly () {
		text = targetText;
		TextChanged(text);
		CompleteTyping();
	}

	public void Clear () {
		targetText = text = "";
		TextChanged(text);
		FinishTyping();
	}

	//STATES
	private void TypeCurrentString () {
		Type(currentString);
		FinishTypingString();
	}

	private void Type (string stringToAdd) {
		text += stringToAdd;
		// if(splitMode == TypedTextSplitMode.Word) {
		// 	text += " ";
		// }
		if(OnTypeText != null) OnTypeText(stringToAdd);
		TextChanged(text);
	}

	private void FinishTypingString () {
		if(currentSplitStringIndex >= splitString.Length-1){
			CompleteTyping();
		} else {
			StartTypingNextString();
		}
	}

	private void StartTypingNextString () {
		currentSplitStringIndex++;
		StartNextStringTypeTimer();
	}

	private float GetNextStringTypeTime () {
		float typeTime;
		if (customTypeDelayDictionary.ContainsKey(currentString)) {
			typeTime = customTypeDelayDictionary[currentString].GetDelay();
		} else {
			if(currentString == " ") typeTime = 0;
			else {
				typeTime = defaultTypeDelay.GetDelay();
				if(currentString == "\n") typeTime *= lineBreakTimeMultiplier;
			}
		}
		return typeTime;
	}

	private void StartNextStringTypeTimer () {
		nextStringTimer = new Timer(GetNextStringTypeTime());
		nextStringTimer.OnComplete += TypeCurrentString;
		nextStringTimer.Start();
	}

	private void CompleteTyping () {
		FinishTyping();
		if(OnCompleteTyping != null) OnCompleteTyping();
	}

	private void FinishTyping () {
		splitString = new string[0];
		nextStringTimer = null;
		typing = false;
	}

	//EVENTS
	private void TextChanged (string text) {
		if(OnTextChange != null) OnTextChange(text);
	}

	//SPLITTING
	private string[] GetSplitText (string myText) {
		if(splitMode == SplitMode.Word) {
			return SplitByWord(myText);
		} else if(splitMode == SplitMode.Character) {
			return SplitByCharacter(myText);
		} else {
			return SplitByCustom(myText);
		}
	}

	private string[] SplitByWord (string _input) {
		string pattern = @"^(\s+|\d+|\w+|[^\d\s\w]+)+$";
		Regex regex = new Regex(pattern);
		List<string> tmpList = new List<string>();
		if (regex.IsMatch(_input)) {
			Match match = regex.Match(_input);
			foreach (Capture capture in match.Groups[1].Captures) {
				// if (!capture.Value.IsNullOrWhiteSpace())
				tmpList.Add(capture.Value);
			}
		}
		return tmpList.ToArray<string>();
	}

	private string[] SplitByCharacter (string _input) {
		return _input.ToStringArray();
	}

	private string[] SplitByCustom (string _input) {
		return _input.Split(new string[] { customSplitString }, StringSplitOptions.None);
	}

	//CUSTOM DELAY DICTIONARY
	private void CreateCustomTypeDelayDictionary () {
		customTypeDelayDictionary = new Dictionary<string, TimeDelay>(StringComparer.OrdinalIgnoreCase);
		if(customTypeDelays.IsNullOrEmpty()) return;
		for (int i = 0; i < customTypeDelays.Count; i++) {
			customTypeDelayDictionary.Add(customTypeDelays[i].customString, customTypeDelays[i].textDelay);
		}
	}

	[System.Serializable]
	public abstract class BaseTimeDelay {
		
		public abstract float GetDelay ();
	}

	[System.Serializable]
	public class TimeDelay : BaseTimeDelay {
		public float delay;
		
		public TimeDelay (float delay) {
			this.delay = delay;
		}
		
		public override float GetDelay () {
			return delay;
		}
	}
	
	[System.Serializable]
	public class RandomTimeDelay : BaseTimeDelay {
		public float minTypeTime;
		public float maxTypeTime;
		
		public RandomTimeDelay (float minTypeTime, float maxTypeTime)  {
			this.minTypeTime = minTypeTime;
			this.maxTypeTime = maxTypeTime;
		}
		
		public override float GetDelay () {
			return UnityEngine.Random.Range(minTypeTime, maxTypeTime);
		}
	}
	
	[System.Serializable]
	public class CustomStringTimeDelay {
		public string customString = "\n";
		public TimeDelay textDelay;
	}


	/// These allow you to define custom opening and closing tags, for example, "*" and "* , or "<q>" and "</q>",
	[System.Serializable]
	public class CustomRichTextTag {
		public string openingTag;
		public string closingTag;
		public RichTextOptions richText;

		public CustomRichTextTag (string openingTag, string closingTag, RichTextOptions richText) {
			this.openingTag = openingTag;
			this.closingTag = closingTag;
			this.richText = richText;
		}

		public CustomRichTextTag (string openingAndClosingTag, RichTextOptions richText) : this (openingAndClosingTag, openingAndClosingTag, richText) {}
	}
}
