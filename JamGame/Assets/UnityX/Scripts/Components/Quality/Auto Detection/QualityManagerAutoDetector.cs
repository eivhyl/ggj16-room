﻿using UnityEngine;
using System.Collections;

public class QualityManagerAutoDetector : MonoBehaviour {
	
	
	/// <summary>
	/// Automatically detects the correct quality setting from a device
	/// </summary>
	/// <returns>The detect quality settings.</returns>
	public int AutoDetectQualitySetting () {
		if (Application.platform == RuntimePlatform.Android) {
			return GetQualitySettingFromAndroid();
		} else if(Application.platform == RuntimePlatform.IPhonePlayer) {
			return GetQualitySettingFromIPhone();
		} else if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor) {
			return GetQualitySettingFromEditor();
		} else {
			return GetQualitySettingFromStandalone();
		}
	}
	
	/// <summary>
	/// Gets the quality setting from editor.
	/// </summary>
	/// <returns>The quality setting from editor.</returns>
	private static int GetQualitySettingFromEditor () {
		return QualitySettings.GetQualityLevel();
	}
	
	/// <summary>
	/// Gets the quality setting from standalone.
	/// </summary>
	/// <returns>The quality setting from standalone.</returns>
	private static int GetQualitySettingFromStandalone () {
		return QualitySettings.GetQualityLevel();
	}
	
	
	/// <summary>
	/// Gets the quality setting from Android device type.
	/// </summary>
	/// <returns>The quality setting from Android device type.</returns>
	private static int GetQualitySettingFromAndroid () {
		if(SystemInfo.graphicsMemorySize < 512) {
			return 0;
		} else if(SystemInfo.graphicsMemorySize < 1024) {
			return 1;
		} else if(SystemInfo.graphicsMemorySize < 2048) {
			return 2;
		} else if(SystemInfo.graphicsMemorySize < 4096) {
			return 3;
		} 
		return 0;
	}
	
	/// <summary>
	/// Gets the quality setting from iPhone.
	/// </summary>
	/// <returns>The quality setting from iPhone.</returns>
	private static int GetQualitySettingFromIPhone () {
		#if UNITY_IPHONE
		if(
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone3G ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone3GS ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouch1Gen ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouch2Gen ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouch3Gen ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPad1Gen ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone4 ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouch4Gen ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPad2Gen ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone4S
			) {
			return 0;
		} else if(	
		          UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPad3Gen ||
		          UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone5 ||
		          UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouch5Gen ||
		          UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPadMini1Gen ||
		          UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone5C ||
		          UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPad4Gen
		          ) {
			return 1;
		} else if (
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone5S ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPadAir1 ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPadAir2 ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPadMini2Gen ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPadMini3Gen ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone6 ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone6Plus ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhoneUnknown ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPadUnknown ||
			UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPodTouchUnknown
			) {
			return 2;
		}
		#endif
		return 0;
	}
}


// 		function AutoChooseQualityLevel ()
// {
//     var shaderLevel = SystemInfo.graphicsShaderLevel;
//     var fillrate = SystemInfo.graphicsPixelFillrate;
//     var vram = SystemInfo.graphicsMemorySize;
//     var cpus = SystemInfo.processorCount;
//     if (fillrate < 0)
//     {
//         if (shaderLevel < 10)
//             fillrate = 1000;
//         else if (shaderLevel < 20)
//             fillrate = 1300;
//         else if (shaderLevel < 30)
//             fillrate = 2000;
//         else
//             fillrate = 3000;
//         if (cpus >= 6)
//             fillrate *= 3;
//         else if (cpus >= 3)
//             fillrate *= 2;
//         if (vram >= 512)
//             fillrate *= 2;
//         else if (vram <= 128)
//             fillrate /= 2;
//     }

//     var resx = Screen.width;
//     var resy = Screen.height;
//     var fillneed : float = (resx*resy + 400*300) * (30.0 / 1000000.0);
//     var levelmult : float[] = [5.0, 30.0, 80.0, 130.0, 200.0, 320.0];

//     var level = 0;
//     while (level < QualityLevel.Fantastic && fillrate > fillneed * levelmult[level+1])
//         ++level;

//     //print (String.Format("{0}x{1} need {2} has {3} = {4} level", resx, resy, fillneed, fillrate, level));

//     overallQuality = level;
//     UpdateAllSettings ();
// }
