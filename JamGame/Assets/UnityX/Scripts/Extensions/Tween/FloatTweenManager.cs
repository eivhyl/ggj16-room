﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
// public class FloatTweenManager : TweenManager<FloatTween, float> {
public class FloatTweenManager {
	public bool tweening = false;
	public float currentValue;
	public float deltaValue;
	public List<FloatTween> tweens;

	public FloatTweenManager () {
		tweens = new List<FloatTween>();
	}

	public void ClearAllTweens(){
		tweens.Clear();
		ChangeNumTweens();
	}


	public void AddNewTween(FloatTween _newTween){
		tweens.Add(_newTween);
		ChangeNumTweens();
	}
	
	// public void AddNewTween(float _startValue, float _targetValue, float _time){
	// 	AddNewTween(_startValue, _targetValue, _time, AnimationCurve.EaseInOut(0,0,1,1));
	// }

	// public void AddNewTween(float _startValue, float _targetValue, float _time){
	// 	AddNewTween(_startValue, _targetValue, _time, AnimationCurve.EaseInOut(0,0,1,1));
	// }

	// public void AddNewTween(float _startValue, float _targetValue, float _time, AnimationCurve _lerpCurve){
	// 	AddNewTween(FloatTween newTween = new FloatTween(_startValue, _targetValue, _time, _lerpCurve);
	// }
	
	public float Loop () {
		return Loop(Time.deltaTime);
	}

	public float Loop(float _deltaTime){
		deltaValue = currentValue = 0;
		if(tweens == null || tweens.Count == 0){
			return currentValue;
		}

		for(int i = tweens.Count-1; i >= 0; i--){
			tweens[i].Loop(_deltaTime);
			if(!tweens[i].tweening) {
				tweens.RemoveAt(i);
				continue;
			}
			currentValue += tweens[i].currentValue;
			deltaValue += tweens[i].deltaValue;
		}

		return currentValue;
	}

	public float LongestTween() {
		float longest = 0;
		for(int i = tweens.Count-1; i >= 0; i--){
			if(tweens[i].currentValue > longest) {
				longest = tweens[i].currentValue;
			}
		}
		return longest;
	}

	private void ChangeNumTweens(){
		if(tweens.Count == 0){
			tweening = false;
		} else {
			tweening = true;
		}
	}
}
