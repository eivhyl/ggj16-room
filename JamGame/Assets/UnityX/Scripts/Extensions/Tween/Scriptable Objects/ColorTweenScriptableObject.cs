using UnityEngine;
using System.Collections;

public class ColorTweenScriptableObject : ScriptableObject {
	public ColorTweenProperties tweenProperties;
	
	[System.Serializable]
	public class ColorTweenProperties : TweenProperties<Color> {
		public ColorTweenProperties (Color startValue, float tweenTime) : base (startValue, tweenTime) {}
		public ColorTweenProperties (Color startValue, Color targetValue, float tweenTime) : base (startValue, targetValue, tweenTime) {}
		public ColorTweenProperties (Color startValue, Color targetValue, float tweenTime, AnimationCurve easingCurve) : base (startValue, targetValue, tweenTime, easingCurve) {}
	}
}
