﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
/// <summary>
/// Timer Manager class. Handles timer looping for other classes.
/// </summary>
public class TimerManager : PersistentMonoSingleton<TimerManager> {
	public List<Timer> timers;

	public TimerManager () {
		timers = new List<Timer>();
	}
	
	void Update () {
		for(int i = timers.Count-1; i >= 0; i--) {
			timers[i].Loop();
			//This could probably be rethought. I probably want to remove from the timers when finished, rather than when stopped.
			if(timers[i].timerState == TimerState.Stopped) {
				timers.RemoveAt(i);
			}
		}
	}

	public void AddTimer (Timer newTimer) {
		newTimer.OnComplete += TimerComplete;
		timers.Add(newTimer);
	}

	public void TimerComplete () {

	}
}
