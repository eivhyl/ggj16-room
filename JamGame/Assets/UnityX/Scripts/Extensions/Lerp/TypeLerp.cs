﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Type lerp. Moves a value of type T towards a target value.
/// </summary>
[System.Serializable]
public abstract class TypeLerp<T> {
	public T current;
	public T target;
	public float lerpSpeed = 0.2f;

	public TypeLerp () {
	
	}

	public TypeLerp (T origin) {
		current = target = origin;
	}

	public abstract void Update(float deltaTime);
}