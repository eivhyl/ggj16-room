using UnityEngine;
using System.Collections;
using UnityX.Geometry;

[System.Serializable]
public class Vector2Map : TypeMap<Vector2> {
	public Vector2 averageVector;
	public float minMagnitude;
	public float maxMagnitude;
	public float deltaMagnitude;
	
	public Vector2Map (Point _size) : base (_size) {}
	public Vector2Map (Point _size, Vector2 _value) : base (_size, _value) {}
	public Vector2Map (Point _size, Vector2[] _mapArray) : base (_size, _mapArray) {}
	public Vector2Map (TypeMap<Vector2> typeMap) : base (typeMap.size, typeMap.values) {}
	public Vector2Map (Vector2Map _map) : base (_map) {
		averageVector = _map.averageVector;
		minMagnitude = _map.minMagnitude;
		maxMagnitude = _map.maxMagnitude;
		deltaMagnitude = _map.deltaMagnitude;
	}
	
	public override void CalculateMapProperties(){
		averageVector = values.Average();
		minMagnitude = Vector2X.SmallestMagnitude(values);
		maxMagnitude = Vector2X.LargestMagnitude(values);
		deltaMagnitude = maxMagnitude - minMagnitude;
	}

	protected override Vector2 Lerp (Vector2 a, Vector2 b, float l) {
		return Vector2.Lerp(a,b,l);
	}
	
	public override Texture2D CreateTexture(bool apply = false){
		Color[] colors = values.ToColorArray();
		Texture2D texture = TextureX.Create(new Point(size.x, size.y), colors);
   		texture.Apply();
		if(apply)texture.Apply();
		return texture;
	}
}
