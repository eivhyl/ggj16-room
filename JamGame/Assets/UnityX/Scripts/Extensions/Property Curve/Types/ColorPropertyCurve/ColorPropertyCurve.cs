﻿using UnityEngine;
using System.Collections;
using UnityX.Geometry;
using UnityX.Geometry.Polygon;

public class ColorPropertyCurve : PropertyCurve<Color> {
	
	public ColorPropertyCurve(params PropertyCurveKeyframe<Color>[] keys) : base (keys) {}
	
	protected override Color GetSmoothedValue(Color key1, Color key2, float time) {
		return Color.Lerp(key1, key2, time);
	}
}
