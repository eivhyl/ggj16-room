using UnityEngine;
using System.Collections;

/// <summary>
/// Distance spawn checker.
/// </summary>
[System.Serializable]
public class TargetIntervalChecker  {
	[HideInInspector]
	public float nextSpawnDistance;
	public float spawnInterval;
	/*/// <summary>
	/// The spawn interval. This is automatically added to the nextSpawnDistance each spawn. Can be set to 0 if the spawn interval is set manually each spawn. 
	/// </summary>
	[SerializeField, NotLessThanAttribute(0f)]
	private float _spawnInterval = 1;
	public float spawnInterval {
		get {
			return _spawnInterval;
		}
		set {
			if(value < 0) DebugX.LogError(this, "Spawn interval cannot be less than or equal to 0.");
			_spawnInterval = value;
		}
	}
	*/
	public delegate void OnSpawnEvent(float distance);
	public event OnSpawnEvent OnSpawn;
	
	public TargetIntervalChecker (float startSpawnDistance) {
		nextSpawnDistance = startSpawnDistance;
	}
	
	public TargetIntervalChecker (float startSpawnDistance, float spawnInterval) {
		nextSpawnDistance = startSpawnDistance;
		this.spawnInterval = spawnInterval;
	}
	
	public void UpdateCheckDistance (float distance) {
		while(distance >= nextSpawnDistance) {
			float lastSpawnDistance = nextSpawnDistance;
			Spawn (nextSpawnDistance);
			nextSpawnDistance += spawnInterval;
			if(nextSpawnDistance <= lastSpawnDistance) {
				Debug.LogError ("nextSpawnDistance is unchanged. This will cause an infinite loop to occur. 1 has been added to prevent a crash.");
				nextSpawnDistance = lastSpawnDistance + 1;
			}
		}
	}
	
	private void Spawn (float distance) {
		if(OnSpawn != null) {
			OnSpawn(distance);
		}
	}
}