﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class PlaneGenerator {

	public static Mesh CreatePlane(){
		return PlaneGenerator.CreatePlane(new Vector3(-0.5f, 0.5f, 0),new Vector3(0.5f, 0.5f, 0), new Vector3(0.5f, -0.5f, 0), new Vector3(-0.5f, -0.5f, 0));
	}
	
	public static Mesh CreatePlane(Rect r){
		return PlaneGenerator.CreatePlane(r.TopLeft(), r.TopRight(), r.BottomRight(), r.BottomLeft());
	}

	public static Mesh CreatePlane(Vector3 topLeft, Vector3 topRight, Vector3 bottomRight, Vector3 bottomLeft){
		Mesh mesh;
		Vector3[] verts = null;
		int[] tris = null;
		Vector2[] uvs = null;
		PlaneGenerator.GetPlaneProperties(topLeft, topRight, bottomRight, bottomLeft, ref verts, ref tris, ref uvs);
		mesh = new Mesh();
		mesh.vertices = verts;
		mesh.SetTriangles(tris,0);
		mesh.uv = uvs;
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		mesh.Optimize();
		
		return mesh;
	}
	
	public static void GetPlaneProperties(Vector3 topLeft, Vector3 topRight, Vector3 bottomRight, Vector3 bottomLeft, ref Vector3[] verts, ref int[] tris, ref Vector2[] uvs){
		verts = new Vector3[4];
		tris = new int[6];
		uvs = new Vector2[4];
		
		verts[0] = topLeft;
		verts[1] = topRight;
		verts[2] = bottomRight;
		verts[3] = bottomLeft;
		
		tris[0] = 3;
		tris[1] = 1;
		tris[2] = 0;
		tris[3] = 3;
		tris[4] = 2;
		tris[5] = 1;
		
		uvs[0] = new Vector2(0,1);
		uvs[1] = new Vector2(1,1);
		uvs[2] = new Vector2(1,0);
		uvs[3] = new Vector2(0,0);
	}
}
