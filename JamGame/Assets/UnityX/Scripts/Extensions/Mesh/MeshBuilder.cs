﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
/// <summary>Helper class when creating meshes. Tucks away mesh generating properties to make any classes that need to generate meshes cleaner.</summary>
public class MeshBuilder {

	[HideInInspector]
	public Mesh mesh;

	public List<Vector3> verts = new List<Vector3>();
	public List<int> tris = new List<int>();
	public List<Vector2> uvs = new List<Vector2>();
	private float u, v;

	// public Vector3[] verts;
	// public int[] tris;
	// public Vector2[] uvs;

	public MeshBuilder () {

	}

	protected virtual void CreateMesh () {
		// This would simply be duplicating the existing create mesh function - bad practice.
		// mesh = MeshX.CreateMesh(mesh, verts, tris, uvs, true, true, true);
	}

	protected virtual void ClearMesh () {
		mesh.Clear();

		verts = new List<Vector3>();
		tris = new List<int>();
		uvs = new List<Vector2>();
	}
}
