﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityX.Geometry.Polygon;

[ExecuteInEditMode]
public class PolygonTest : MonoBehaviour {
	public Polygon myPolygon;
	public Polygon[] polygons;
	public PolygonTestSub sub;
	
	[ContextMenu("Set")]
	public void Set () {
		myPolygon = new RegularPolygon(6,15);
//		polygons[0] = new RegularPolygon(9,124);
//		sub.myPolygon = new RegularPolygon(12,15);
		
	}
	[ContextMenu("Log")]
	public void Log () {
		Debug.Log (myPolygon);
//		Debug.Log (polygons[0]);
//		Debug.Log (sub.myPolygon);
	}
	
	[System.Serializable]
	public class PolygonTestSub {
		public Polygon myPolygon;
		public Polygon[] myPolygons;
	}
}