﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public static class CollectionX {
	
	public static bool Equal<T>(IList<T> a1, IList<T> a2) {
		if (ReferenceEquals(a1,a2))
			return true;
		
		if (a1 == null || a2 == null)
			return false;
		
		if (a1.Count != a2.Count)
			return false;
		
		System.Collections.Generic.EqualityComparer<T> comparer = System.Collections.Generic.EqualityComparer<T>.Default;
		for (int i = 0; i < a1.Count; i++)
		{
			if (!comparer.Equals(a1[i], a2[i])) return false;
		}
		return true;
	}
	
	public static T[] Fill<T>(this T[] array, T myVar) {
        for(int i = 0; i < array.Length; i++){
			array[i] = myVar;
		}
		return array;
    }

 //    public static bool ValidIndex<T>(this IList<T> array, int index) {
	// 	return MathX.InRange(index, 0, array.Count-1);
	// }

	// public static bool IsNullOrEmpty(this Array array) {
	// 	return (array == null || array.Length == 0);
	// }

	// public static bool IsNullOrEmpty<T>(this T[] array) {
	// 	return (array == null || array.Length == 0);
	// }

	//Returns average, min and max as a Vector. Saves crunching the same data three times.
	public static float AverageMinMax(this IList<float> floatArray){
		float total = 0;
		for(int i = 0; i < floatArray.Count; i++){
			total += floatArray[i]; 
		}
		float average = total/floatArray.Count;
		return average;
	}


	public static Color[] FloatArrayToGrayscaleColorArray(IList<float> floatArray){
		Color[] colorArray = new Color[floatArray.Count];
		for(int i = 0; i < floatArray.Count; i++){
			colorArray[i] = floatArray[i].ToColor();
		}
		return colorArray;
	}

	public static Color[] FloatArrayToColorArray(IList<float> floatArray, float hueStart = 0, float hueRange = 360){
		Color[] colorArray = new Color[floatArray.Count];
		for(int i = 0; i < floatArray.Count; i++){
			colorArray[i] = new HSLColor(hueStart + floatArray[i] * hueRange, 1, 0.5f);
		}
		return colorArray;
	}

	public static float[] ColorArrayToFloatArray(IList<Color> colorArray){
		float[] floatArray = new float[colorArray.Count];
		for(int i = 0; i < colorArray.Count; i++){
			floatArray[i] = colorArray[i].grayscale;
		}
		return floatArray;
	}

	/// <summary>
    /// Converts an array of bools to an array of ints.
    /// </summary>
	public static int[] BoolArrayToIntArray(IList<bool> boolArray){
		int[] intArray = new int[boolArray.Count];
		for(int i = 0; i < boolArray.Count; i++){
			intArray[i] = Convert.ToInt32(boolArray[i]);
		}
		return intArray;
	}

	/// <summary>
    /// Converts valid members of an array of bools to an array of ints storing valid indexes.
    /// </summary>
	public static int[] ValidBoolArrayToIntIndexArray(IList<bool> boolArray, bool state){
		List<int> validIntIndexArray = new List<int>();
		for(int i = 0; i < boolArray.Count; i++){
			if(boolArray[i] == state) validIntIndexArray.Add(i);
		}
		return validIntIndexArray.ToArray();
	}

	public static float[] BoolArrayToFloatArray(IList<bool> boolArray){
		float[] floatArray = new float[boolArray.Count];
		for(int i = 0; i < boolArray.Count; i++){
			floatArray[i] = Convert.ToSingle(boolArray[i]);
		}
		return floatArray;
	}

	public static Color[] BoolArrayToColorArray(IList<bool> boolArray){
		Color[] colorArray = new Color[boolArray.Count];
		for(int i = 0; i < boolArray.Count; i++){
			colorArray[i] = Convert.ToSingle(boolArray[i]).ToColor();
		}
		return colorArray;
	}

	public static Color[] BoolArrayToColorArray(IList<bool> boolArray, Color c1, Color c2){
		Color[] colorArray = new Color[boolArray.Count];
		for(int i = 0; i < boolArray.Count; i++){
			colorArray[i] = boolArray[i]?c1:c2;
		}
		return colorArray;
	}

		
}
