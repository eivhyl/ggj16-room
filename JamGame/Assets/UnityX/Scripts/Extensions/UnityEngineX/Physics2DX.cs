﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class Physics2DX {

	public static List<Vector2> PlotTrajectory (Vector2 start, Vector2 startVelocity, float timestep, float maxTime) {
		return PlotTrajectory(start, startVelocity, timestep, maxTime, Physics2D.gravity);
	}
	
	public static List<Vector2> PlotTrajectory (Vector2 start, Vector2 startVelocity, float timestep, float maxTime, Vector2 gravity) {
		if(DebugX.Assert(timestep > 0, "Timestep must be more than 0")) {
			return null;
		}
		
		List<Vector2> segments = new List<Vector2>();
		segments.Add(start);
		for (int i = 1;; i++) {
			float t = timestep * i;
			if (t > maxTime) break;
			segments.Add(PlotTrajectoryAtTime (start, startVelocity, t, gravity));
//			if (Physics.Linecast (segments[i - 1], segments[i])) {
//				
//			}
		}
		return segments;
	}
	
	public static List<Vector2> PlotTrajectoryDistance (Vector2 start, Vector2 startVelocity, float length, float segmentScale) {
		return PlotTrajectoryDistance(start, startVelocity, length, segmentScale, Physics2D.gravity);
	}
	
	public static List<Vector2> PlotTrajectoryDistance (Vector2 start, Vector2 startVelocity, float length, float segmentScale, Vector2 gravity) {
		int segmentCount = Mathf.RoundToInt(length/segmentScale);
		Vector2[] segments = new Vector2[segmentCount];
		
		segments[0] = PlotTrajectoryAtTime(start, startVelocity, 0, gravity);
		for (int i = 1; i < segmentCount; i++) {
			// Time it takes to traverse one segment of length segScale (careful if velocity is zero)
			float time = i * ((startVelocity.sqrMagnitude != 0) ? (segmentScale) / startVelocity.magnitude : 0);
			segments[i] = PlotTrajectoryAtTime(start, startVelocity, time, gravity);
		}
		return segments.ToList();
	}
	
	public static Vector2 PlotTrajectoryAtTime (Vector2 start, Vector2 startVelocity, float time) {
		return PlotTrajectoryAtTime(start, startVelocity, time, Physics2D.gravity);
	}
	
	public static Vector2 PlotTrajectoryAtTime (Vector2 start, Vector2 startVelocity, float time, Vector2 gravity) {
		return start + startVelocity*time + gravity*time*time*0.5f;
	}
	
	/*void SimulatePath () {
		
		// The initial velocity
		Vector3 segVelocity = rigidbody.velocity * Time.deltaTime;
		Debug.Log (segVelocity);
		// reset our hit object
		_hitObject = null;
		
		for (int i = 1; i < segmentCount; i++)
		{
			// Time it takes to traverse one segment of length segScale (careful if velocity is zero)
			float segTime = (segVelocity.sqrMagnitude != 0) ? segmentScale / segVelocity.magnitude : 0;
			
			// Add velocity from gravity for this segment's timestep
			segVelocity = segVelocity + Physics2D.gravity * segTime;
			
			segments[i] = segments[i - 1] + segVelocity * segTime;
			continue;
			
			// Check to see if we're going to hit a physics object
			RaycastHit hit;
			if (Physics2D.Raycast(segments[i - 1], segVelocity, out hit, segmentScale))
			{
				// remember who we hit
				_hitObject = hit.collider;
				
				// set next position to the position where we hit the physics object
				segments[i] = segments[i - 1] + segVelocity.normalized * hit.distance;
				// correct ending velocity, since we didn't actually travel an entire segment
				segVelocity = segVelocity - Physics2D.gravity * (segmentScale - hit.distance) / segVelocity.magnitude;
				// flip the velocity to simulate a bounce
				segVelocity = Vector3.Reflect(segVelocity, hit.normal);
				
				
//				 * Here you could check if the object hit by the Raycast had some property - was 
//				 * sticky, would cause the ball to explode, or was another ball in the air for 
//				 * instance. You could then end the simulation by setting all further points to 
//				 * this last point and then breaking this for loop.
//				
			}
			// If our raycast hit no objects, then set the next position to the last one plus v*t
			else
			{
				segments[i] = segments[i - 1] + segVelocity * segTime;
			}
		}
	}*/
}
