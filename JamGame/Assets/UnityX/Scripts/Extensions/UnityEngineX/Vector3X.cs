﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class Vector3X {

	
	/// <summary>
	/// The number of components in the vector.
	/// </summary>
	/// <param name="v">V.</param>
	public static int NumComponents(this Vector3 v) {
		return 3;
	}
	
	/// <summary>
	/// Returns a half.
	/// </summary>
	/// <value>The half.</value>
	public static Vector3 half {
		get {
			return new Vector3(0.5f, 0.5f, 0.5f);
		}
	}
	
	public static string ToString(this Vector3 v, int numDecimalPlaces = 3){
		return ("("+v.x.RoundTo(numDecimalPlaces).ToString()+", "+v.y.RoundTo(numDecimalPlaces).ToString()+", "+v.z.RoundTo(numDecimalPlaces).ToString()+")");
	}
	
	/// <summary>
	/// Returns direction from a to b.
	/// </summary>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	public static Vector3 Direction(Vector3 a, Vector3 b){
		return b - a;
	}
	
	/// <summary>
	/// Returns normalized direction from a to b.
	/// </summary>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	public static Vector3 NormalizedDirection(Vector3 a, Vector3 b){
		return Direction(a, b).normalized;
	}
	
	/// <summary>
	/// Divide the specified left and right Vectors.
	/// </summary>
	/// <param name="left">Left.</param>
	/// <param name="right">Right.</param>
	public static Vector3 Divide(float left, Vector3 right){
		return new Vector3(left / right.x, left / right.y, left / right.z);
	}
	
	/// <summary>
	/// Divide the specified left and right Vectors.
	/// </summary>
	/// <param name="left">Left.</param>
	/// <param name="right">Right.</param>
	public static Vector3 Divide(Vector3 left, float right){
		return new Vector3(left.x / right, left.y / right, left.z / right);
	}
	
	/// <summary>
	/// Divide the specified left and right Vectors.
	/// </summary>
	/// <param name="left">Left.</param>
	/// <param name="right">Right.</param>
	public static Vector3 Divide(Vector3 left, Vector3 right){
		return new Vector3(left.x / right.x, left.y / right.y, left.z / right.z);
	}
	
	/// <summary>
	/// Adds a float to a Vector3.
	/// </summary>
	/// <param name="left">Left.</param>
	/// <param name="right">Right.</param>
	public static Vector3 Add(this Vector3 left, float right){
		return new Vector3(left.x + right, left.y + right, left.z + right);
	}
	
	/// <summary>
	/// Subtracts a float from a Vector3.
	/// </summary>
	/// <param name="left">Left.</param>
	/// <param name="right">Right.</param>
	public static Vector3 Subtract(this Vector3 left, float right){
		return new Vector3(left.x - right, left.y - right, left.z - right);
	}
	
	
	/// <summary>
	/// Clamps the components of the Vector between 0 and 1.
	/// </summary>
	/// <param name="v">V.</param>
	public static Vector3 Clamp01(Vector3 v){
		return Vector3X.Clamp(v, Vector3.zero, Vector3.one);
	}
	
	/// <summary>
	/// Clamps the components of the Vector between the matching components of min and max.
	/// </summary>
	/// <param name="v">V.</param>
	/// <param name="min">Minimum.</param>
	/// <param name="max">Max.</param>
	public static Vector3 Clamp(Vector3 v, Vector3 min, Vector3 max){
		v.x = Mathf.Clamp(v.x, min.x, max.x);
		v.y = Mathf.Clamp(v.y, min.y, max.y);
		v.z = Mathf.Clamp(v.z, min.z, max.z);
		return v;
	}
	
	/// <summary>
	/// Clamp this instance.
	/// </summary>
	public static Vector3 Clamp (Vector3 vector, Bounds bounds) {
		return Vector3X.Clamp (vector, bounds.min, bounds.max);
	}
	
	/// <summary>
	/// Gets a Vector3 with all components set to random numbers between 0 and 1. To get a normalized random direction, use RandomX.OnUnitSphere.
	/// </summary>
	/// <value>The random.</value>
	public static Vector3 random {
		get {
			return new Vector3(Random.value - 0.5f, Random.value - 0.5f, Random.value - 0.5f);
		}
	}

	public static float SqrDistance (Vector3 a, Vector3 b) {
		return (a-b).sqrMagnitude;
	}
	
	/// <summary>
	/// Returns the distance between two vectors in a specific direction, using projection.
	/// For example, if the direction is forward and a is (0,0,0) and b is (0,2,1), the function will return 1, as the upwards component of b is ignored by the forward direction.
	/// </summary>
	/// <returns>The in direction.</returns>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	/// <param name="direction">Direction.</param>
	public static float DistanceInDirection (Vector3 a, Vector3 b, Vector3 direction) {
		Vector3 normalizedDirection = direction.sqrMagnitude == 1 ? direction : direction.normalized;
		Vector3 projectedA = Vector3.Project(a, normalizedDirection);
		Vector3 projectedB = Vector3.Project(b, normalizedDirection);
		return Vector3.Distance(projectedA, projectedB);
	}
	
	/// <summary>
	/// Returns the smallest component of the Vector3.
	/// </summary>
	/// <param name="v">V.</param>
	public static float Smallest(this Vector3 v){
		return (new float[3]{v.x, v.y, v.z}).Smallest();
	}
	
	/// <summary>
	/// Returns the largest component of the Vector3.
	/// </summary>
	/// <param name="v">V.</param>
	public static float Largest(this Vector3 v){
		return (new float[3]{v.x, v.y, v.z}).Largest();
	}

	/// <summary>
	/// Returns the index of the smallest component of the Vector3.
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="v">V.</param>
	public static int SmallestIndex(this Vector3 v){
		return (new float[3]{v.x, v.y, v.z}).SmallestIndex();
	}
	
	/// <summary>
	/// Returns the index of the largest component of the Vector3.
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="v">V.</param>
	public static int LargestIndex(this Vector3 v){
		return (new float[3]{v.x, v.y, v.z}).LargestIndex();
	}
	
	/// <summary>
	/// Returns the index of the absolute smallest component of the Vector3.
	/// </summary>
	/// <returns>The smallest index.</returns>
	/// <param name="v">V.</param>
	public static int AbsSmallestIndex(this Vector3 v){
		return (new float[3]{v.x, v.y, v.z}).AbsSmallestIndex();
	}
	
	/// <summary>
	/// Returns the index of the absolute largest component of the Vector3.
	/// </summary>
	/// <returns>The largest index.</returns>
	/// <param name="v">V.</param>
	public static int AbsLargestIndex(this Vector3 v){
		return (new float[3]{v.x, v.y, v.z}).AbsLargestIndex();
	}
	
	/// <summary>
	/// Average the components of the Vector.
	/// </summary>
	/// <param name="v">V.</param>
	public static float Average(this Vector3 v){
		return (new float[3]{v.x, v.y, v.z}).Average();
	}
	
	/// <summary>
	/// Returns a Vector3 with all components made absolute.
	/// </summary>
	/// <param name="v">V.</param>
	public static Vector3 Abs( this Vector3 v ){
		return new Vector3(Mathf.Abs(v.x), Mathf.Abs(v.y), Mathf.Abs(v.z));
	}

	public static Vector3 NormalizedDirection( this Vector3 v ){
		Vector3 newVector = new Vector3();
		int index = v.AbsLargestIndex();
		newVector[index] = v[index].Sign(false);
		return newVector;
	}

	
	/// <summary>
	/// Takes the reciprocal of each component in the vector.
	/// </summary>
	/// <param name="value">The vector to take the reciprocal of.</param>
	/// <returns>A vector that is the reciprocal of the input vector.</returns>
	public static Vector3 Reciprocal(this Vector3 v) {
//		return new Vector3(1f/v.x, 1f/v.y, 1f/v.z);
		return Vector3X.Divide(1f, v);
	}
	
	/// <summary>
	/// Inverts the components of the Vector3.
	/// </summary>
	/// <param name="tmpRotation">Tmp rotation.</param>
	public static Vector3 Invert (Vector3 tmpRotation){
		return tmpRotation * -1;
	}
	
	/// <summary>
	/// Randomly inverts the components of the Vector3.
	/// </summary>
	/// <returns>The invert.</returns>
	/// <param name="tmpRotation">Tmp rotation.</param>
	public static Vector2 RandomInvert (Vector3 tmpRotation){
		for(int i = 0; i < 3; i++) {
			if(RandomX.boolean) tmpRotation[i] *= -1;
		}
		return tmpRotation;
	}
	
	/// <summary>
	/// Returns a Vector3 with all components rounded.
	/// </summary>
	/// <returns>The to.</returns>
	/// <param name="v">V.</param>
	/// <param name="decimalPlaces">Decimal places.</param>
	public static Vector3 RoundTo (this Vector3 v, int decimalPlaces = 2){
		return new Vector3(v.x.RoundTo(decimalPlaces), v.y.RoundTo(decimalPlaces), v.z.RoundTo(decimalPlaces));
	}

	/// <summary>
	/// Returns a Vector3 with all components rounded upwards.
	/// </summary> 
	public static Vector3 FloorTo (this Vector3 v, int decimalPlaces = 2){
		return new Vector3(v.x.FloorTo(decimalPlaces), v.y.FloorTo(decimalPlaces), v.z.FloorTo(decimalPlaces));
	}

	/// <summary>
	/// Returns a Vector3 with all components rounded upwards.
	/// </summary> 
	public static Vector3 CeilTo (this Vector3 v, int decimalPlaces = 2){
		return new Vector3(v.x.CeilTo(decimalPlaces), v.y.CeilTo(decimalPlaces), v.z.CeilTo(decimalPlaces));
	}
	
	/// <summary>
	/// Rotates the specified Vector3 as if it were a eulerAngle. 
	/// Shortcut to Quaternion.Rotate.
	/// </summary>
	/// <param name="rotation">Rotation.</param>
	/// <param name="eulerAngles">Euler angles.</param>
	/// <param name="space">Space.</param>
	public static Vector3 Rotate (this Vector3 rotation, Vector3 eulerAngles, Space space = Space.Self) {
		return (Quaternion.Euler(rotation).Rotate(eulerAngles, space)).eulerAngles;
	}
	
	public static Vector3 RotateX(this Vector3 v,  float angle) {
		float sin = Mathf.Sin( angle );
		float cos = Mathf.Cos( angle );
		float ty = v.y;
		float tz = v.z;
		v.y = (cos * ty) - (sin * tz);
		v.z = (cos * tz) + (sin * ty);

		return v;
	}

	public static Vector3 RotateY(this Vector3 v, float angle) {
		float sin = Mathf.Sin( angle );
		float cos = Mathf.Cos( angle );
		float tx = v.x;
		float tz = v.z;
		v.x = (cos * tx) + (sin * tz);
		v.z = (cos * tz) - (sin * tx);

		return v;
	}

	public static Vector3 RotateZ (this Vector3 v, float angle) {
		float sin = Mathf.Sin( angle );
		float cos = Mathf.Cos( angle );
		float tx = v.x;
		float ty = v.y;
		v.x = (cos * tx) - (sin * ty);
		v.y = (cos * ty) + (sin * tx);

		return v;
	}

	public static float GetPitch (this Vector3 v){
		float len = Mathf.Sqrt((v.x * v.x) + (v.z * v.z));    // Length on xz plane.
		return(-Mathf.Atan2(v.y, len));
	}

	public static float GetYaw (this Vector3 v)  {
		return( Mathf.Atan2( v.x, v.z ) );
	}
	
	
	/// <summary>
	/// Returns the index of the closest vector to v in the values list
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static int ClosestIndex(Vector3 v, IList<Vector3> values){
		int index = 0;
		float closest = Vector3X.SqrDistance(v, values[index]);
		for(int i = 1; i < values.Count; i++){
			float distance = Vector3X.SqrDistance(v, values[i]);
			if (distance < closest) {
				closest = distance;
				index = i;
			}
		}
		return index;
	}
	
	
	/// <summary>
	/// Returns the distance between the closest vector to v in the values list and v
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static float ClosestDistance(Vector3 v, IList<Vector3> values){
		return Vector3.Distance(v, Closest(v, values));
	}
	
	/// <summary>
	/// Returns the closest vector to v in the values list
	/// </summary>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static Vector3 Closest(Vector3 v, IList<Vector3> values){
		return values[Vector3X.ClosestIndex(v, values)];
	}


	/// <summary>
	/// Returns the index of the furthest vector to v in the values list
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static int FurthestIndex(IList<Vector3> valuesA, IList<Vector3> valuesB){
		int index = 0;
		float furthest = Vector3X.FurthestDistance(valuesA[index], valuesB);
		for(int i = 0; i < valuesA.Count; i++) {
			float distance = Vector3X.FurthestDistance(valuesA[i], valuesB);
			Debug.Log (distance +" "+furthest);
			if(distance > furthest) {
				furthest = distance;
				index = i;
			}
		}
		return index;
	}
	
	
	/// <summary>
	/// Returns the index of the furthest vector to v in the values list
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static int FurthestIndex(Vector3 v, IList<Vector3> values){
		if(values.Count == 0) {
			Debug.LogError("Values is empty!");
			return -1;
		}
		int index = 0;
		float furthest = Vector3X.SqrDistance(v, values[index]);
		for(int i = 1; i < values.Count; i++){
			float distance = Vector3X.SqrDistance(v, values[i]);
			if (distance > furthest) {
				furthest = distance;
				index = i;
			}
		}
		return index;
	}
	
	
	/// <summary>
	/// Returns the distance between the furthest vector to v in the values list and v
	/// </summary>
	/// <returns>The index.</returns>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static float FurthestDistance(Vector3 v, IList<Vector3> values){
		return Vector3.Distance(v, Furthest(v, values));
	}
	
	/// <summary>
	/// Returns the furthest vector to v in the values list
	/// </summary>
	/// <param name="v">V.</param>
	/// <param name="values">Values.</param>
	public static Vector3 Furthest(Vector3 v, IList<Vector3> values){
		return values[Vector3X.FurthestIndex(v, values)];
	}
	
	
	
	
	
	public static int MinIndex(IList<Vector3> values){
		if(values.Count == 0) {
			Debug.LogError("Values is empty!");
			return -1;
		}
		int index = 0;
		Vector3 min = values[0];
		for(int i = 1; i < values.Count; i++){
			Vector3.Min (min, values[i]);
		}
		return index;
	}
	
	public static Vector3 Min(IList<Vector3> values){
		return values[Vector3X.MinIndex(values)];
	}
	
	
	
	
	
	public static int MaxIndex(IList<Vector3> values){
		if(values.Count == 0) {
			Debug.LogError("Values is empty!");
			return -1;
		}
		int index = 0;
		Vector3 max = values[0];
		for(int i = 1; i < values.Count; i++){
			Vector3.Max (max, values[i]);
		}
		return index;
	}
	
	public static Vector3 Max(IList<Vector3> values){
		return values[Vector3X.MaxIndex(values)];
	}
	
	
	public static Vector3 Average(this IList<Vector3> values){
		Vector3 total = values[0];
		for(int i = 1; i < values.Count; i++){
			total += values[i];
		}
		return total/values.Count;
	}
	
	
	
	
	public static Vector3 ClampMagnitudeInDirection (Vector3 velocity, Vector3 direction, float clampValue, bool outwardsOnly = false) {
		float speedAlongTangent = Vector3.Dot(velocity, direction);
		if(Mathf.Abs(speedAlongTangent) > clampValue) {
			float clampedSpeed = Mathf.Clamp(speedAlongTangent, -clampValue, +clampValue);
			float speedDiff = clampedSpeed - speedAlongTangent;
			velocity += speedDiff * direction;
		}
		return velocity;
	}
	
	
	
	// To Vector2
	/// <summary>
	/// Creates a Vector2 from a Vector3, using the X and Y components (in that order).
	/// </summary> 
	public static Vector2 XY (this Vector3 v) {
		return new UnityEngine.Vector2(v.x,v.y);
	}

	/// <summary>
	/// Creates a Vector2 from a Vector3, using the X and Z components (in that order).
	/// </summary> 
	public static Vector2 XZ (this Vector3 v) {
		return new UnityEngine.Vector2(v.x, v.z);
	}

	/// <summary>
	/// Creates a Vector2 from a Vector3, using the Y and Z components (in that order).
	/// </summary> 
	public static Vector2 YZ (this Vector3 v) {
		return new UnityEngine.Vector2(v.y, v.z);
	}

	/// <summary>
	/// Creates a Vector2 from a Vector3, using the Y and X components (in that order).
	/// </summary> 
	public static Vector2 YX (this Vector3 v) {
		return new UnityEngine.Vector2(v.y, v.x);
	}

	/// <summary>
	/// Creates a Vector2 from a Vector3, using the Z and X components (in that order).
	/// </summary> 
	public static Vector2 ZX (this Vector3 v) {
		return new UnityEngine.Vector2(v.z, v.x);
	}

	/// <summary>
	/// Creates a Vector2 from a Vector3, using the Z and Y components (in that order)..
	/// </summary> 
	public static Vector2 ZY (this Vector3 v) {
		return new UnityEngine.Vector2(v.z, v.y);
	}


	//SWAP FUNCTIONS
	/// <summary>
	/// Changes the order of components of a vector. Reverses the Y and Z components.
	/// </summary> 
	public static Vector3 XZY (this Vector3 v) {
		return new UnityEngine.Vector3(v.x, v.z, v.y);
	}

	/// <summary>
	/// Changes the order of components of a vector. Swaps the X and Y components.
	/// </summary> 
	public static Vector3 YXZ (this Vector3 v) {
		return new UnityEngine.Vector3(v.y, v.x, v.z);
	}

	/// <summary>
	/// Changes the order of components of a vector. Replaces X with Y, Y with Z and Z with X.
	/// </summary> 
	public static Vector3 YZX (this Vector3 v) {
		return new UnityEngine.Vector3(v.y, v.z, v.x);
	}

	/// <summary>
	/// Changes the order of components of a vector. Replaces X with Z, Y with X and Z with Y.
	/// </summary> 
	public static Vector3 ZXY (this Vector3 v) {
		return new UnityEngine.Vector3(v.z, v.x, v.y);
	}

	/// <summary>
	/// Changes the order of components of a vector. Reverses all the components.
	/// </summary> 
	public static Vector3 ZYX (this Vector3 v) {
		return new UnityEngine.Vector3(v.z, v.y, v.x);
	}
	
	

	/// <summary>
	/// Sets the value of the X component.
	/// </summary> 
	public static Vector3 SetX (this Vector3 v, float newX) {
		return new Vector3(newX, v.y, v.z);
	}
 	
 	/// <summary>
	/// Sets the value of the Y component
	/// </summary> 
	public static Vector3 SetY (this Vector3 v, float newY) {
		return new Vector3(v.x, newY, v.z);
	}
 	
 	/// <summary>
	/// Sets the value of the Z component
	/// </summary> 
	public static Vector3 SetZ (this Vector3 v, float newZ) {
		return new Vector3(v.x, v.y, newZ);
	}	
	
	

	/// <summary>
	/// Adds a value to the X component
	/// </summary> 
	public static Vector3 AddX (this Vector3 v, float addX) {
		return new Vector3(v.x + addX, v.y, v.z);
	}
 	
 	/// <summary>
	/// Adds a value to the Y component
	/// </summary> 
	public static Vector3 AddY (this Vector3 v, float addY) {
		return new Vector3(v.x, v.y + addY, v.z);
	}
	
	/// <summary>
	/// Adds a value to the Z component
	/// </summary> 
	public static Vector3 AddZ (this Vector3 v, float addZ) {
		return new Vector3(v.x, v.y, v.z + addZ);
	}
	
	

	/// <summary>
	/// Returns a new Vector only containing the X component of the input
	/// </summary>
	public static Vector3 X (this Vector3 v) {
		return new UnityEngine.Vector3(v.x, 0, 0);
	}

	/// <summary>
	/// Returns a new Vector only containing the Y component of the input
	/// </summary>
	public static Vector3 Y (this Vector3 v) {
		return new UnityEngine.Vector3(0, v.y, 0);
	}

	/// <summary>
	/// Returns a new Vector only containing the Z component of the input
	/// </summary>
	public static Vector3 Z (this Vector3 v) {
		return new UnityEngine.Vector3(0, 0, v.z);
	}
	
	
	
	/// <summary>
	/// Returns a new Vector only containing the X component of the input
	/// </summary>
	public static Vector3 X (float val) {
		return new UnityEngine.Vector3(val, 0, 0);
	}
	
	/// <summary>
	/// Returns a new Vector only containing the Y component of the input
	/// </summary>
	public static Vector3 Y (float val) {
		return new UnityEngine.Vector3(0, val, 0);
	}
	
	/// <summary>
	/// Returns a new Vector only containing the Z component of the input
	/// </summary>
	public static Vector3 Z (float val) {
		return new UnityEngine.Vector3(0, 0, val);
	}
	
	
	/// <summary>
	/// Creates a vector3 from a vector2 where the x and y components are mapped to x and z
	/// </summary>
	/// <param name="v">V.</param>
	public static Vector3 XZ(Vector2 v) {
		return new UnityEngine.Vector3(v.x, 0, v.y);
	}
	
	/// <summary>
	/// Creates a vector3 from a vector2 where the x and y components are mapped to y and z
	/// </summary>
	/// <param name="v">V.</param>
	public static Vector3 YZ(Vector2 v) {
		return new UnityEngine.Vector3(0, v.x, v.y);
	}
	
	/// <summary>
	/// Creates a vector3 from a vector2 where the x and y components are mapped to z and x
	/// </summary>
	/// <param name="v">V.</param>
	public static Vector3 ZX(Vector2 v) {
		return new UnityEngine.Vector3(v.y, 0, v.x);
	}
	
	/// <summary>
	/// Creates a vector3 from a vector2 where the x and y components are mapped to y and x
	/// </summary>
	/// <param name="v">V.</param>
	public static Vector3 ZY(Vector2 v) {
		return new UnityEngine.Vector3(0, v.y, v.x);
	}
}