﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public static class RandomX {
	
	/// <summary>
	/// Returns random true/false
	/// </summary>
	public static bool boolean {
		get { return (UnityEngine.Random.value > 0.5f); }
	}
	
	/// <summary>
	/// Returns a random Vector2 of a circle edge 
	/// </summary>
	public static Vector2 onUnitCircle {
		get {
			float angle = RandomX.eulerAngle;
			return MathX.Degrees2Vector2(angle);
		}
	}

	/// <summary>
	/// Returns a random float between 0 and 360
	/// </summary>
	public static float eulerAngle {
		get {
			return UnityEngine.Random.value*360;
		}
	}

	/// <summary>
	/// Creates a Vector2 with each component set to a random euler angle.
	/// </summary>
	/// <value>The vector2 euler angles.</value>
	public static Vector2 vector2EulerAngles {
		get {
			return new Vector2(RandomX.eulerAngle, RandomX.eulerAngle);
		}
	}
	
	/// <summary>
	/// Creates a Vector3 with each component set to a random euler angle.
	/// </summary>
	/// <value>The vector3 euler angles.</value>
	public static Vector3 vector3EulerAngles {
		get {
			return new Vector3(RandomX.eulerAngle, RandomX.eulerAngle, RandomX.eulerAngle);
		}
	}
	
	/// <summary>
	/// Returns a boolean from a chance value between 0 and 1, where 0.25 is unlikely and 0.75 is likely.
	/// </summary>
	/// <param name="chance">Chance.</param>
	public static bool Chance(float chance) {
		return chance > UnityEngine.Random.value;
	}
	
	public static Vector2 Range(Vector2 min, Vector2 max) {
		return new Vector2 (UnityEngine.Random.Range(min.x, max.x), UnityEngine.Random.Range(min.y, max.y));
	}

	public static Vector3 Range(Vector3 min, Vector3 max) {
		return new Vector3 (UnityEngine.Random.Range(min.x, max.x), UnityEngine.Random.Range(min.y, max.y), UnityEngine.Random.Range(min.z, max.z));
	}

	public static Vector4 Range(Vector4 min, Vector4 max) {
		return new Vector4 (UnityEngine.Random.Range(min.x, max.x), UnityEngine.Random.Range(min.y, max.y), UnityEngine.Random.Range(min.z, max.z), UnityEngine.Random.Range(min.w, max.w));
	}

	/// <summary>
	/// Returns random int between min (inclusive) and max (exclusive). So will never return max.
	/// </summary>
	/// <param name="min">Minimum.</param>
	/// <param name="max">Max.</param>
	public static int RangeInt (int min, int max) {
		return (int)Mathf.Floor (UnityEngine.Random.Range (min, max));
	}

	/// <summary>
	/// Randomly splits an integer value into a list of many integer values
	/// </summary>
	/// <param name="total">Total to split.</param>
	/// <param name="numValues">Number of values to divide into.</param>
	/// <param name="allowNegativeValues">Allow negative values.</param>
	public static List<int> SplitInteger (int total, int numValues, bool allowNegativeValues = false) {
		int runningTotal = 0;
		List<int> result = new List<int> ();
		for (int i = 0; i < numValues - 1; i++) {
			int newValue = (allowNegativeValues) ? RandomX.RangeInt (-total, total) : RandomX.RangeInt (0, total - runningTotal);
			runningTotal += newValue;
			result.Add (newValue);
		}
		// wrap up
		result.Add (total - runningTotal);
		return result;
	}

	/// <summary>
	/// Randomly splits a float value into a list of many float values
	/// </summary>
	/// <param name="total">Total to split.</param>
	/// <param name="numValues">Number of values to divide into.</param>
	/// <param name="allowNegativeValues">Allow negative values.</param>
	public static List<float> SplitFloat (float total, int numValues, bool allowNegativeValues = false) {
		float runningTotal = 0;
		List<float> result = new List<float> ();
		for (int i = 0; i < numValues - 1; i++) {
			float newValue = (allowNegativeValues) ? UnityEngine.Random.Range (-total, total) : UnityEngine.Random.Range (0, total - runningTotal);
			runningTotal += newValue;
			result.Add (newValue);
		}
		// wrap up
		result.Add (total - runningTotal);
		return result;
	}

	/// <summary>
	/// Randomly splits a float value into a list of many float values
	/// </summary>
	/// <param name="total">Total to split.</param>
	/// <param name="numValues">Number of values to divide into.</param>
	/// <param name="allowNegativeValues">Allow negative values.</param>
	public static List<float> SplitFloat (float total, int numValues, int minValue, int maxValue) {
		float runningTotal = 0;
		List<float> result = new List<float> ();
		for (int i = 0; i < numValues - 1; i++) {
			float newValue = UnityEngine.Random.Range (0, total - runningTotal);
			runningTotal += newValue;
			result.Add (newValue);
		}
		// wrap up
		result.Add (total - runningTotal);
		return result;
	}
	
	
	/// <summary>
	/// Returns a random index, using the value of each array item as a weight
	/// </summary>
	/// <returns>The random index.</returns>
	/// <param name="weights">Weights.</param>
	public static int WeightedIndex(IList<float> weights){
		return RandomX.WeightedIndex(weights, weights.Total());
	}
	
	/// <summary>
	/// Returns a random index, using the value of each array item as a weight
	/// </summary>
	/// <returns>The random index.</returns>
	/// <param name="weights">Weights.</param>
	/// <param name="total">Total.</param>
	public static int WeightedIndex(IList<float> weights, float total){
		if(total == 0) return weights.RandomIndex();
		float currentValue = 0;
		float randomValue = UnityEngine.Random.Range(0f, total);
		for(int i = 0; i < weights.Count; i++){
			currentValue += weights[i];
			if(currentValue >= randomValue) return i;
		}
		Debug.LogError("Could not find a value. Total was: "+total+" and num values was "+weights.Count);
		return -1;
	}
	
	/// <summary>
	/// Returns a random index from an array of values, weighting the random choice with an array of weights.
	/// </summary>
	/// <returns>The random.</returns>
	/// <param name="values">Values.</param>
	/// <param name="weights">Weights.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T Weighted<T>(IList<T> values, IList<float> weights){
		if(values.Count != weights.Count) {
			Debug.LogError("Value and Weight counts are not identical!");
			return default(T);
		}
		
		int index = RandomX.WeightedIndex(weights);
		if(index == -1) return default(T);
		return values[index];
	}
}
