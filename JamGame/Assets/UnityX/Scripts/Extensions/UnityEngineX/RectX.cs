﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// TODO - change new Rect() to rect.Set()!
public static class RectX {
	
	/// <summary>
	/// Creates new rect that encapsulates a list of vectors.
	/// </summary>
	/// <param name="vectors">Vectors.</param>
	public static Rect CreateEncapsulating (IList<Vector2> vectors) {
		Rect rect = new Rect(vectors[0].x, vectors[0].y, 0, 0);
		for(int i = 1; i < vectors.Count; i++)
			rect = rect.Encapsulate(vectors[i]);
		return rect;
	}
	
	
	public static Rect CreateEncapsulating (params Rect[] rects) {
		Rect rect = new Rect(rects[0]);
		for(int i = 1; i < rects.Length; i++)
			rect = rect.Encapsulate(rects[i]);
		return rect;
	}
	
	
	/// <summary>
	/// DEPRECIATED - USE Rect.MinMaxRect!
	/// Creates from corners. Exactly the same as SetMinMax in the Bounds class.
	/// </summary>
	/// <returns>The from corners.</returns>
	/// <param name="topLeft">Top left.</param>
	/// <param name="bottomRight">Bottom right.</param>
	public static Rect CreateFromCorners (Vector2 topLeft, Vector2 bottomRight) {
		return new Rect(topLeft.x, topLeft.y, bottomRight.x - topLeft.x, bottomRight.y - topLeft.y);
	}
	
	public static Rect CreateFromCenter (Vector2 centerPosition, Vector2 size) {
		return CreateFromCenter(centerPosition.x, centerPosition.y, size.x, size.y);
	}
	
	public static Rect CreateFromCenter (float centerX, float centerY, float sizeX, float sizeY) {
		return new Rect(centerX - sizeX * 0.5f, centerY - sizeY * 0.5f, sizeX, sizeY);
	}

	public static Rect Lerp (Rect rect1, Rect rect2, float lerp) {
		Vector4 newRect = Vector4.Lerp(new Vector4(rect1.x, rect1.y, rect1.width, rect1.height), new Vector4(rect2.x, rect2.y, rect2.width, rect2.height), lerp);
		return new Rect(newRect.x, newRect.y, newRect.z, newRect.w);
	}
	
	public static Rect Add(this Rect left, Rect right){
		return new Rect(left.x+right.x, left.y+right.y, left.width+right.width, left.height+right.height);
	}
	
	public static Rect Subtract(this Rect left, Rect right){
		return new Rect(left.x-right.x, left.y-right.y, left.width-right.width, left.height-right.height);
	}

	public static Rect CopyWithX(this Rect r, float x){
		return new Rect(x, r.y, r.width, r.height);
	}

	public static Rect CopyWithY(this Rect r, float y){
		return new Rect(r.x, y, r.width, r.height);
	}

	public static Rect CopyWithWidth(this Rect r, float width){
		return new Rect(r.x, r.y, width, r.height);
	}

	public static Rect CopyWithHeight(this Rect r, float height){
		return new Rect(r.x, r.y, r.width, height);
	}

	public static Rect CopyWithPosition(this Rect r, Vector2 position){
		return new Rect(position.x, position.y, r.width, r.height);
	}

	public static Rect CopyWithSize(this Rect r, Vector2 size){
		return new Rect(r.x, r.y, size.x, size.y);
	}
	
	/// <summary>
	/// Expands (or contracts) the rect by a specified amount.
	/// </summary>
	/// <param name="r">The red component.</param>
	/// <param name="expansion">Expansion.</param>
	public static Rect Expand(this Rect r, Vector2 expansion){
		return Expand (r, expansion, Vector2X.half);
	}
	
	/// <summary>
	/// Expands (or contracts) the rect by a specified amount, using a pivot to control the expansion center point.
	/// </summary>
	/// <param name="r">The red component.</param>
	/// <param name="expansion">Expansion.</param>
	/// <param name="pivot">Pivot.</param>
	public static Rect Expand(this Rect r, Vector2 expansion, Vector2 pivot){
		return new Rect(r.x - (expansion.x * 0.5f * (pivot.x + 0.5f)), r.y - (expansion.y * 0.5f * (pivot.y + 0.5f)), r.width + (expansion.x * (pivot.x + 0.5f)), r.height + (expansion.y * (pivot.y + 0.5f)));
	}
	
	public static Rect Encapsulate(this Rect r, Rect rect) {
		r = r.Encapsulate(rect.min);
		r = r.Encapsulate(rect.max);
		return r;
	}
	
	public static Rect Encapsulate(this Rect r, Vector2 point){
		return RectX.CreateFromCorners (Vector2.Min (r.min, point), Vector2.Max (r.max, point));
	}
	
	public static Vector2[] Corners (this Rect r) {
		return new Vector2[4] {r.TopLeft(), r.TopRight(), r.BottomRight(), r.BottomLeft()};
	}
	
	public static Vector2 TopLeft(this Rect r){
		return r.min;
	}
	
	public static Vector2 TopRight(this Rect r){
		return new Vector2(r.x+r.width, r.y);
	}
	
	public static Vector2 BottomLeft(this Rect r){
		return new Vector2(r.x, r.y+r.height);
	}
	
	public static Vector2 BottomRight(this Rect r){
		return r.max;
	}
	
	/// <summary>
	/// Clamps a point inside the rect.
	/// </summary>
	/// <param name="r">The red component.</param>
	/// <param name="point">Point.</param>
	public static Vector2 Clamp(this Rect r, Vector2 point) {
		return Vector2X.Clamp(point, r.min, r.max);
	}
	
	public static Rect Clamp(Rect r, Rect container) {
		Rect ret = new Rect();
		ret.x = MathX.Clamp0Infinity(Mathf.Max(r.x, container.x));
		ret.y = MathX.Clamp0Infinity(Mathf.Max(r.y, container.y));
		ret.width = MathX.Clamp0Infinity(Mathf.Min(r.x + r.width, container.x + container.width) - ret.x);
		ret.height = MathX.Clamp0Infinity(Mathf.Min(r.y + r.height, container.y + container.height) - ret.y);
		
		float offsetX = MathX.Clamp0Infinity(r.x - Mathf.Max(ret.width, container.width));
		float offsetY = MathX.Clamp0Infinity(r.y - Mathf.Max(ret.height, container.height));
		ret.x -= offsetX;
		ret.y -= offsetY;
		return ret;
	}
	
	public static Rect ClampKeepSize(Rect r, Rect container) {
		Rect ret = new Rect();
		ret.x = MathX.Clamp0Infinity(Mathf.Max(r.x, container.x));
		ret.y = MathX.Clamp0Infinity(Mathf.Max(r.y, container.y));
		ret.width = Mathf.Min(r.width, container.width);
		ret.height = Mathf.Min(r.height, container.height);
		
		float offsetX = MathX.Clamp0Infinity((r.x + r.width) - (container.x + container.width));
		float offsetY = MathX.Clamp0Infinity((r.y + r.height) - (container.y + container.height));
		ret.x -= MathX.ClampMax(offsetX, ret.x);
		ret.y -= MathX.ClampMax(offsetY, ret.y);
		
		return ret;
	}
	
	/// <summary>
	/// Gets the vertices.
	/// Returns moving clockwise around from the center, starting with the top left.
	/// </summary>
	/// <returns>The vertices.</returns>
	/// <param name="rect">Rect.</param>
	public static Vector2[] GetVertices(this Rect rect) {
		Vector2[] vertices = new Vector2[4];
		Vector2 max = rect.max;
		vertices[0] = rect.min;
		vertices[1] = new Vector2(max.x, vertices[0].y);
		vertices[2] = max;
		vertices[3] = new Vector2(vertices[0].x, max.y);
		return vertices;
	}

	public static string ToStringShort(this Rect rect) {
		return "X: " + rect.x + " Y: " + rect.y + " W: " + rect.width + " H: " + rect.height;
	}
}
