﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class BoolX {

	/// <summary>
	/// Returns bool based on int value, as defined by C#.
	/// </summary>
	/// <returns>The bool.</returns>
	/// <param name="_bool">If set to <c>true</c> _bool.</param>
	public static bool ToBool(this int _int) {
		return System.Convert.ToBoolean(_int);
	}
		
	/// <summary>
	/// Returns int based on boolean value, as defined by C#. False is 0, True is 1.
	/// </summary>
	/// <returns>The int.</returns>
	/// <param name="_bool">If set to <c>true</c> _bool.</param>
	public static int ToInt(this bool _bool) {
		return System.Convert.ToInt32(_bool);
	}
	
	/// <summary>
	/// Returns int based on boolean value bool, where False is 0 and True is trueValue.
	/// </summary>
	/// <returns>The int.</returns>
	/// <param name="_bool">If set to <c>true</c> _bool.</param>
	/// <param name="trueValue">True value.</param>
	public static int ToInt(this bool _bool, int trueValue) {
		return _bool.ToInt(0, trueValue);
	}
	
	/// <summary>
	/// Returns int based on boolean value bool, where False is falseValue and True is trueValue.
	/// </summary>
	/// <returns>The int.</returns>
	/// <param name="_bool">If set to <c>true</c> _bool.</param>
	/// <param name="falseValue">False value.</param>
	/// <param name="trueValue">True value.</param>
	public static int ToInt(this bool _bool, int falseValue, int trueValue) {
		return _bool?trueValue:falseValue;
	}
	
	/// <summary>
	/// Returns int based on boolean value bool, where False is 0 and True is trueValue.
	/// </summary>
	/// <returns>The int.</returns>
	/// <param name="_bool">If set to <c>true</c> _bool.</param>
	/// <param name="trueValue">True value.</param>
	public static float ToFloat(this bool _bool, float trueValue) {
		return _bool.ToFloat(0, trueValue);
	}
	
	/// <summary>
	/// Returns int based on boolean value bool, where False is falseValue and True is trueValue.
	/// </summary>
	/// <returns>The int.</returns>
	/// <param name="_bool">If set to <c>true</c> _bool.</param>
	/// <param name="falseValue">False value.</param>
	/// <param name="trueValue">True value.</param>
	public static float ToFloat(this bool _bool, float falseValue, float trueValue) {
		return _bool?trueValue:falseValue;
	}
	
	/// <summary>
	/// Returns int based on boolean value bool, where False is falseValue and True is trueValue.
	/// </summary>
	/// <returns>The int.</returns>
	/// <param name="_bool">If set to <c>true</c> _bool.</param>
	/// <param name="falseValue">False value.</param>
	/// <param name="trueValue">True value.</param>
	public static T Pick<T>(this bool _bool, T falseValue, T trueValue) {
		return _bool?trueValue:falseValue;
	}
}
