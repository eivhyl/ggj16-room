﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public static class CanvasX {
	
	/// <summary>
	/// Converts a point in world space to a point in canvas space by converting from world space to screen space using a specified camera. Returns null if the point is behind the camera.
	/// </summary>
	/// <returns>The point to local point in rectangle.</returns>
	/// <param name="canvas">Canvas.</param>
	/// <param name="camera">Camera.</param>
	/// <param name="worldPosition">World position.</param>
	public static Vector3? WorldPointToLocalPointInRectangle (this Canvas canvas, Camera camera, Vector3 worldPosition) {
		Vector3 screenPoint = camera.WorldToScreenPoint(worldPosition);
		if(screenPoint.z < 0) return null;
		return canvas.ScreenPointToLocalPointInRectangle(screenPoint);
	}
	
	public static Vector3? ScreenPointToLocalPointInRectangle (this Canvas canvas, Vector2 screenPoint) {
		Camera camera = canvas.renderMode == RenderMode.ScreenSpaceCamera ? canvas.worldCamera : null;
		Vector2 localPosition;
		if(RectTransformUtility.ScreenPointToLocalPointInRectangle(canvas.GetComponent<RectTransform>(), screenPoint, camera, out localPosition))
			return localPosition;
		else return null;
	}
	
	
	public static Vector3? ScreenPointToCanvasSpace(this Canvas canvas, Vector2 screenPoint) {
		Camera camera = canvas.renderMode == RenderMode.ScreenSpaceCamera ? canvas.worldCamera : null;
		Vector3 canvasSpace = Vector3.zero;
		if(RectTransformUtility.ScreenPointToWorldPointInRectangle(canvas.GetComponent<RectTransform>(), screenPoint, camera, out canvasSpace))
			return canvasSpace;
		else return null;
	}
}
