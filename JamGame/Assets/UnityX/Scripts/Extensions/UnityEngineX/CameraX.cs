using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Extension methods for the UnityEngine.Camera class, and helper methods for working with cameras in general.
/// </summary>
public static class CameraX {

	#region Extensions for UnityEngine.Camera
	
	public static Vector2[] WorldToViewportPoints (this Camera camera, IList<Vector3> input) {
		Vector2[] output = new Vector2[input.Count];
		for(int i = 0; i < output.Length; i++)
			output[i] = camera.WorldToViewportPoint(input[i]);
		return output;
	}
	
	public static Vector2[] WorldToScreenPoints (this Camera camera, IList<Vector3> input) {
		Vector2[] output = new Vector2[input.Count];
		for(int i = 0; i < output.Length; i++)
			output[i] = camera.WorldToScreenPoint(input[i]);
		return output;
	}
	
	public static Vector3[] ViewportToWorldPoints (this Camera camera, IList<Vector3> input) {
		Vector3[] output = new Vector3[input.Count];
		for(int i = 0; i < output.Length; i++)
			output[i] = camera.ViewportToWorldPoint(input[i]);
		return output;
	}
	
	
	
	
	public static Vector3 ViewportToWorldPoint (Camera camera, float distance) {
		return ViewportToWorldPoint(camera, distance, new Vector2(0,0));
	}

	public static Vector3 ViewportToWorldPoint (Camera camera, float distance, Vector2 screenPosition) {	
		return camera.ViewportToWorldPoint(new Vector3(screenPosition.x, screenPosition.y, distance));
	}

	public static Vector3 ViewportToWorldScale (Camera camera, float distance) {
		return ViewportToWorldScale(camera, distance, new Vector2(1,1));
	}

	public static Vector3 ViewportToWorldScale (Camera camera, float distance, Vector2 screenScale) {	
		return camera.ViewportToWorldPoint(new Vector3(screenScale.x + 0.5f, screenScale.y + 0.5f, distance));
	}

	// public static Rect ViewportToWorldRect (Camera camera, float distance) {
	// 	return ViewportToWorldRect(camera, distance, new Vector2(0,0), new Vector2(1,1));
	// }

	// public static Rect ViewportToWorldRect (Camera camera, float distance, Rect screenRect) {
	// 	return ViewportToWorldRect(camera, distance, new Vector2(screenRect.x, screenRect.y), new Vector2(screenRect.width, screenRect.height));
	// }

	// public static Rect ViewportToWorldRect (Camera camera, float distance, Vector2 screenPosition, Vector2 screenScale) {	
	// 	Vector2 position = ViewportToWorldPoint(camera, distance, screenPosition);
	// 	Vector2 scale = ViewportToWorldScale(camera, distance, screenScale);
	// 	return new Rect(position.x, position.y, scale.x, scale.y);
	// }


	public static Vector3 WorldToScreenScale (Camera camera, float distance) {
		return ViewportToWorldPoint(camera, distance, new Vector2(0,0));
	}

	/*public static Vector3 TranslateBetweenCameras (Camera worldCamera, Vector3 worldPosition, Camera uiCamera) {
		return uiCamera.ViewportToWorldPoint(worldCamera.WorldToViewportPoint(worldPosition));
		//return ViewportToWorldPoint(camera, distance, new Vector2(0,0));
	}

	public */


	public static Bounds ScreenToWorldBounds (this Camera camera, Bounds screenBounds, float distance) {
		Vector3 center = camera.ScreenToWorldPoint(new Vector3(screenBounds.center.x, screenBounds.center.y, distance));
		Vector3 size = camera.ScreenToWorldPoint(new Vector3(screenBounds.size.x, screenBounds.size.y, distance));
		return new Bounds(center, size * 2);
	}

	public static Bounds ViewportToWorldBounds (this Camera camera, Bounds viewportBounds, float distance) {
		Vector3 center = camera.ViewportToWorldPoint(new Vector3(viewportBounds.center.x, viewportBounds.center.y, distance));
		Vector3 size = camera.ViewportToWorldPoint(new Vector3(viewportBounds.size.x, viewportBounds.size.y, distance));
		return new Bounds(center, size * 2);
	}

	public static Bounds WorldToScreenBounds (this Camera camera, Bounds worldBounds, float distance) {
		Vector3 center = camera.WorldToScreenPoint(new Vector3(worldBounds.center.x, worldBounds.center.y, distance));
		Vector3 size = camera.WorldToScreenPoint(new Vector3(worldBounds.size.x, worldBounds.size.y, distance));
		Bounds b = new Bounds(center, size * 2);
		return b;
	}

	public static Bounds WorldToViewportBounds (this Camera camera, Bounds worldBounds, float distance) {
		Vector3 center = camera.WorldToViewportPoint(new Vector3(worldBounds.center.x, worldBounds.center.y, distance));
		Vector3 size = camera.WorldToViewportPoint(new Vector3(worldBounds.size.x, worldBounds.size.y, distance));
		Bounds b = new Bounds(center, size * 2);
		return b;
	}
	
	
	public static Rect WorldToViewportRect (this Camera camera, Bounds worldBounds, float distance) {
		Vector2 center = camera.WorldToViewportPoint(new Vector3(worldBounds.center.x, worldBounds.center.y, distance));
		Vector2 size = camera.WorldToViewportPoint(new Vector3(worldBounds.size.x, worldBounds.size.y, distance));
		return RectX.CreateFromCenter(center, size);
	}
	
	public static Rect WorldToScreenRect (this Camera camera, Bounds worldBounds, float distance) {
		Vector2 center = camera.WorldToScreenPoint(new Vector3(worldBounds.center.x, worldBounds.center.y, distance));
		Vector2 size = camera.WorldToScreenPoint(new Vector3(worldBounds.size.x, worldBounds.size.y, distance));
		return RectX.CreateFromCenter(center, size);
	}
	
	
	public static Rect ScreenToWorldRect (this Camera camera, Rect screenRect, float distance) {
		Vector3 worldTopLeft = camera.ScreenToWorldPoint(new Vector3(screenRect.x, screenRect.y, distance));
		Vector3 worldBottomRight = camera.ScreenToWorldPoint(new Vector3(screenRect.x + screenRect.width, screenRect.y + screenRect.height, distance));
		return RectX.CreateFromCorners(worldTopLeft, worldBottomRight);
	}

	public static Rect ViewportToWorldRect (this Camera camera, Rect viewportRect, float distance) {
		Vector3 worldTopLeft = camera.ViewportToWorldPoint(new Vector3(viewportRect.x, viewportRect.y, distance));
		Vector3 worldBottomRight = camera.ViewportToWorldPoint(new Vector3(viewportRect.x + viewportRect.width, viewportRect.y + viewportRect.height, distance));
		return RectX.CreateFromCorners(worldTopLeft, worldBottomRight);
	}
	
	public static Rect WorldToScreenRect (this Camera camera, Rect worldRect, float distance) {
		Vector3 worldTopLeft = camera.WorldToScreenPoint(new Vector3(worldRect.x, worldRect.y, distance));
		Vector3 worldBottomRight = camera.WorldToScreenPoint(new Vector3(worldRect.x + worldRect.width, worldRect.y + worldRect.height, distance));
		return RectX.CreateFromCorners(worldTopLeft, worldBottomRight);
	}

	public static Rect WorldToViewportRect (this Camera camera, Rect worldRect, float distance) {
		Vector3 worldTopLeft = camera.WorldToViewportPoint(new Vector3(worldRect.x, worldRect.y, distance));
		Vector3 worldBottomRight = camera.WorldToViewportPoint(new Vector3(worldRect.x + worldRect.width, worldRect.y + worldRect.height, distance));
		return RectX.CreateFromCorners(worldTopLeft, worldBottomRight);
	}
	
	
	/// <summary>
	/// Focuses on bounds
	/// </summary>
	/// <param name="camera">Cmera.</param>
	/// <param name="go">Go.</param>
	public static void FocusOnBounds(this Camera camera, Bounds bounds) {
		// Get the radius of a sphere circumscribing the bounds
		float radius = bounds.size.magnitude / 2f;
		// Use the smaller FOV as it limits what would get cut off by the frustum        
		float fov = Mathf.Min(camera.fieldOfView, camera.GetHorizontalFieldOfView());
		float dist = radius / (Mathf.Sin(fov * 0.5f * Mathf.Deg2Rad));
		
		camera.transform.SetLocalPositionZ(dist);
		if (camera.orthographic)
			camera.orthographicSize = radius;
		
		// Frame the object hierarchy
		camera.transform.LookAt(bounds.center);
	}
	

	
	public static float GetHorizontalFieldOfView (this Camera camera) {
		return GetHorizontalFieldOfView(camera.fieldOfView, camera.aspect);
	}
	
	public static float GetFrustrumHeightAtDistance (this Camera camera, float distance) {
		return GetFrustrumHeightAtDistance(distance, camera.fieldOfView);
	}
	
	public static float GetFrustrumWidthAtDistance (this Camera camera, float distance) {
		return GetFrustrumWidthAtDistance(distance, camera.fieldOfView, camera.aspect);
	}
	
	public static float GetDistanceAtFrustrumHeight (this Camera camera, float frustumHeight) {
		return GetDistanceAtFrustrumHeight(frustumHeight, camera.fieldOfView);
	}
	
	public static float GetDistanceAtFrustrumWidth (this Camera camera, float frustumWidth) {
		return GetDistanceAtFrustrumWidth(frustumWidth, camera.fieldOfView, camera.aspect);
	}
	
	public static float GetFOVAngleAtWidthAndDistance (this Camera camera, float frustumWidth, float distance) {
		return GetFOVAngleAtWidthAndDistance(frustumWidth, distance, camera.aspect);
	}
	
	public static float ConvertFrustumWidthToFrustumHeight (this Camera camera, float frustumWidth) {
		return ConvertFrustumWidthToFrustumHeight(frustumWidth, camera.aspect);
	}
	
	public static float ConvertFrustumHeightToFrustumWidth (this Camera camera, float frustumHeight) {
		return ConvertFrustumHeightToFrustumWidth(frustumHeight, camera.aspect);
	}
	
	
	#endregion
	
	
	
	
	#region Camera Helper Functions
	
	
	public static float GetHorizontalFieldOfView (float verticalFieldOfView, float aspectRatio) {
		return 2f * Mathf.Atan(Mathf.Tan(verticalFieldOfView * Mathf.Deg2Rad * 0.5f) * aspectRatio) * Mathf.Rad2Deg;
	}
	
	
	public static float GetFrustrumHeightAtDistance (float distance, float fieldOfView) {
		return 2f * distance * Mathf.Tan(fieldOfView * 0.5f * Mathf.Deg2Rad);
	}
	
	public static float GetFrustrumWidthAtDistance (float distance, float fieldOfView, float aspectRatio) {
		return ConvertFrustumHeightToFrustumWidth(GetFrustrumHeightAtDistance(distance, fieldOfView), aspectRatio);
	}
	
	
	public static float GetDistanceAtFrustrumHeight (float frustumHeight, float fieldOfView) {
		return frustumHeight * 0.5f / Mathf.Tan(fieldOfView * 0.5f * Mathf.Deg2Rad);
	}
	
	public static float GetDistanceAtFrustrumWidth (float frustumWidth, float fieldOfView, float aspectRatio) {
		return GetDistanceAtFrustrumHeight(ConvertFrustumWidthToFrustumHeight(frustumWidth, aspectRatio), fieldOfView);
	}
	
	
	public static float GetFOVAngleAtHeightAndDistance (float frustumHeight, float distance) {
		return 2f * Mathf.Atan(frustumHeight * 0.5f / distance) * Mathf.Rad2Deg;
	}
	
	public static float GetFOVAngleAtWidthAndDistance (float frustumWidth, float distance, float aspectRatio) {
		return GetFOVAngleAtHeightAndDistance(ConvertFrustumWidthToFrustumHeight(frustumWidth, aspectRatio), distance);
	}
	
	
	public static float ConvertFrustumWidthToFrustumHeight (float frustumWidth, float aspectRatio) {
		return frustumWidth / aspectRatio;
	}
	
	public static float ConvertFrustumHeightToFrustumWidth (float frustumHeight, float aspectRatio) {
		return frustumHeight * aspectRatio;
	}
	
	#endregion
}
