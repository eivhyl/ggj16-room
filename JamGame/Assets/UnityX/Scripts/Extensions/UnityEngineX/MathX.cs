using UnityEngine;
using System;
using System.Collections.Generic;

public static class MathX {
	
	// Compensate damping value for framerate.
	// As used in calculations like:
	//    speed = speed * damping(0.97, frameTime)
	const float kStandardFrameTime = 1.0f/30.0f;
	public static float Damping(float dampVal, float frameTime = -1.0f, float deltaTime = kStandardFrameTime) {
		if( frameTime == -1.0f )
			frameTime = Time.deltaTime;
		
		float numFrames = frameTime / kStandardFrameTime;
		return Mathf.Pow(Mathf.Clamp01(dampVal), numFrames);
	}
	
	// Inverse of damping, used for calculations like:
	// x = lerp(x, xTarget, lerping(0.05, frameTime))
	public static float Lerping(float lerpVal, float frameTime) {
		return 1.0f - MathX.Damping(1.0f - lerpVal, frameTime);
	}
	
	public static float Squared(float val) {
		return val * val;
	}

	/// <summary>
	/// Repeat the specified value around a min and max.
	/// Note that unlike Unity's Repeat function, this function returns max when val == max instead of min.
	/// </summary>
	/// <param name="val">Value.</param>
	/// <param name="min">Minimum.</param>
	/// <param name="max">Max.</param>
	public static int Repeat (int val, int min, int max) {
		int range = max - min;
		if (val > max) 
			val -= range * Mathf.CeilToInt((val-max) / range);
		else if (val < min) 
			val += range * Mathf.CeilToInt((-val+min) / range);
		return val;
	}
	
	/// <summary>
	/// Repeat the specified value around a min and max.
	/// Note that unlike Unity's Repeat function, this function returns max when val == max instead of min.
	/// </summary>
	/// <param name="val">Value.</param>
	/// <param name="min">Minimum.</param>
	/// <param name="max">Max.</param>
	public static float Repeat (float val, float min, float max) {
		float range = max - min;
		if (val > max) 
			val -= range * Mathf.CeilToInt((val-max) / range);
		else if (val < min) 
			val += range * Mathf.CeilToInt((-val+min) / range);
		return val;
	}
	
	
	/// <summary>
	/// Returns the reciprocal of a number.
	/// </summary>
	public static float Reciprocal (this float f) {
		return 1f/f;
	}
	
	/// <summary>
	/// Returns true is the float is a whole number
	/// </summary>
	public static bool IsWhole (this float f) {
		return f.IsMultipleOf(1);
	}

	/// <summary>
	/// Returns true is the float is positive (more than or equal to zero)
	/// </summary>
	public static bool IsPositive (this float f) {
		return (f >= 0);
	}
	
	/// <summary>
	/// Determines if the float is even.
	/// </summary>
	/// <returns><c>true</c> if the specified f is even; otherwise, <c>false</c>.</returns>
	/// <param name="f">F.</param>
	public static bool IsEven (this int f) {
		return f % 2 == 0;
	}
	
	/// <summary>
	/// Determines if the float is odd.
	/// </summary>
	/// <returns><c>true</c> if the specified float is odd; otherwise, <c>false</c>.</returns>
	/// <param name="f">F.</param>
	public static bool IsOdd (this int f) {
		return f % 2 == 1;
	}
	
	
	/// <summary>
	/// Determines if the float is odd.
	/// </summary>
	/// <returns><c>true</c> if the specified float is odd; otherwise, <c>false</c>.</returns>
	/// <param name="f">F.</param>
	public static bool IsMultipleOf (this float f, float val) {
		return f % val == 0;
	}
	
	
	/// <summary>
	/// Returns the modulus value, not wrapping the value if the value is equal to, or is a factor if the mod value.
	/// For example, 4%4 returns 0. This method returns 4 instead.
	/// </summary>
	/// <returns><c>true</c>, if inclusive was moduloed, <c>false</c> otherwise.</returns>
	/// <param name="f">F.</param>
	public static float ModuloInclusive (float f, float modValue) {
		float output = f % modValue;
		if((output == 0) && (f != 0)/* && (f / modValue).IsWhole()*/)
			output = modValue;
		return output;
	}
	
	/// <summary>
	/// Returns a value indicating the sign of a number.
	/// </summary>
	public static int Sign (this float f, bool allowZero = false) {
		return ((allowZero && f.IsWhole()) ? 0 : (f.IsPositive() ? 1 : -1));
	}

	

	/// <summary>
	/// Random number between 0 and number.
	/// </summary>
	public static float Random( this float newNum) {
		return UnityEngine.Random.Range(0f, newNum);
	}

	/// <summary>
	/// Random number between negative and positive number
	/// </summary>
	public static float RandomRange( this float newNum) {
		return UnityEngine.Random.Range(-newNum, newNum);
	}
	

	/// <summary>
	/// Float as grayscale color value
	/// </summary>
	public static Color ToColor( this float _float){
        return new Color(_float,_float,_float);
    }
    

    //COMPARISON 

    /// <summary>
	/// Returns a similarity between 0 and 1 for two values and a max delta value. With a difference between this value, output will be 0.
	/// </summary>
    public static float Similarity(this float a, float b, float maxDifference) {
		return 1f-MathX.Normalize(0, Mathf.Abs(maxDifference), MathX.Difference(a, b));
    }

    //Similar to Unity's epsilon comparison, but allows for any precision.
    public static bool NearlyEqual(this float a, float b, float maxDifference = 0.001f) {
		if (a == b)  { 
			return true;
		} else {
			return MathX.Difference(a, b) < maxDifference;
	    }
	}

	// Returns true if all values are nearly equal
	public static bool NearlyEqual(IList<float> input, float maxDifference = 0.001f) {
		if(input.Count < 2) return true;
		for(int i = 1; i < input.Count; i++)
			if(!input[i].NearlyEqual(input[0], maxDifference)) return false;
		return true;
	}

	// Returns true if all values are equal
	public static bool Same(params float[] input) {
		if(input.Length < 2) return true;
		for(int i = 1; i < input.Length; i++)
			if(input[i] != input[0]) return false;
		return true;
	}


	/// <summary>
	/// Determines if f is more than a, and less than to b.
	/// </summary>
	/// <returns><c>true</c> if f is more than a, and less than to b.; otherwise, <c>false</c>.</returns>
	/// <param name="f">F.</param>
	/// <param name="a">A.</param>
	/// <param name="b">B.</param>
	public static bool IsBetween(this float f, float a, float b) {
		return f > a && f < b;
	}
	
	/// <summary>
	/// Determines if f is equal or more than a, and less than or equal to b.
	/// </summary>
	/// <returns><c>true</c> if f is equal or more than a, and less than or equal to b.; otherwise, <c>false</c>.</returns>
	/// <param name="f">F.</param>
	/// <param name="a">A.</param>
	/// <param name="b">B.</param>
	public static bool IsBetweenInclusive(this float f, float a, float b) {
		return f >= a && f <= b;
	}
	
	/// <summary>
	/// Difference between a and b. Similar to Vector3.Distance
	/// </summary>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	public static float Difference(float a, float b) {
		return Mathf.Abs(a - b);
	}
	
	/// <summary>
	/// Returns the index of the closest value to the target in a list using a binary search.
	/// </summary>
	/// <returns>The index in sorted list.</returns>
	/// <param name="values">Values.</param>
	/// <param name="target">Target.</param>
	public static int ClosestIndexInSortedList(IList<float> values, float target) {
		int low = 0;
		int high = values.Count - 1;
		
		if (high < 0)
			Debug.LogError("The array cannot be empty");
		while (low <= high) {
			int mid = (int)((uint)(low + high) >> 1);
			
			float midVal = values[mid];
			
			if (midVal < target)
				low = mid + 1;
			else if (midVal > target)
				high = mid - 1;
			else
				return mid; // key found
		}
		if(high < 0) {
			high = 0;
		}
		return high;
	}
	
	
	/// <summary>
	/// Returns the index of the closest value to the target in a list
	/// </summary>
	/// <param name="values">The list to search</param>
	/// <returns>An integer index of the closest value to the target in a list</returns>
	public static int ClosestIndex(IList<float> values, float target){
		float closest = values[0];
		int index = 0;
		for(int i = 1; i < values.Count; i++){
			if(MathX.Closer(target, values[i], closest)) {
				closest = values[i];
				index = i;
			}
		}
		return index;
	}

	/// <summary>
	/// Finds the closest value to a target between two values and returns the value
	/// </summary>
	/// <param name="a">The first value to check</param>
	/// <param name="b">The second value to check</param>
	/// <param name="target">The value to compare to</param>
	/// <returns>The closest value</returns>
	public static float Closest (float a, float b, float target) {
		if(Closer(target, a, b)) return a;
		else return b;
	}

	/// <summary>
	/// Returns true if a is closer to the target, else b.
	/// </summary>
	/// <param name="a">The first value to check</param>
	/// <param name="b">The second value to check</param>
	/// <param name="target">The value to compare to</param>
	/// <returns>True if A is closer than B to the target, else false</returns>
	public static bool Closer (float a, float b, float target) {
		return (MathX.Difference(a, target) < MathX.Difference(b, target));
	}

	
	/// <summary>
	/// Finds the smallest item in a list
	/// </summary>
	/// <param name="values">The list to search</param>
	/// <returns>A float representing the largest item in the list</returns>
	public static float Smallest(this IList<float> values){
		float smallest = values[0];
		for(int i = 1; i < values.Count; i++){
			smallest = Mathf.Min(smallest, values[i]);
		}
		return smallest;
	}

	/// <summary>
	/// Finds the largest item in a list
	/// </summary>
	/// <param name="values">The list to search</param>
	/// <returns>A float representing the largest item in the list</returns>
	public static float Largest(this IList<float> values){
		float largest = values[0];
		for(int i = 1; i < values.Count; i++){
			largest = Mathf.Max(largest, values[i]);
		}
		return largest;
	}

	/// <summary>
	/// Finds the smallest item in a list
	/// </summary>
	/// <param name="values">The list to search</param>
	/// <returns>An integer index of the largest item in the list</returns>
	public static int SmallestIndex(this IList<float> values){
		int index = 0;
		float smallest = values[index];
		for(int i = 1; i < values.Count; i++){
			if(values[i] < smallest) {
        		smallest = values[i];
        		index = i;
    		}
		}
		return index;
	}

	/// <summary>
	/// Finds the largest item in a list
	/// </summary>
	/// <param name="values">The list to search</param>
	/// <returns>An integer index of the largest item in the list</returns>
	public static int LargestIndex(this IList<float> values){
		int index = 0;
		float largest = values[index];
		for(int i = 1; i < values.Count; i++){
			if(values[i] > largest) {
        		largest = values[i];
        		index = i;
    		}
		}
		return index;
	}
	
	/// <summary>
	/// Finds the difference between the smallest and largest items in a list.
	/// </summary>
	/// <param name="values">The list to search</param>
	/// <returns>An integer index of the largest item in the list</returns>
	public static float LargestDifference(this IList<float> values){
		float smallest = values[0];
		float largest = values[0];
		for(int i = 1; i < values.Count; i++){
			if(values[i] < smallest) {
				smallest = values[i];
			}
			if(values[i] > largest) {
				largest = values[i];
			}
		}
		return largest - smallest;
	}
	

	/// <summary>
	/// Finds the absolute smallest item in a list
	/// </summary>
	/// <param name="values">The list to search</param>
	/// <returns>An integer index of the absolute smallest item in the list</returns>
	public static int AbsSmallestIndex(this IList<float> values){
		int index = 0;
		float smallest = Mathf.Abs(values[index]);
		for(int i = 1; i < values.Count; i++){
			if(Mathf.Abs(values[i]) < smallest) {
        		smallest = Mathf.Abs(values[i]);
        		index = i;
    		}
		}
		return index;
	}

	/// <summary>
	/// Finds the absolute largest item in a list
	/// </summary>
	/// <param name="values">The list to search</param>
	/// <returns>An integer index of the absolute largest item in the list</returns>
	public static int AbsLargestIndex(this IList<float> values){
		int index = 0;
		float largest = Mathf.Abs(values[index]);
		for(int i = 1; i < values.Count; i++){
			if(Mathf.Abs(values[i]) > largest) {
        		largest = Mathf.Abs(values[i]);
        		index = i;
    		}
		}
		return index;
	}

	/// <summary>
	/// Finds the average value from a list
	/// </summary>
	/// <param name="values">The list from which to find the average</param>
	/// <returns>A float equal to the average value of the list</returns>
	public static float Average(this IList<float> values){
		return values.Total()/values.Count;
	}

	/// <summary>
	/// Returns the total of the specified values.
	/// </summary>
	/// <param name="values">Values.</param>
	public static float Total(this IList<float> values){
		float total = 0;
		int count = values.Count;
		for(int i = 0; i < count; i++)
			total += values[i];
		return total;
	}

	// public virtual void Add(IList<T> _mapArray) {
	// 	if(mapArray.Count != _mapArray.Count) Debug.LogWarning("Map arrays are of different length");
	// 	for(int i = 0; i < mapArray.Length; i++) {
	// 		mapArray[i] += _mapArray[i];
	// 	}
	// }

	// public virtual void Subtract(IList<T> _mapArray) {
	// 	if(mapArray.Count != _mapArray.Count) Debug.LogWarning("Map arrays are of different length");
	// 	for(int i = 0; i < mapArray.Length; i++) {
	// 		mapArray[i] -= _mapArray[i];
	// 	}
	// }

	// public virtual void Multiply(IList<T> _mapArray) {
	// 	if(mapArray.Count != _mapArray.Count) Debug.LogWarning("Map arrays are of different length");
	// 	for(int i = 0; i < mapArray.Length; i++) {
	// 		mapArray[i] *= _mapArray[i];
	// 	}
	// }

	// public virtual void Divide(IList<T> _mapArray) {
	// 	if(mapArray.Count != _mapArray.Count) Debug.LogWarning("Map arrays are of different length");
	// 	for(int i = 0; i < mapArray.Length; i++) {
	// 		mapArray[i] /= _mapArray[i];
	// 	}
	// }

	//Absolute value as extension method
	public static float Abs(this float f) {
		return Mathf.Abs(f);
	}
	
	

	//Returns percentage value between value and max
	public static float Percentage(float val, float max) {
		return Fraction(val, max) * 100;
	}


	// ANGLES
	
	/// <summary>
	/// Returns degrees given a value and a max
	/// </summary>
	/// <param name="val">Value.</param>
	/// <param name="max">Max.</param>
	public static float Radians (float val, float max) {
		return Degrees(val, max) * Mathf.Deg2Rad;
	}
	
	/// <summary>
	/// Returns degrees given a value and a max
	/// </summary>
	/// <param name="val">Value.</param>
	/// <param name="max">Max.</param>
	public static float Degrees (float val, float max) {
		return Fraction(val, max) * 360f;
	}

	/// <summary>
	/// The sin of an angle in degrees.
	/// </summary>
	/// <param name="angleInDegrees">Angle in degrees.</param>
	public static float SinD(float angleInDegrees) {
		return Mathf.Sin(angleInDegrees * Mathf.Deg2Rad);
	}

	/// <summary>
	/// The cos of an angle in degrees.
	/// </summary>
	/// <param name="angleInDegrees">Angle in degrees.</param>
	public static float CosD(float angleInDegrees) {
		return Mathf.Cos(angleInDegrees * Mathf.Deg2Rad);
	}

	/// <summary>
	/// The tan of an angle in degrees.
	/// </summary>
	/// <param name="angleInDegrees">Angle in degrees.</param>
	public static float TanD(float angleInDegrees) {
		return Mathf.Tan(angleInDegrees * Mathf.Deg2Rad);
	}
	
	
	/// <summary>
	/// Wraps a number around 360
	/// </summary>
	/// <returns>The degrees.</returns>
	/// <param name="degrees">Degrees.</param>
	public static float WrapDegrees (float degrees) {
		return MathX.Repeat(degrees, -180, 180);
	}
	
	/// <summary>
	/// Returns the difference between two degrees, wrapping values where appropriate.
	/// </summary>
	/// <param name="val">Value.</param>
	/// <param name="max">Max.</param>
	public static float DeltaDegrees (float a, float b) {
		float delta = a-b;
		return WrapDegrees(delta);
	}
	
	/// <summary>
	/// Returns the difference between two degrees, wrapping values where appropriate.
	/// </summary>
	/// <param name="val">Value.</param>
	/// <param name="max">Max.</param>
	public static float AbsDeltaDegrees (float a, float b) {
		return DeltaDegrees(a, b).Abs ();
	}
	
	public static float ClampMax (float val, float max) {
		if(val > max) return max;
		return val;
	}
	
	public static float ClampMin (float val, float min) {
		if(val < min) return min;
		return val;
	}
	
	/// <summary>
	/// Wraps an angle around 360 and clamps it between min and max
	/// </summary>
	/// <returns>The degrees.</returns>
	/// <param name="degrees">Degrees.</param>
	/// <param name="min">Minimum.</param>
	/// <param name="max">Max.</param>
	public static float ClampDegrees (float degrees, float min, float max) {
		return Mathf.Clamp (MathX.WrapDegrees(degrees), min, max);
	}
	
	/// <summary>
	/// Angle in degrees to normalized direction as Vector2
	/// </summary>
	/// <returns>The vector2.</returns>
	/// <param name="degrees">Degrees.</param>
	public static Vector2 Degrees2Vector2 (float degrees) {
		return Radians2Vector2(degrees * Mathf.Deg2Rad);
	}
	
	/// <summary>
	/// Angle in degrees to normalized direction as Vector2
	/// </summary>
	/// <returns>The vector2.</returns>
	/// <param name="radians">Radians.</param>
	public static Vector2 Radians2Vector2 (float radians) {
		float sin = Mathf.Sin(radians);
		float cos = Mathf.Cos(radians);
		return new Vector2(sin, cos);
	}

	/// <summary>
	/// Returns a random point on the edge of a square.
	/// </summary>
	/// <returns>The square edge.</returns>
	/// <param name="sideLength">Side length.</param>
	public static Vector2 RandomSquareEdge(float sideLength) {
		return new Vector2(UnityEngine.Random.Range(-sideLength, sideLength) * 0.5f, UnityEngine.Random.Range(-sideLength, sideLength) * 0.5f);
	}

	/// <summary>
	// Transforms points on a circle edge to points on a square edge.
	/// </summary>
	/// <returns>The coordinates to square.</returns>
	/// <param name="circlePosition">Circle position.</param>
	public static Vector2 CircleCoordinatesToSquare(Vector2 circlePosition){
		if(circlePosition.x < 0 && circlePosition.y < 0){
			if (circlePosition.x < circlePosition.y){
				return new Vector2(-circlePosition.x * 0.5f/circlePosition.y, 0.5f);
			} else {
				return new Vector2(0.5f, -circlePosition.x * 0.5f/circlePosition.y);
			}
		} else {
			if (circlePosition.x < circlePosition.y){
				return new Vector2(circlePosition.x * 0.5f/circlePosition.y, 0.5f);
			} else {
				return new Vector2(0.5f, circlePosition.x * 0.5f/circlePosition.y);
			}
		}	
	}

	
	/// <summary>
	//Pythagoras theorem. Returns length side of a triangle, where other side lengths are a and b.
	/// </summary>
	/// <param name="a">A.</param>
	/// <param name="b">B.</param>
	public static float Pythagoras(float a, float b) {
		return Mathf.Sqrt(a * a + b * b);
	}
	

	//Returns true if val is between min and max
	public static bool InRange(float val, float min, float max) {
		return (val >= min && val <= max);
	}

	
	
	/// <summary>
	/// Factorial of n.
	/// </summary>
	/// <param name="n">N.</param>
	public static float Factorial (this int n){
		if (n <= 1){ 
			return 1;
		} else { 
			return n * MathX.Factorial(n-1); 
		}
	}
	


	//Oscellates a value between 0 and 1, where a value of 0 yields 0, 0.5 yields 1, and 1 returns to 0.
	public static float Oscellate (float value) {
		return 1f-(Mathf.Cos(Mathf.PI * 2 * value) + 1f) * 0.5f;
	}

	//Oscellates a value between min and max, where a value of 0 yields min, 0.5 yields max, and 1 returns to min.
	public static float Oscellate (float value, float min, float max) {
		return Mathf.Lerp(min, max, Oscellate(value));
	}

	//Oscellates a value between 0 and 1, where a value of 0 yields 0, 0.5 yields 1, and 1 returns to 0. Repeats input.
	public static float OscellateRepeating (float value) {
		return 1f-(Mathf.Cos(Mathf.PI * 2 * (value%1)) + 1f) * 0.5f;
	}

	//Oscellates a value between min and max, where a value of 0 yields min, 0.5 yields max, and 1 returns to min. Repeats input.
	public static float OscellateRepeating (float value, float min, float max) {
		return Mathf.Lerp(min, max, OscellateRepeating(value));
	}
	
	
	
	//Oscellates a value between 0 and 1 using a triangle wave
	public static float OscellateTriangleRepeating (float value) {
		return 1f-MathX.Difference(1f, ((value * 2) % 2));
	}
	
	//Oscellates a value between 0 and 1 using a sawtooth wave
	public static float OscellateSawtoothRepeating (float value) {
		return (OscellateTriangleRepeating(value) < 0.5f ? 0 : 1);
	}
	
	// DEPRECIATED! USE Mathf.InverseLerp instead!
	/// <summary>
    /// Returns a normalized value between 0 and 1 for the value within the range min to max.
    /// </summary>
    /// <param name="min">Minimum value in range.</param>
    /// <param name="max">Max value in range.</param>
    /// <param name="value">Value to be normalized.</param>
    public static float Normalize(float min, float max, float value) {	
		Debug.LogWarning ("DEPRECIATED! USE Mathf.InverseLerp instead!");
    	//If all three inputs are equal, return 0 instead of NAN.
    	if(min == max && min == value) return 0;
        return Mathf.Lerp(0, 1, ((value - min) / (max - min)));
    }


	//CLAMP
	
	public static float Clamp0Infinity(float value) {
		return Mathf.Clamp(value, 0, Mathf.Infinity);
	}

	public static float Clamp1Infinity(float value) {
		return Mathf.Clamp(value, 1, Mathf.Infinity);
	}
	
	public static int Clamp0Infinity(int value) {
		return Mathf.Clamp(value, 0, int.MaxValue);
	}
	
	public static int Clamp1Infinity(int value) {
		return Mathf.Clamp(value, 1, int.MaxValue);
	}
	
	
	
	//ROUND
	
	//Seems about twice as fast as Unity's FloorToInt
	public static int FastFloorToInt(float x) {
		return x>=0 ? (int)x : -(int)x;
	}

	
	/// <summary>
	/// Rounds a value to a specified power of ten. 
	/// Decimal places can be positive or negative
	/// USED AS SO: (123.456f).RoundTo(-2); = 100;
	/// USED AS SO: (123.456f).RoundTo(2); = 123.46;
	/// </summary>
	/// <returns>The to.</returns>
	/// <param name="newNum">New number.</param>
	/// <param name="decimalPlaces">Decimal places.</param>
	public static float RoundTo( this float newNum, int decimalPlaces){
		return Mathf.Round(newNum*Mathf.Pow(10,decimalPlaces)) / Mathf.Pow(10,decimalPlaces);
	}

	//Floor to X decimal places
	public static float FloorTo( this float newNum, int decimalPlaces){
		return Mathf.Floor(newNum*Mathf.Pow(10,decimalPlaces)) / Mathf.Pow(10,decimalPlaces);
	}

	//Ceil to X decimal places
	public static float CeilTo( this float newNum, int decimalPlaces){
		return Mathf.Ceil(newNum*Mathf.Pow(10,decimalPlaces)) / Mathf.Pow(10,decimalPlaces);
	}

	//Round to int
	public static int RoundToInt( this float newNum){
		return Mathf.RoundToInt(newNum);
	}

	//Floor to int
	public static int FloorToInt( this float newNum){
		return MathX.FastFloorToInt(newNum);
	}	

	//Ceil to int
	public static int CeilToInt( this float newNum){
		return Mathf.RoundToInt(newNum);
	}


	public static float RoundToSig(this float d, int digits){
		if(d == 0) return 0;
		float scale = Mathf.Pow(10, Mathf.Floor(Mathf.Log10(Mathf.Abs(d))) + 1);
		return scale * (d / scale).RoundTo(digits);
	}

	public static float FloorToSig(this float d, int digits){
		if(d == 0) return 0;
		float scale = Mathf.Pow(10, Mathf.Floor(Mathf.Log10(Mathf.Abs(d))) + 1);
		return scale * (d / scale).FloorTo(digits);
	}

	public static float CeilToSig(this float d, int digits){
		if(d == 0) return 0;
		float scale = Mathf.Pow(10, Mathf.Floor(Mathf.Log10(Mathf.Abs(d))) + 1);
		return scale * (d / scale).CeilTo(digits);
	}
	
	public static int RoundToSigInt(this float d, int digits){
		return d.RoundToSig(digits).RoundToInt();
	}

	public static int FloorToSigInt(this float d, int digits){
		return d.FloorToSig(digits).RoundToInt();
	}

	public static int CeilToSigInt(this float d, int digits){
		return d.CeilToSig(digits).RoundToInt();
	}

	// static double TruncateToSignificantDigits(this double d, int digits){
	//     if(d == 0)
	//         return 0;

	//     double scale = Math.Pow(10, Math.Floor(Math.Log10(Math.Abs(d))) + 1 - digits);
	//     return scale * Math.Truncate(d / scale);
	// }
	
	/// <summary>
	/// Rounds to an interval of nearestValue.
	/// </summary>
	/// <returns>The rounded value.</returns>
	/// <param name="newNum">Value.</param>
	/// <param name="nearestValue">Nearest value.</param>
	public static float RoundToNearest(this float newNum, float nearestValue){
		return Mathf.Round(newNum/nearestValue)*nearestValue;
	}

	/// <summary>
	/// Floors to an interval of nearestValue.
	/// </summary>
	/// <returns>The rounded value.</returns>
	/// <param name="newNum">Value.</param>
	/// <param name="nearestValue">Nearest value.</param>
	public static float FloorToNearest(this float newNum, float nearestValue){
		return Mathf.Floor(newNum/nearestValue)*nearestValue;
	}

	/// <summary>
	/// Ceils to an interval of nearestValue.
	/// </summary>
	/// <returns>The rounded value.</returns>
	/// <param name="newNum">Value.</param>
	/// <param name="nearestValue">Nearest value.</param>
	public static float CeilToNearest(this float newNum, float nearestValue){
		return Mathf.Ceil(newNum/nearestValue)*nearestValue;
	}
	
	/// <summary>
	/// Rounds to an interval of nearestValue.
	/// </summary>
	/// <returns>The rounded value.</returns>
	/// <param name="newNum">Value.</param>
	/// <param name="nearestValue">Nearest value.</param>
	public static int RoundToNearestInt(this float newNum, float nearestValue){
		return Mathf.RoundToInt(Mathf.Round(newNum/nearestValue)*nearestValue);
	}

	/// <summary>
	/// Floors to an interval of nearestValue.
	/// </summary>
	/// <returns>The rounded value.</returns>
	/// <param name="newNum">Value.</param>
	/// <param name="nearestValue">Nearest value.</param>
	public static int FloorToNearestInt(this float newNum, float nearestValue){
		return Mathf.FloorToInt(Mathf.Floor(newNum/nearestValue)*nearestValue);
	}

	/// <summary>
	/// Ceils to an interval of nearestValue.
	/// </summary>
	/// <returns>The rounded value.</returns>
	/// <param name="newNum">Value.</param>
	/// <param name="nearestValue">Nearest value.</param>
	public static int CeilToNearestInt(this float newNum, float nearestValue){
		return Mathf.CeilToInt(Mathf.Ceil(newNum/nearestValue)*nearestValue);
	}

	


    //FRACTIONS
    
    //Returns fraction value between value and max
	public static float Fraction(float top, float bottom) {
		return top/bottom;
	}

    //Greatest Common Denominator
	public static int GCD(int num1, int num2) {
		while (num1 != num2) {
			if (num1 > num2)
				num1 = num1 - num2;
			if (num2 > num1)
				num2 = num2 - num1;
		}
		return num1;
	}

	//Lowest Common Multiple
	public static int LCM(int num1, int num2) {
		return (num1 * num2) / MathX.GCD(num1, num2);
	}

	//SERIES

	//Returns the value in the Renard sequence
	// From http://en.wikipedia.org/wiki/Preferred_number
	public static float RenardSeries(int seriesValue, int i) {
		return Mathf.Pow(10, ((float)i/seriesValue));//.RoundToSig(1);
	}

	public static int ClosestRenardSeriesIndex(int seriesValue, float val) {
		float floatIndex = Mathf.Log(val)/Mathf.Log(10) * seriesValue;
		return floatIndex.RoundToInt();
	}

	//Returns the value in the Renard sequence
	// From http://en.wikipedia.org/wiki/Preferred_number
	public static float OneTwoFiveSeries(int i) {
		return RenardSeries(3, i).RoundToSig(1);
	}


	//GEOMETRY

	public static float VolumeFromRadius(float radius){
		return (4/3)*Mathf.PI*(Mathf.Pow(radius, 3));
	}
	
	//INTERPOLATE FUNCTIONS.

	//Also known as Linear Dodge
	public static float LerpAdditive(float float1, float float2, float lerp = 1f){
		return Mathf.Lerp(float1, float1 + float2, lerp);
	}

	//Multiply
	public static float LerpMultiply(float float1, float float2, float lerp = 1f){
		return Mathf.Lerp(float1, float1 * float2, lerp);
	}

	//Screen
	public static float LerpScreen(float float1, float float2, float lerp = 1f){
		return Mathf.Lerp(float1, 1 - ((1 - float1) * (1 - float2)), lerp);
	}

	//This doesn't quite mirror what photoshop do - I think they do both techniques (multiply and screen - thats what this is) at the same time, while we do one or the other.
	public static float LerpOverlay(float float1, float float2, float lerp = 1f){
		if(float2 < 0.5f){
			return Mathf.Lerp(float1, float1 * (float2 * 2f), lerp);
		} else {
			return Mathf.Lerp(float1, 1f - ((1f - float1) * (1f - float2 * 2f)), lerp);
		}
	}

	//Lighten
	public static float LerpLighten(float float1, float float2, float lerp = 1f){
		return Mathf.Lerp(float1, Mathf.Max(float1, float2), lerp);
	}
	
	//Darken
	public static float LerpDarken(float float1, float float2, float lerp = 1f){
		return Mathf.Lerp(float1, Mathf.Min(float1, float2), lerp);
	}

	//Difference
	public static float LerpDifference(float float1, float float2, float lerp = 1f){
		return Mathf.Lerp(float1, Mathf.Max(float1, float2) - Mathf.Min(float1, float2), lerp);
	}
	
	
	
	/// <summary>
	/// Returns the total of the specified values.
	/// </summary>
	/// <param name="values">Values.</param>
	public static float[] Divide(this IList<float> values, float divider){
		float[] newValues = new float[values.Count];
		for(int i = 0; i < newValues.Length; i++)
			newValues[i] = values[i]/divider;
		return newValues;
	}
	
	public static IList<float> Multiply(this IList<float> values, float multiplier){
		for(int i = 0; i < values.Count; i++)
			values[i] = values[i] * multiplier;
		return values;
	}
	
	
	/// <summary>
	/// Normalizes a list of floats so that the total equals total
	/// </summary>
	/// <param name="values">Values.</param>
	/// <param name="total">The normalized total (1 by default).</param>
	public static float[] Normalize(this IList<float> values, float total = 1) {
		float valueTotal = values.Total();
		valueTotal /= total;
		return values.Divide (valueTotal);
	}
	
	/// <summary>
	/// Returns a number on a curve that returns 0 where x is 0, 1 where x is 0.5, and infinity where x is 1
	/// </summary>
	/// <returns>The infinity curve.</returns>
	/// <param name="x">The x coordinate.</param>
	public static float ZeroInfinityCurve (float x) {
		return -1/(x-1) - 1;
	}
	
	
	
	
	// Passing null for either maxWidth or maxHeight maintains aspect ratio while
	//        the other non-null parameter is guaranteed to be constrained to
	//        its maximum value.
	//
	//  Example: maxHeight = 50, maxWidth = null
	//    Constrain the height to a maximum value of 50, respecting the aspect
	//    ratio, to any width.
	//
	//  Example: maxHeight = 100, maxWidth = 90
	//    Constrain the height to a maximum of 100 and width to a maximum of 90
	//    whichever comes first.
	public static Vector2 ScaleSize(Vector2 from, float? maxWidth, float? maxHeight) {
		float? widthScale = null;
		float? heightScale = null;
		
		if (maxWidth.HasValue) {
			widthScale = maxWidth.Value / from.x;
		}
		if (maxHeight.HasValue) {
			heightScale = maxHeight.Value / from.y;
		}
		
		float scale = Mathf.Min((float)(widthScale ?? heightScale), (float)(heightScale ?? widthScale));
		return new Vector2(Mathf.FloorToInt(from.x * scale), Mathf.CeilToInt(from.y * scale));
	}
}
