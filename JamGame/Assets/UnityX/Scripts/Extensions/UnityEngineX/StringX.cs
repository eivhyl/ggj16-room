﻿using UnityEngine;
using System;
using System.Collections;
using System.Text;

public static class StringX {
	
	/// <summary>
	/// Determines whether this instance is uppercase.
	/// </summary>
	/// <returns><c>true</c> if this instance is upper; otherwise, <c>false</c>.</returns>
	/// <param name="input">Input.</param>
	/// <param name="ignoreNonAlphabeticCharacters">If set to <c>true</c> ignore non alphabetic characters.</param>
	public static bool IsUpper(this string input, bool ignoreNonAlphabeticCharacters = true) {
		for (int i = 0; i < input.Length; i++) {
			if ((!ignoreNonAlphabeticCharacters && !char.IsLetter(input[i])) || !char.IsUpper(input[i]))
				return false;
		}
		return true;
	}
	
	static string UppercaseFirstCharacter(string s){
		if (string.IsNullOrEmpty(s)) return string.Empty;
		char[] a = s.ToCharArray();
		a[0] = char.ToUpperInvariant(a[0]);
		return new string(a);
	}
	
	static string LowercaseFirstCharacter(string s){
		if (string.IsNullOrEmpty(s)) return string.Empty;
		char[] a = s.ToCharArray();
		a[0] = char.ToLowerInvariant(a[0]);
		return new string(a);
	}
	
	//For convinience. Also, marginally faster! http://www.dotnetperls.com/isnullorempty
	public static bool IsNullOrEmpty(this string s) {
		return (s == null || s.Length == 0);
	}

	//Replacement until Unity upgrades .Net
	public static bool IsNullOrWhiteSpace(this string s){
		return (IsNullOrEmpty(s) || s.IsWhiteSpace());
	}

	//Returns true if string is only white space
	public static bool IsWhiteSpace(this string s){
		foreach(char c in s){
			if(c != ' ' && c != '\t') return false;
		}
		return true;
	}
	
	/// <summary>
	/// Contains the specified source, toCheck and comp.
	/// </summary>
	/// <param name="source">Source.</param>
	/// <param name="toCheck">To check.</param>
	/// <param name="comp">Comp.</param>
	public static bool Contains(this string source, string toCheck, StringComparison comp) {
		return source.IndexOf(toCheck, comp) >= 0;
	}

	//Returns true if string contains any white space
	public static bool ContainsWhiteSpace(this string s){
		foreach(char c in s) {
			if(c == ' ') return true;
		}
		return true;
	}

	//Returns true if string contains any of the listed strings
	public static bool ContainsAny(this string str, params string[] strings) {
	    foreach (string tmpString in strings) {
			if (str.Contains(tmpString)) return true;
		}
    	return false;
	}

	//Splits a string into individual string characters, and outputs an array
	public static string[] ToStringArray(this string str) {
        if (str.IsNullOrEmpty()) return null;
        char[] charArray = str.ToCharArray();
        string[] stringArray = new string[charArray.Length];
        for(int i = 0; i < charArray.Length; i++) {
        	stringArray[i] = charArray[i].ToString();
        }
        return stringArray;
    }
    
    //Returns a trunciated string longer than length
    public static string Truncate(this string source, int length){
		if (source.Length > length) {
			source = source.Substring(0, length);
		}
		return source;
	}

	public static string AddSpacesToSentence(this string text, bool preserveAcronyms) {
		if (text.IsNullOrWhiteSpace())
			return string.Empty;
		StringBuilder newText = new StringBuilder(text.Length * 2);
		newText.Append(text[0]);
		for (int i = 1; i < text.Length; i++) {
			if (char.IsUpper(text[i]))
				if ((text[i - 1] != ' ' && !char.IsUpper(text[i - 1])) ||
				    (preserveAcronyms && char.IsUpper(text[i - 1]) && 
				 i < text.Length - 1 && !char.IsUpper(text[i + 1])))
					newText.Append(' ');
			newText.Append(text[i]);
		}
		return newText.ToString();
	}

	/// <summary>
    /// Get string value between [first] a and [last] b.
    /// </summary>
    public static string Between(this string value, string a, string b) {
		int posA = value.IndexOf(a);
		int posB = value.LastIndexOf(b);
		if (posA == -1) {
		    return "";
		}
		if (posB == -1) {
		    return "";
		}
		int adjustedPosA = posA + a.Length;
		if (adjustedPosA >= posB) {
		    return "";
		}
		return value.Substring(adjustedPosA, posB - adjustedPosA);
    }

    /// <summary>
    /// Get string value after [first] a.
    /// </summary>
    public static string Before(this string value, string a) {
		int posA = value.IndexOf(a);
		if (posA == -1) {
		    return "";
		}
		return value.Substring(0, posA);
    }

    /// <summary>
    /// Get string value after [last] a.
    /// </summary>
    public static string After(this string value, string a) {
		int posA = value.LastIndexOf(a);
		if (posA == -1) {
		    return "";
		}
		int adjustedPosA = posA + a.Length;
		if (adjustedPosA >= value.Length) {
		    return "";
		}
		return value.Substring(adjustedPosA);
    }
}
