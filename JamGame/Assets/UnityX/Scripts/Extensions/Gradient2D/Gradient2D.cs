﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public sealed class Gradient2D {
	public Gradient2DColorKey[] colorKeys;
	
	public Gradient2D () {
		colorKeys = new Gradient2DColorKey[] {
			new Gradient2DColorKey(new Color(1,0,1,0), new Vector2(0.05f,0.4f)),
			new Gradient2DColorKey(new Color(0,0,0,0), new Vector2(0.9f,0.75f)),
			new Gradient2DColorKey(new Color(0,1,1,0), new Vector2(0.4f,0.1f)),
			new Gradient2DColorKey(new Color(1,1,0,0), new Vector2(0.6f,0.9f))
		};
	}
	
	public Gradient2D (Gradient2DColorKey[] _colorKeys) {
		colorKeys = _colorKeys;
	}
	
	public void SetKeys (Gradient2DColorKey[] _colorKeys) {
		colorKeys = _colorKeys;
	}
	
	public Color Evaluate (Vector2 position) {
		return Color.white;
	}
	
	public Color[] ToColorArray (int width, int height) {
		int numPixels = width * height;
		Color[] pixels = new Color[numPixels];
		float widthReciprocal = 1f/MathX.Clamp1Infinity(width-1);
		float heightReciprocal = 1f/MathX.Clamp1Infinity(height-1);
//		float length = Vector2.Distance(startPosition, endPosition);
		
		for(int y = 0; y < height; y++){
			for(int x = 0; x < width; x++){
				Color c = Color.gray;
				float[] strengths = new float[colorKeys.Length];
				for(int i = 0; i < colorKeys.Length; i++){
					//Debug.Log (new Vector2(x * widthReciprocal, y * heightReciprocal));
					strengths[i] = 1f-(Vector2.Distance(new Vector2(x * widthReciprocal, y * heightReciprocal), colorKeys[i].position));
					
				}
				strengths = MathX.Normalize(strengths);
				
				for(int i = 0; i < colorKeys.Length; i++){
					c = ColorX.Blend(c, colorKeys[i].color, strengths[i], ColorBlendMode.Normal);
				}
				
				pixels[y * width + x] = c;
			}
		}
		
		return pixels;
	}
}