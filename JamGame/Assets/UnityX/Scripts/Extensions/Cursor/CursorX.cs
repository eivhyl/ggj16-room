﻿using UnityEngine;
using System.Collections;

public static class CursorX {
	// This should be integrated!
	// Screen.showCursor = false;
	
	
	/// <summary>
	/// Whether the custom cursor is shown.
	/// </summary>
	private static bool _showCursor = true;
	public static bool showCursor {
		get {
			return CursorX._showCursor;
		} set {
			CursorX._showCursor = value;
			if(!CursorX._showCursor) {
				CursorX.HideCursor();
			} else {
				CursorX.ShowCursor();
			}
		}
	}

	/// <summary>
	/// The current cursor texture.
	/// </summary>
	public static Texture2D currentCursorTexture;
	
	/// <summary>
	/// The current cursor offset.
	/// </summary>
	public static Vector2 currentCursorOffset;
	
	/// <summary>
	/// Sets the cursor view.
	/// </summary>
	/// <param name="texture">Texture.</param>
	/// <param name="pixelOffset">Pixel offset.</param>
	public static void SetCursor (Texture2D texture, Vector2 pixelOffset) {
		CursorX.currentCursorTexture = texture;
		CursorX.currentCursorOffset = pixelOffset;
		Cursor.SetCursor(texture, pixelOffset, CursorMode.ForceSoftware);
	}
	
	/// <summary>
	/// Shows the custom cursor.
	/// </summary>
	public static void ShowCursor () {
		if(CursorX.currentCursorTexture != null)
		Cursor.SetCursor(CursorX.currentCursorTexture, CursorX.currentCursorOffset, CursorMode.ForceSoftware);
	}
	
	/// <summary>
	/// Hides the custom cursor.
	/// </summary>
	public static void HideCursor () {
		Cursor.SetCursor(null, Vector2.zero, CursorMode.ForceSoftware);
	}
	
	/// <summary>
	/// Sets the mouse position as a global shader property. 
	/// Consider changing this to screen position rather than a viewport one.
	/// </summary>
	public static void SetMousePositionShaderProperty () {
		Shader.SetGlobalVector("_MousePosition", new Vector2((float)Input.mousePosition.x/Screen.width, (float)Input.mousePosition.y/Screen.height));
	}
}
