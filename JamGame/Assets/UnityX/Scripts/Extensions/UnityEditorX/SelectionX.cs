﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif

public static class SelectionX {
	#if UNITY_EDITOR
	
	/// <summary>
	/// Filters the selection. Generic implementation.
	/// </summary>
	/// <returns>The selection.</returns>
	/// <param name="selectionMode">Selection mode.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T[] GetFiltered<T> (SelectionMode selectionMode) where T : Object {
		Object[] objs = Selection.GetFiltered(typeof(T), selectionMode);
		T[] castObjs = new T[objs.Length];
		for(int i = 0; i < objs.Length; i++)
			castObjs[i] = (T)objs[i];
		return castObjs;
	}
	
	#endif
}