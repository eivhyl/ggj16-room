﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public static class SceneManagerX {

	public static string[] GetSceneNamesInProject (string path = "", bool includeExtension = false) {
		string[] files = GetScenePathsInProject(path, includeExtension);
		for(int i = 0; i < files.Length; i++) {
			files[i] = files[i].Split('/').Last();
		}
		return files;
	}

	public static string[] GetScenePathsInProject (string path = "", bool includeExtension = false) {
		string[] files = System.IO.Directory.GetFiles(System.IO.Path.Combine(Application.dataPath, path), "*.unity", System.IO.SearchOption.AllDirectories);
		if(files == null) files = new string[0];
		if(includeExtension) {
			for(int i = 0; i < files.Length; i++) {
				files[i] = Path.GetFileNameWithoutExtension(files[i]);
			}
		}
		return files;
	}
}
