﻿using UnityEngine;
using System.Collections;
using System;

public class CustomRaycastResult: IComparable<CustomRaycastResult> {
	public Transform transform;
	public Collider collider;
	public float distance;
	public Vector3 point;
	public Vector2 textureCoord;
	public CustomRaycastResult(RaycastHit hit) {
		transform = hit.transform;
		collider = hit.collider;
		distance = hit.distance;
		point = hit.point;
		textureCoord = hit.textureCoord;
	}

	public int CompareTo(CustomRaycastResult other) {
		return distance.CompareTo(other.distance);
	}
}