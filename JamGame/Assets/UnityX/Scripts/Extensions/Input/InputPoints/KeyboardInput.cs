﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class KeyboardInput {
	public bool anyKeyDown = false;

	public KeyboardInput () {
	
	}
	
	public void UpdateState () {
		anyKeyDown = Input.anyKeyDown;
	}

	public void ResetInput () {
		anyKeyDown = false;
	}

	public void End () {

	}
	
	public Vector2 GetCardinalDirectionFromArrowKeys (bool alsoUseWASD = true) {
		if(Input.GetKey(KeyCode.UpArrow) || (alsoUseWASD && Input.GetKey(KeyCode.W))) {
			return Vector2.up;
		} else if(Input.GetKey(KeyCode.DownArrow) || (alsoUseWASD && Input.GetKey(KeyCode.S))) {
			return Vector2.down;
		} else if(Input.GetKey(KeyCode.LeftArrow) || (alsoUseWASD && Input.GetKey(KeyCode.A))) {
			return Vector2.left;
		} else if(Input.GetKey(KeyCode.RightArrow) || (alsoUseWASD && Input.GetKey(KeyCode.D))) {
			return Vector2.right;
		}
		return Vector2.zero;
	}
	
	public Vector2 GetCombinedDirectionFromArrowKeys (bool alsoUseWASD = true) {
		Vector2 direction = Vector2.zero;
		if(Input.GetKey(KeyCode.UpArrow) || (alsoUseWASD && Input.GetKey(KeyCode.W))) {
			direction += Vector2.up;
		} else if(Input.GetKey(KeyCode.DownArrow) || (alsoUseWASD && Input.GetKey(KeyCode.S))) {
			direction += Vector2.down;
		}
		if(Input.GetKey(KeyCode.LeftArrow) || (alsoUseWASD && Input.GetKey(KeyCode.A))) {
			direction += Vector2.left;
		} else if(Input.GetKey(KeyCode.RightArrow) || (alsoUseWASD && Input.GetKey(KeyCode.D))) {
			direction += Vector2.right;
		}
		return direction.normalized;
	}
}
