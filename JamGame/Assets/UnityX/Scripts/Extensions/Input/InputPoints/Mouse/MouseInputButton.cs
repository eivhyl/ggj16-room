﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MouseInputButton {
	public int buttonIndex = 0;
	public bool down = false;
	public bool held = false;
	public bool up = false;
	public float startTime;
	public float activeTime;

	public delegate void OnMouseButtonDownEvent();
	public event OnMouseButtonDownEvent OnMouseButtonDown;
	public delegate void OnMouseButtonUpEvent(float activeTime);
	public event OnMouseButtonUpEvent OnMouseButtonUp;

	public MouseInputButton(int _buttonIndex){
		buttonIndex = _buttonIndex;
	}

	public void UpdateLifeTime(float _deltaTime){
		if(held){
			activeTime += _deltaTime;
		}
	}

	public void UpdateState(){
		down = false;
		up = false;
		if(Input.GetMouseButtonDown(buttonIndex)){
			ButtonDown();
		}

		if(Input.GetMouseButtonUp(buttonIndex)){
			ButtonUp();
		}
	}

	public void ResetInput(){
		ButtonUp(false);
	}

	private void ButtonDown(bool sendEvent = true){
		down = true;
		held = true;
		startTime = Time.time;

		if(sendEvent && OnMouseButtonDown != null){
			OnMouseButtonDown();
		}
	}

	private void ButtonUp(bool sendEvent = true){
		held = false;
		up = true;
		if(sendEvent && OnMouseButtonUp != null){
			OnMouseButtonUp(activeTime);
		}
		activeTime = startTime = 0;
	}
	
	public override string ToString () {
		return string.Format ("[MouseInputButton] Button Index {0} Down {1} Up {2}", buttonIndex, held, up);
	}
}