using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum InputType {
	Mouse,
	Touch,
	Other
}

public class InputX : MonoSingleton<InputX> {

	[SerializeField]
	private bool _acceptInput = true;
	public bool acceptInput {
		get {
			return _acceptInput;
		} set {
			if(_acceptInput == value) return;
			_acceptInput = value;
			if(acceptInput){
				ResetInput();
			} else {
				ResetInput();
			}
		}
	}

	public InputType inputType = InputType.Mouse;
	
	public KeyboardInput keyboardInput;
	public MouseInput mouseInput;
	//public List<InputPoint> inputPoints = new List<InputPoint>();
	public List<Finger> fingers = new List<Finger>();

	public List<Gesture> gestures = new List<Gesture>();

	//public delegate void OnDragEvent(InputPoint inputPoint);
	//public event OnDragEvent OnDrag;

	public delegate void OnTouchStartEvent(Finger finger);
	public event OnTouchStartEvent OnTouchStart;
	public delegate void OnTouchEndEvent(Finger finger);
	public event OnTouchEndEvent OnTouchEnd;
	public delegate void OnTapEvent(InputPoint inputPoint);
	public event OnTapEvent OnTap;

	public delegate void OnPinchStartEvent(Finger finger1, Finger finger2);
	public event OnPinchStartEvent OnPinchStart;
	public delegate void OnPinchEndEvent();
	public event OnPinchEndEvent OnPinchEnd;

	public delegate void OnMouseStartEvent(MouseInput mouse);
	public event OnMouseStartEvent OnMouseStart;
	public delegate void OnMouseEndEvent(MouseInput mouse);
	public event OnMouseEndEvent OnMouseEnd;

	public delegate void OnMouseLeftDownEvent(MouseInput inputPoint);
	public event OnMouseLeftDownEvent OnMouseLeftDown;
	public delegate void OnMouseLeftUpEvent(MouseInput inputPoint, float activeTime);
	public event OnMouseLeftUpEvent OnMouseLeftUp;
	public delegate void OnMouseLeftClickEvent(MouseInput inputPoint);
	public event OnMouseLeftClickEvent OnMouseLeftClick;

	public delegate void OnMouseRightDownEvent(MouseInput inputPoint);
	public event OnMouseRightDownEvent OnMouseRightDown;
	public delegate void OnMouseRightUpEvent(MouseInput inputPoint, float activeTime);
	public event OnMouseRightUpEvent OnMouseRightUp;
	public delegate void OnMouseRightClickEvent(MouseInput inputPoint);
	public event OnMouseRightClickEvent OnMouseRightClick;

	public delegate void OnMouseMiddleDownEvent(MouseInput inputPoint);
	public event OnMouseMiddleDownEvent OnMouseMiddleDown;
	public delegate void OnMouseMiddleUpEvent(MouseInput inputPoint, float activeTime);
	public event OnMouseMiddleUpEvent OnMouseMiddleUp;
	public delegate void OnMouseMiddleClickEvent(MouseInput inputPoint);
	public event OnMouseMiddleClickEvent OnMouseMiddleClick;
	
	public delegate void OnMouseScrollEvent(MouseInput inputPoint);
	public event OnMouseScrollEvent OnMouseScroll;
	
	
	
	public void Begin () {
		if(SystemInfo.deviceType == DeviceType.Handheld){
			inputType = InputType.Touch;
		} else {
			inputType = InputType.Mouse;
			InitMouse();
			InitKeyboard();
		}
	}

	public void Loop () {
		if(acceptInput){
			UpdateInput();
		}
	}

	//Resets all input values.
	public void ResetInput(){
		if(fingers != null) {
			for (int i = 0; i < fingers.Count; i++) {
				fingers[i].ResetInput();
			}
		}

		if(mouseInput != null) {
			mouseInput.ResetInput();
		}

		if(keyboardInput != null) {
			keyboardInput.ResetInput();
		}
	}

	private void UpdateInput(){
		if(inputType == InputType.Touch){
			UpdateTouch();
		} else if(inputType == InputType.Mouse){
			UpdateMouse();
			UpdateKeyboard();
		}
	}

	private void UpdateTouch(){
		Finger tmpFinger;
		int? fingerIndex;

		for (int i = Input.touches.Length-1; i >= 0; i--) {
        	fingerIndex = GetFingerIndexByID(Input.touches[i].fingerId);        	

        	tmpFinger = GetFingerByID(Input.touches[i].fingerId);
        	if(tmpFinger != null){
        		tmpFinger.fingerId = Input.touches[i].fingerId;
        		
				if(Input.touches[i].phase == TouchPhase.Ended || Input.touches[i].phase == TouchPhase.Canceled){
					if(fingerIndex != null){
						TouchEnd((int)fingerIndex);
						continue;
					}
				}
        	} else {
				if(Input.touches[i].phase == TouchPhase.Began){
					TouchStart(Input.touches[i]);
				} else {
					Debug.Log ("ERROR?" +Input.touches[i].phase);
				}
        	}
        }

		for (int i = 0; i < fingers.Count; i++) {
			fingers[i].fingerArrayIndex = i;
			Touch _touch = (Touch)GetTouchByID(fingers[i].fingerId);
		
			fingers[i].UpdateLifeTime(Time.deltaTime);
			fingers[i].UpdatePosition(_touch.position);
			fingers[i].UpdateState();
		}

		CheckForPinchStart();
		if(pinching){
			UpdatePinch();
		}
	}

	private void UpdateMouse(){
		if(mouseInput != null) {
			mouseInput.UpdateLifeTime(Time.deltaTime);
			mouseInput.UpdatePosition(Input.mousePosition);
			mouseInput.UpdateState();
		}		
	}

	private void UpdateKeyboard(){
		if(keyboardInput != null) {
			//mouseInput.UpdateLifeTime(Time.deltaTime);
			//mouseInput.UpdatePosition(Input.mousePosition);
			keyboardInput.UpdateState();
		}
	}

	private void InitMouse(){
		mouseInput = new MouseInput(Input.mousePosition);

		//mouseInput.OnStart += InputPointStart;
		mouseInput.OnTap += Tap;
		mouseInput.OnMouseLeftClick += MouseLeftClick;
		mouseInput.OnMouseLeftDown += MouseLeftDown;
		mouseInput.OnMouseLeftUp += MouseLeftUp;
		mouseInput.OnMouseRightClick += MouseRightClick;
		mouseInput.OnMouseRightDown += MouseRightDown;
		mouseInput.OnMouseRightUp += MouseRightUp;
		mouseInput.OnMouseMiddleClick += MouseMiddleClick;
		mouseInput.OnMouseMiddleDown += MouseMiddleDown;
		mouseInput.OnMouseMiddleUp += MouseMiddleUp;
		mouseInput.OnMouseScroll += MouseScroll;
		//mouseInput.OnEnd += InputPointEnd;

		if(OnMouseStart != null){
			OnMouseStart(mouseInput);
		}
	}

	private void DestroyMouse(){
		mouseInput.End();

		//mouseInput.OnStart -= InputPointStart;
		mouseInput.OnTap -= Tap;
		mouseInput.OnMouseLeftClick -= MouseLeftClick;
		mouseInput.OnMouseLeftDown -= MouseLeftDown;
		mouseInput.OnMouseLeftUp -= MouseLeftUp;
		mouseInput.OnMouseRightClick -= MouseRightClick;
		mouseInput.OnMouseRightDown -= MouseRightDown;
		mouseInput.OnMouseRightUp -= MouseRightUp;
		mouseInput.OnMouseMiddleClick -= MouseMiddleClick;
		mouseInput.OnMouseMiddleDown -= MouseMiddleDown;
		mouseInput.OnMouseMiddleUp -= MouseMiddleUp;
		mouseInput.OnMouseScroll -= MouseScroll;
		//mouseInput.OnEnd -= InputPointEnd;

		if(OnMouseEnd != null){
			OnMouseEnd(mouseInput);
		}

		mouseInput = null;
	}

	private void InitKeyboard(){
		keyboardInput = new KeyboardInput();

	}

	private void DestroyKeyboard(){
		keyboardInput.End();
		keyboardInput = null;
	}

	private void TouchStart(Touch _touch){
		fingers.Add(new Finger(_touch));
		if(OnTouchStart != null){
			OnTouchStart(fingers[fingers.Count-1]);
		}
	}
	private void TouchEnd(int _index){
		fingers[_index].End();

		if(OnTouchEnd != null){
			OnTouchEnd(fingers[_index]);
		}

        if(fingers.Count == 0){
			
        }

		fingers.RemoveAt(_index);

		CheckForPinchEnd();
	}


	public bool pinching = false;
	public float? startPinchDistance = null;
	public float? currentPinchDistance = null;
	public float? lastPinchDistance = null;
	public float deltaPinchDistance = 0;

	public float normalizedDeltaPinchDistance = 0;

	public Finger pinchFinger1;
	public Finger pinchFinger2;

	private void CheckForPinchStart(){
		if(pinching)return;
		List<Finger> pinchFingers = new List<Finger>();
		for(int i = 0; i < fingers.Count; i++){
			if(fingers[i].state != InputPointState.Started){
				pinchFingers.Add(fingers[i]);
				if(pinchFingers.Count == 2){
					PinchStart(fingers[0], fingers[1]);
					break;
				}
			}
		}
	}
    private float GetPinchDistance(){
    	return Vector2.Distance(pinchFinger1.position, pinchFinger2.position);
    }
    private void CheckForPinchEnd(){

    	if(fingers.Count < 2){
			PinchEnd();
        } else {
        	if(pinchFinger1 != null){
	    		for(int i = 0; i < fingers.Count; i++){
					if(fingers[i].state != InputPointState.Started && fingers[i].state != InputPointState.Pinch2){
						fingers[i].state = InputPointState.Pinch1;
						pinchFinger1 = fingers[i];
					}
				}
	    	}
	    	if(pinchFinger2 != null){
	    		for(int i = 0; i < fingers.Count; i++){
					if(fingers[i].state != InputPointState.Started && fingers[i].state != InputPointState.Pinch1){
						fingers[i].state = InputPointState.Pinch2;
						pinchFinger2 = fingers[i];
					}
				}
	    	}
        }
    }
    private void UpdatePinch(){
		lastPinchDistance = currentPinchDistance;
		currentPinchDistance = GetPinchDistance();
		deltaPinchDistance = (float)currentPinchDistance - (float)lastPinchDistance;
		normalizedDeltaPinchDistance = deltaPinchDistance * ScreenX.diagonalReciprocal;
	}

    public Touch? GetTouchByID(int _id){
		for (int i = 0; i < Input.touches.Length; i++) {
			if(Input.touches[i].fingerId == _id){
				return Input.touches[i];
			}
		}
		return null;
	}

	public int? GetTouchIndexByID(int _id){
		for (int i = 0; i < Input.touches.Length; i++) {
			if(Input.touches[i].fingerId == _id){
				return i;
			}
		}
		return null;
	}

	public Finger GetFingerByID(int _id){
		for (int i = 0; i < fingers.Count; i++) {
			if(fingers[i].fingerId == _id){
				return fingers[i];
			}
		}
		return null;
	}

	public int? GetFingerIndexByID(int _id){
		for (int i = 0; i < fingers.Count; i++) {
			if(fingers[i].fingerId == _id){
				return i;
			}
		}
		return null;
	}

	private void TouchStart (Finger inputPoint) {
		if(OnTouchStart != null){
			OnTouchStart(inputPoint);
		}
	}
	private void TouchEnd (Finger inputPoint) {
		if(OnTouchEnd != null){
			OnTouchEnd(inputPoint);
		}
	}

	private void Tap (InputPoint inputPoint) {
		if(OnTap != null){
			OnTap(inputPoint);
		}
	}

	private void PinchStart (Finger inputPoint1, Finger inputPoint2) {
		pinching = true;
    	pinchFinger1 = inputPoint1;
    	pinchFinger2 = inputPoint2;
    	inputPoint1.state = InputPointState.Pinch1;
    	inputPoint2.state = InputPointState.Pinch2;
    	if(OnPinchStart != null){
			OnPinchStart(inputPoint1, inputPoint2);
		}
		startPinchDistance = currentPinchDistance = GetPinchDistance();

		if(OnPinchStart != null){
			OnPinchStart(inputPoint1, inputPoint2);
		}
	}

	private void PinchEnd () {
		pinching = false;
    	if(pinchFinger1 != null){
    		pinchFinger1.state = InputPointState.Stationary;
    	}
		if(pinchFinger2 != null){
			pinchFinger2.state = InputPointState.Stationary;
		}
		pinchFinger1 = null;
		pinchFinger2 = null;
    	if(OnPinchEnd != null){
			OnPinchEnd();
		}
	}

	private void MouseLeftDown (MouseInput inputPoint) {
		if(OnMouseLeftDown != null){
			OnMouseLeftDown(inputPoint);
		}
	}
	private void MouseLeftUp (MouseInput inputPoint, float activeTime) {
		if(OnMouseLeftUp != null){
			OnMouseLeftUp(inputPoint, activeTime);
		}
	}
	private void MouseLeftClick (MouseInput inputPoint) {
		if(OnMouseLeftClick != null){
			OnMouseLeftClick(inputPoint);
		}
		Tap(inputPoint);
	}
	
	private void MouseRightDown (MouseInput inputPoint) {
		if(OnMouseRightDown != null){
			OnMouseRightDown(inputPoint);
		}
	}
	private void MouseRightUp (MouseInput inputPoint, float activeTime) {
		if(OnMouseRightUp != null){
			OnMouseRightUp(inputPoint, activeTime);
		}
	}
	private void MouseRightClick (MouseInput inputPoint) {
		if(OnMouseRightClick != null){
			OnMouseRightClick(inputPoint);
		}
	}
	
	private void MouseMiddleDown (MouseInput inputPoint) {
		if(OnMouseMiddleDown != null){
			OnMouseMiddleDown(inputPoint);
		}
	}
	private void MouseMiddleUp (MouseInput inputPoint, float activeTime) {
		if(OnMouseMiddleUp != null){
			OnMouseMiddleUp(inputPoint, activeTime);
		}
	}
	private void MouseMiddleClick (MouseInput inputPoint) {
		if(OnMouseMiddleClick != null){
			OnMouseMiddleClick(inputPoint);
		}
	}

	private void MouseScroll (MouseInput inputPoint) {
		if(OnMouseScroll != null){
			OnMouseScroll(inputPoint);
		}
	}

	public static int? GetNumberKey () {
		foreach (char c in Input.inputString) {
			int num;
			if(int.TryParse(c.ToString(), out num)){
				return num;
			}
		}
		return null;
	}

	public static bool TryGetNumberKey (ref int numberKey) {
		int? _numberKey = GetNumberKey();
		if(_numberKey != null) {
			numberKey = (int)_numberKey;
			return true;
		}
		return false;
	}

	public static KeyCode GetNumberKeyCode () {
		int numberKey = -1;
		if(TryGetNumberKey(ref numberKey)) {
			if(numberKey == 0) return KeyCode.Alpha0;
			if(numberKey == 1) return KeyCode.Alpha1;
			if(numberKey == 2) return KeyCode.Alpha2;
			if(numberKey == 3) return KeyCode.Alpha3;
			if(numberKey == 4) return KeyCode.Alpha4;
			if(numberKey == 5) return KeyCode.Alpha5;
			if(numberKey == 6) return KeyCode.Alpha6;
			if(numberKey == 7) return KeyCode.Alpha7;
			if(numberKey == 8) return KeyCode.Alpha8;
			if(numberKey == 9) return KeyCode.Alpha9;
		}
		return KeyCode.None;
	}
	
	private void OnLevelWasLoaded () {
		ResetInput();
	}
}