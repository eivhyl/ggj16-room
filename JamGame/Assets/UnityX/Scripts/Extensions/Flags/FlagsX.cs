﻿using UnityEngine;
using System.Collections;

public static class FlagsX {
	
	/// <summary>
	/// Converts index value (0,1,2,3...) to flag value (1,2,4,8..).
	/// </summary>
	/// <returns>The flag value.</returns>
	/// <param name="flags">Flags.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static int ToFlagValue(int indexValue) {
		return (int)Mathf.Pow(2, indexValue);
	}
	
	public static int ToFlagValue<T>(T flags) where T : struct {
		return ToFlagValue((int)(object)flags);
	}
	
	/// <summary>
	/// Determines if any flags are set.
	/// </summary>
	/// <returns><c>true</c> if is set the specified flags; otherwise, <c>false</c>.</returns>
	/// <param name="flags">Flags.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static bool AnySet<T>(T flags) where T : struct {
		int flagsValue = (int)(object)flags;
		return flagsValue != 0;
	}
	
	public static bool IsSet(int flagsValue, int flagValue) {
		return (flagsValue & flagValue) != 0;
	}
	
	public static bool IsSet<T>(T flags, T flag) where T : struct {
		int flagsValue = (int)(object)flags;
		int flagValue = ToFlagValue(flag);
		return IsSet(flagsValue, flagValue);
	}
	
	public static bool Equals<T>(T flags, T flag) where T : struct {
		int flagsValue = (int)(object)flags;
		int flagValue = ToFlagValue(flag);
		return flagsValue == flagValue;
	}
	
	public static int Set(int flagsValue, int flagValue) {
		return flagsValue | flagValue;
	}
	
	public static T Set<T>(T flags, T flag) where T : struct {
		int flagsValue = ToFlagValue(flags);
		int flagValue = ToFlagValue(flag);
		return (T)(object) Set(flagsValue, flagValue);
	}
	
	/// <summary>
	/// Creates a new Flag including the values provided
	/// </summary>
	/// <param name="flags">Flags.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T Create<T>(params T[] flags) where T : struct {
		int[] flagValues = new int[flags.Length];
		for(int i = 0; i < flagValues.Length; i++) {
			flagValues[i] = ToFlagValue(flags[i]);
		}
		return (T)(object) Create(flagValues);
	}
	
	private static int Create(params int[] flags) {
		int flagsValue = 0;
		foreach(int flag in flags) {
			if(!IsSet(flagsValue, flag)) {
				flagsValue = Set (flagsValue, flag);
			}
		}
		return flagsValue;
	}
	
	/// <summary>
	/// Flag, containing all the values
	/// </summary>
	/// <returns>The everything.</returns>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T CreateEverything<T>() where T : struct {
		return (T)(object)-1;
	}
	
	public static int Unset(int flagsValue, int flagValue) {
		return flagsValue & (~flagValue);
	}
	
	public static T Unset<T>(T flags, T flag) where T : struct {
		int flagsValue = ToFlagValue(flags);
		int flagValue = ToFlagValue(flag);
		return (T)(object) Unset(flagsValue, flagValue);
	}
	
	/// <summary>
	/// Using Unity's EnumMaskPopup can lead to values less than -1. This is probably a bug, and while the value is technically valid, it makes working with it a pain.
	/// This cleans the enum mask popup value, returning the value within the expected range.
	/// </summary>
	/// <returns>The enum mask popup.</returns>
	/// <param name="flags">Flags.</param>
	/// <param name="flag">Flag.</param>
	/// <typeparam name="T">The 1st type parameter.</typeparam>
	public static T CleanEnumMaskPopup<T>(T flag) where T : struct {
		return (T)(object)CleanEnumMaskPopupInt<T>((int)((object)flag));
	}
	
	public static int CleanEnumMaskPopupInt<T>(int flagIntVal) where T : struct {
		if(flagIntVal < -1) {
			flagIntVal += FlagsX.ToFlagValue(EnumX.Length<T>());
		}
		return flagIntVal;
	}
}