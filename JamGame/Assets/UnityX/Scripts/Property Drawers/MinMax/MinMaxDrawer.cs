﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer (typeof (MinMaxAttribute))]
public class MinMaxDrawer : PropertyDrawer {

    MinMaxAttribute minMaxAttribute { 
        get { 
            return ((MinMaxAttribute)attribute); 
        } 
    }

    public override void OnGUI (Rect rect, SerializedProperty property, GUIContent label) {
        EditorGUI.BeginChangeCheck();
        if(property.propertyType == SerializedPropertyType.Vector2){
            Vector2 v2 = property.vector2Value;
            
            EditorGUI.MinMaxSlider(new GUIContent(label), new Rect(rect.x, rect.y, rect.width, 12), ref v2.x, ref v2.y, minMaxAttribute.min,  minMaxAttribute.max);
			if(EditorGUIUtility.wideMode) {
				rect.y+=EditorGUIUtility.singleLineHeight;
			}
            v2 = EditorGUI.Vector2Field (new Rect (rect.x, rect.y, rect.width-10, rect.height+8), new GUIContent(" "), v2);
            
            if (EditorGUI.EndChangeCheck()) {
				if(minMaxAttribute.step > 0) {
					v2.x = Mathf.Round(v2.x/minMaxAttribute.step)*minMaxAttribute.step;
					v2.y = Mathf.Round(v2.y/minMaxAttribute.step)*minMaxAttribute.step;
                }
                property.vector2Value = new Vector2 (Mathf.Clamp(v2.x, minMaxAttribute.min, v2.y), Mathf.Clamp(v2.y, v2.x, minMaxAttribute.max));
            }
		} else {
			EditorGUI.LabelField (new Rect (rect.x, rect.y+20, rect.width-10, rect.height+8), label, "Use only with Vector2");
        }
    }


    /*public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
        //SerializedProperty minMax = property.FindPropertyRelative ("minMax");
        //EditorGUI.MinMaxSlider(new Rect(0f, 0f, 100f, 20f), ref minMaxAttribute.min, ref minMaxAttribute.max, 10,10);

        //EditorGUI.PropertyField( position, minMax, GUIContent.none);

        if(property.propertyType == SerializedPropertyType.Float)

        EditorGUI.Slider(position, property, attribute.property, attribute.property, label);
    }*/

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        return base.GetPropertyHeight(property, label) + GetHeight(property);
    }

    private static float GetHeight(SerializedProperty property) {
		if(EditorGUIUtility.wideMode) {
			return EditorGUIUtility.singleLineHeight * 1 + EditorGUIUtility.standardVerticalSpacing;
		} else {
			return EditorGUIUtility.singleLineHeight * 1;
		}
    }

    private static SerializedProperty GetProperty(SerializedProperty property, string name)
    {
        return property.FindPropertyRelative(name);
    }
}
#endif