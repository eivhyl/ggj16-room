﻿using UnityEngine;

public class EnumButtonsExample : MonoBehaviour
{

    public enum Example {
        High,
        Medium,
        Low,
        Off
    }

    [EnumButtons]
    public Example test = Example.Low;
}
