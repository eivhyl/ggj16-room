﻿using System;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
#endif 

[AttributeUsage(AttributeTargets.Enum | AttributeTargets.Field)]
public class EnumButtonsAttribute : PropertyAttribute {
	public EnumButtonsAttribute () {}
}