// https://github.com/anchan828/property-drawer-collection
// Copyright (C) 2014 Keigo Ando
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

#if UNITY_EDITOR
[CustomPropertyDrawer(typeof(ParticleSystemInfoAttribute))]
public class ParticleSystemInfoDrawer : PropertyDrawer {
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        if (!IsSupported(property)) {
            return;
        }

		position.height = EditorGUIUtility.singleLineHeight;
		ParticleSystem ps = ((ParticleSystem)property.objectReferenceValue);
		if(ps == null) {
			EditorGUI.BeginDisabledGroup(particleSystemInfoAttribute.disableField);
			property.objectReferenceValue = EditorGUI.ObjectField(position, property.objectReferenceValue, typeof(ParticleSystem), true);
			EditorGUI.EndDisabledGroup();
		} else {
			EditorGUI.BeginDisabledGroup(particleSystemInfoAttribute.disableField);
			property.objectReferenceValue = EditorGUI.ObjectField(position, property.objectReferenceValue, typeof(ParticleSystem), true);
			EditorGUI.EndDisabledGroup();
			position.y += 20;
			position.height = EditorGUIUtility.singleLineHeight + 8;
			EditorGUI.HelpBox(position, "Particle Count: "+ps.particleCount+"/"+ps.maxParticles, MessageType.None);
		}
    }

    bool IsSupported(SerializedProperty property) {
        return property.propertyType == SerializedPropertyType.ObjectReference;
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
        if (IsSupported(property)) {
			if(((ParticleSystem)property.objectReferenceValue) != null) {
				return base.GetPropertyHeight(property, label) + 30;
			} else {
				return base.GetPropertyHeight(property, label);
			}
        } else {
			return base.GetPropertyHeight(property, label) + 30;
        }
    }

	ParticleSystemInfoAttribute particleSystemInfoAttribute {
        get {
			return (ParticleSystemInfoAttribute) attribute;
        }
    }

}
#endif