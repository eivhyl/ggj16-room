#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer (typeof (IntRangeAttribute))]
public class IntRangeDrawer : PropertyDrawer {
	IntRangeAttribute intRangeAttribute { 
		get { 
			return ((IntRangeAttribute)attribute); 
		} 
	}

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		property.floatValue = Mathf.RoundToInt(property.floatValue);
        EditorGUI.Slider(position, property, intRangeAttribute.min, intRangeAttribute.max);
    }
}
#endif