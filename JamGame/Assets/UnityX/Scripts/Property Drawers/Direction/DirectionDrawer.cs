﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using System.Text.RegularExpressions;

[CustomPropertyDrawer (typeof (DirectionAttribute))]
public class DirectionDrawer : PropertyDrawer {
	
	DirectionAttribute directionAttribute { get { return ((DirectionAttribute)attribute); } }
	
    public override void OnGUI (Rect position, SerializedProperty prop, GUIContent label) {
		if(prop.type == "Vector2") {
			DrawVector2Field (position, prop, label);
		} else if(prop.type == "Vector3") {
			DrawVector3Field (position, prop, label);
		} else if(prop.type == "Vector4") {
			DrawVector4Field (position, prop, label);
		}
    }

	private void DrawVector2Field (Rect position, SerializedProperty prop, GUIContent label) {
        EditorGUI.BeginChangeCheck ();
		Vector2 val = EditorGUI.Vector2Field (position, label, prop.vector2Value);
		if(val.sqrMagnitude == 0) val = new Vector2(0,1);
		else val.Normalize();
        if (EditorGUI.EndChangeCheck ())
			prop.vector2Value = val;
    }
    
	private void DrawVector3Field (Rect position, SerializedProperty prop, GUIContent label) {
		EditorGUI.BeginChangeCheck ();
//		Vector3 val = EditorGUI.Vector3Field (position, label, prop.vector3Value);
//		if(val.sqrMagnitude == 0) val = new Vector3(0,0,1);
//		else val.Normalize();
		
//		EditorGUI.Vector3Field (position, label, prop.vector3Value);
		float height = (prop.vector3Value.y);
		float m = prop.vector3Value.XZ ().magnitude;
		float degrees = Vector2X.Degrees(prop.vector3Value.XZ ().normalized);
		
		if(EditorGUIUtility.wideMode) {
			EditorGUI.PrefixLabel (new Rect(position.x, position.y, EditorGUIUtility.labelWidth, position.height), label);
		} else {
			EditorGUI.PrefixLabel (new Rect(position.x, position.y, position.width, EditorGUIUtility.singleLineHeight), label);
		}
		
		position.height = EditorGUIUtility.singleLineHeight;
		if(EditorGUIUtility.wideMode) {
			position.x += EditorGUIUtility.labelWidth;
			position.width -= EditorGUIUtility.labelWidth;
		} else {
			position.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
		}
		
		float savedLabelWidth = EditorGUIUtility.labelWidth;
		
		var textDimensions = GUI.skin.label.CalcSize(new GUIContent("Degrees"));
		EditorGUIUtility.labelWidth = textDimensions.x;
		
		position.width /= 2;
		degrees = EditorGUI.FloatField (position, "Degrees", degrees);
		
		textDimensions = GUI.skin.label.CalcSize(new GUIContent("Height"));
		EditorGUIUtility.labelWidth = textDimensions.x;
		
		position.x += position.width;
		height = Mathf.Clamp(EditorGUI.FloatField (position, "Height", height), -1, 1);
		
		Vector2 backAgainV2 = MathX.Degrees2Vector2(degrees);
		Vector3 backAgain = new Vector3(backAgainV2.x, 0, backAgainV2.y) * m;
		backAgain.y = height;
		backAgain.Normalize();
		
		if (EditorGUI.EndChangeCheck ())
			prop.vector3Value = backAgain;
			
		EditorGUIUtility.labelWidth = savedLabelWidth;
	}
	
	private void DrawVector4Field (Rect position, SerializedProperty prop, GUIContent label) {
		EditorGUI.BeginChangeCheck ();
		Vector4 val = EditorGUI.Vector4Field (position, label.text, prop.vector4Value);
		if(val.sqrMagnitude == 0) val = new Vector4(0,0,0,1);
		else val.Normalize();
		if (EditorGUI.EndChangeCheck ())
			prop.vector4Value = val;
	}
	
	
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
		
		return base.GetPropertyHeight(property, label) + (EditorGUIUtility.wideMode ? EditorGUIUtility.singleLineHeight : EditorGUIUtility.singleLineHeight * 2);
	}
}
#endif