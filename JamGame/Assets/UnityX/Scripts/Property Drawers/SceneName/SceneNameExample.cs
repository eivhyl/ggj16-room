using UnityEngine;

public class SceneNameExample : MonoBehaviour
{
	[SceneName]
	public string sceneName;
    [SceneName(SceneNameAttribute.SceneFindMethod.AllInProject)]
    public string sceneName2;
}