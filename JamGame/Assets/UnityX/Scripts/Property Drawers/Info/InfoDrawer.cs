﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(InfoAttribute))]
public class InfoDrawer : PropertyDrawer {
	private int helpBoxHeight = 38;
	
	public override float GetPropertyHeight (SerializedProperty property, GUIContent label) {
		return base.GetPropertyHeight (property, label) + helpBoxHeight;
	}
	
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		EditorGUI.HelpBox(position.CopyWithHeight(helpBoxHeight), ((InfoAttribute)attribute).info, MessageType.Info);
		EditorGUI.PropertyField(position.CopyWithY(position.y + helpBoxHeight).CopyWithHeight(position.height-helpBoxHeight), property, label);
    }
}

#endif