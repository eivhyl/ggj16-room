using UnityEngine;
using System.Collections;

public class SetCaseExample : MonoBehaviour
{
	[SetCase(SetCaseAttribute.CaseType.Upper)]
	public string myString;
}
