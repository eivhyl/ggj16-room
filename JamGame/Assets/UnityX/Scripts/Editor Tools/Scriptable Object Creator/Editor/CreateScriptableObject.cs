﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using System.IO;
using System.Reflection;
using System.Linq;
using Rotorz.ReorderableList;
using UnityEditorInternal;

public class CreateScriptableObject : EditorWindow {
	private int tab;
	public string objectName = "";
	public string typeName = "";
	public Type type;
	public int typeIndex = 0;
	string[] assemblyNames = new string [] {
		"Assembly-UnityScript",
		"Assembly-UnityScript-Editor",
		"Assembly-CSharp",
		"Assembly-CSharp-Editor",
		"Assembly-Boo",
		"Assembly-Boo-Editor"
	};
	
	private ReorderableList list;
	[MenuItem("Assets/Create/Scriptable Object")]
	static void Init () {
		EditorWindow.GetWindow(typeof(CreateScriptableObject));
	}
	
	void OnGUI () {
	
		tab = GUILayout.Toolbar (tab, new string[] {"Create Class", "Create Object"});
		switch (tab) {
			case 0: 
			DrawCreateClass();
			break;
			case 1: 
			DrawCreateObject();
			break;
		}
	}
	
	
	
	
	public string newClassName = "MyNewScriptableObject";
	private void DrawCreateClass () {
		newClassName = EditorGUILayout.TextField ("ScriptableObject Class name ", newClassName);
		
		
//		list.DoLayoutList();
//		ReorderableListGUI.Title("Wishlist");
//		ReorderableListGUI.ListField((SerializedProperty)s);
		
		if(GUILayout.Button("Create Scriptable Object")) {
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("using UnityEngine;");
			stringBuilder.AppendLine();
			stringBuilder.Append("using System.Collections;");
			stringBuilder.AppendLine();
			stringBuilder.Append("");
			stringBuilder.AppendLine();
			stringBuilder.Append("public class "+newClassName+" : ScriptableObject {");
			stringBuilder.AppendLine();
			stringBuilder.Append("\t");
			stringBuilder.AppendLine();
			stringBuilder.Append("}");
			ClassUtility.CreateRawCSharp(newClassName, stringBuilder.ToString());
//			ClassUtility.Create(newClassName);
		}
	}
	
	private void DrawCreateObject () {
		System.Type[] validFoundTypes = GetValidFoundTypes (assemblyNames);
		
		if(validFoundTypes.Length == 0) {
//			GUILayout.BeginArea(new Rect(10, 10, 100, 100));
//			EditorGUILayout.Box
				EditorGUILayout.LabelField(new GUIContent("No ScriptableObject classes found in project\nUse the 'Create Class' panel to create your class."),GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
			return;
//			GUILayout.EndArea();
		}
		
		string[] validFoundTypeStrings = new string[validFoundTypes.Length];
		
		for(int i = 0; i < validFoundTypes.Length; i++) {
			validFoundTypeStrings[i] = validFoundTypes[i].ToString();
		}
		
		typeIndex = EditorGUILayout.Popup(typeIndex, validFoundTypeStrings);
		//		typeName = EditorGUILayout.TextField ("Type Name ", typeName);
		
		if(!validFoundTypeStrings.ContainsIndex(typeIndex)) typeIndex = 0;
		else typeName = validFoundTypeStrings[typeIndex];
		
		Type newType = type;
		if(!typeName.IsNullOrWhiteSpace()) {
			newType = GetType(typeName, assemblyNames);
		} else {
			newType = null;
		}
		
		if(type != newType) {
			type = newType;
			if(type != null && type.IsSubclassOf(typeof(ScriptableObject))) {
				objectName = "New "+typeName;
			}
		}
		
		if(type == null) {
			EditorGUILayout.LabelField(new GUIContent("Type with name '"+typeName+"' does not exist"), GUILayout.Width(400), GUILayout.Height(20));
		} else {
			if(!type.IsSubclassOf(typeof(ScriptableObject))) {
				type = null;
				EditorGUILayout.LabelField(new GUIContent("Type with name '"+typeName+"' does not derive from ScriptableObject!"), GUILayout.Width(400), GUILayout.Height(20));
			}
		}
		
		if(type == null) GUI.enabled = false;
		objectName = EditorGUILayout.TextField ("Object Name ", objectName);
		if(GUILayout.Button("Create Scriptable Object")) {
			ScriptableObjectUtility.CreateAssetUsingReflection(type, objectName);
		}
		if(type == null) GUI.enabled = true;
	}
	
	System.Type[] GetValidFoundTypes (string[] assemblyNames) {
		List<System.Type> types	= new List<System.Type>();
		
		Assembly[] referencedAssemblies = System.AppDomain.CurrentDomain.GetAssemblies();
		for(int i = 0; i < referencedAssemblies.Length; ++i) {
//			Debug.Log ("i: "+i+") "+referencedAssemblies[i].GetName().Name);
			for(int j = 0; j < assemblyNames.Length; ++j) {
				if(referencedAssemblies[i].GetName().Name == assemblyNames[j]) {
					types.AddMany(referencedAssemblies[i].GetTypes());
				}
			}
		}		
		
		return (
			from System.Type typeX 
			in types 
			where typeX.IsSubclassOf(typeof(UnityEngine.ScriptableObject)) 
			where !typeX.IsSubclassOf(typeof(EditorWindow)) 
			where !typeX.IsSubclassOf(typeof(Editor)) 
			select typeX)
			.ToArray();
	}
	
	
	/// <summary>
	/// Gets the type, using a custom set of assembly names.
	/// </summary>
	/// <returns>The type.</returns>
	/// <param name="TypeName">Type name.</param>
	public Type GetType( string TypeName, string[] _assemblyNames )
	{
		// Try Type.GetType() first. This will work with types defined
		// by the Mono runtime, in the same assembly as the caller, etc.
		var type = Type.GetType( TypeName );
		
		// If it worked, then we're done here
		if( type != null )
			return type;
			
		Assembly[] referencedAssemblies = System.AppDomain.CurrentDomain.GetAssemblies();
		for(int i = 0; i < referencedAssemblies.Length; ++i) {
			for(int j = 0; j < assemblyNames.Length; ++j) {
				if(referencedAssemblies[i].GetName().Name == assemblyNames[j]) {
					type = referencedAssemblies[i].GetType( TypeName );
					if( type != null )
						return type;
				}
			}
		}
		
		
		// If the TypeName is a full name, then we can try loading the defining assembly directly
		if( TypeName.Contains( "." ) )
		{
			
			// Get the name of the assembly (Assumption is that we are using 
			// fully-qualified type names)
			var assemblyName = TypeName.Substring( 0, TypeName.IndexOf( '.' ) );
			
			// Attempt to load the indicated Assembly
			var assembly = Assembly.Load( assemblyName );
			if( assembly == null )
				return null;
			
			// Ask that assembly to return the proper Type
			type = assembly.GetType( TypeName );
			if( type != null )
				return type;
			
		}
		
		// If we still haven't found the proper type, we can enumerate all of the 
		// loaded assemblies and see if any of them define the type
//		var currentAssembly = Assembly.GetExecutingAssembly();
//		var referencedAssemblies = currentAssembly.GetReferencedAssemblies();
//		foreach( var assemblyName in referencedAssemblies )
//		{
//			
//			// Load the referenced assembly
//			var assembly = Assembly.Load( assemblyName );
//			if( assembly != null )
//			{
//				// See if that assembly defines the named type
//				type = assembly.GetType( TypeName );
//				if( type != null )
//					return type;
//			}
//		}
//		
		
		
//		Assembly[] referencedAssemblies = System.AppDomain.CurrentDomain.GetAssemblies();
//		for(int i = 0; i < referencedAssemblies.Length; ++i) {
//			for(int j = 0; j < assemblyNames.Length; ++j) {
//				if(referencedAssemblies[i].GetName().Name == assemblyNames[j]) {
//					type = referencedAssemblies[i].GetType( TypeName );
//					if( type != null )
//						return type;
//				}
//			}
//		}
		
		// The type just couldn't be found...
		return null;
	}
}

#endif