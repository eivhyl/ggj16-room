﻿// FROM: http://wiki.unity3d.com/index.php?title=CountLines

using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Text;
public class CountLines: EditorWindow  {
	
	static string strDir {
		get {
			return System.IO.Directory.GetCurrentDirectory() + @"/Assets";
		}
	}
	
	static List<string> statStrings;
	
	StringBuilder strStats;
	Vector2 scrollPosition = new Vector2(0,0);
	struct File {
		public string name;
		public int lineCount;
		public int wordCount;
		public int characterCount;
		
		public File(string name, int lineCount, int wordCount, int characterCount) {
			this.name 			= name;
			this.lineCount	 	= lineCount;
			this.wordCount 		= wordCount;
			this.characterCount = characterCount;
		}
	}
	
	void OnGUI() {	
		if (GUILayout.Button("Refresh")) {
			Refresh();
		}
		if(!statStrings.IsNullOrEmpty()) {
			scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
			foreach(string s in statStrings) {
				EditorGUILayout.HelpBox(s, MessageType.None);
			}
			EditorGUILayout.EndScrollView();
		}
	}
	
	static void Refresh () {
		List<File> stats = GetStats(strDir);
		statStrings = new List<string>();
		
		if(!stats.IsNullOrEmpty()) {
			int lineCount = 0;
			int wordCount = 0;
			int characterCount = 0;
			foreach(File f in stats) {
				lineCount += f.lineCount;
				wordCount += f.wordCount;
				characterCount += f.characterCount;
			}
			
			System.Text.StringBuilder strStats = new System.Text.StringBuilder();
			strStats.Append("Number of Files: " + stats.Count + "\n");		
			strStats.Append("Number of Lines: " + lineCount + "\n");
			strStats.Append("Number of Words: " + wordCount + "\n");
			strStats.Append("Number of Characters: " + characterCount + "\n");
			statStrings.Add(strStats.ToString());
			
			int iLengthOfRootPath = strDir.Length;
			foreach(File f in stats) {
				statStrings.Add(f.name.Substring(iLengthOfRootPath+1, f.name.Length-iLengthOfRootPath-1) + " --> " + f.lineCount);
			}
		}
	}
	
	
	[MenuItem("UnityX/Stats/Count Lines")]
	public static void Init() {
		CountLines window = EditorWindow.GetWindow<CountLines>("Count Lines");
		window.Show();
		window.Focus();
		Refresh();
	}
	
	static List<File> GetStats (string directory) {
		List<File> stats = new List<File>();
		ProcessDirectory(stats, directory);
		return stats;
	}
	
	static void ProcessDirectory(List<File> stats, string dir) {	
		string[] strArrFiles = System.IO.Directory.GetFiles(dir, "*.cs");
		foreach (string strFileName in strArrFiles)
			ProcessFile(stats, strFileName);
		
		strArrFiles = System.IO.Directory.GetFiles(dir, "*.js");
		foreach (string strFileName in strArrFiles)
			ProcessFile(stats, strFileName);
		
		string[] strArrSubDir = System.IO.Directory.GetDirectories(dir);
		foreach (string strSubDir in strArrSubDir)
			ProcessDirectory(stats, strSubDir);
	}
	
	static void ProcessFile(List<File> stats, string filename) {
		System.IO.StreamReader reader = System.IO.File.OpenText(filename);
		int lineCount = 0;
		int wordCount = 0;
		int characterCount = 0;
		while (reader.Peek() >= 0) {
			string line = reader.ReadLine();
			wordCount += line.Split(' ').Length;
			characterCount += line.Length;
			++lineCount;
		}
		stats.Add(new File(filename, lineCount, wordCount, characterCount));
		reader.Close();			
	}	
}