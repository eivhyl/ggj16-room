﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Rotorz.ReorderableList;

namespace UnityX.SceneManagement {
	
	[CustomEditor(typeof(CustomSceneSettings))]
	public class CustomSceneSettingsEditor: BaseEditor<CustomSceneSettings>  {

		public override void OnInspectorGUI () {
			EditorGUILayout.HelpBox("Add Custom Scene Load Events Here.", MessageType.Info);
			data.defaultSettings = EditorGUILayout.ObjectField ("Default Scene Settings", data.defaultSettings, typeof(CustomSceneSettingsProperties), false) as CustomSceneSettingsProperties;
			ReorderableListGUI.Title("Custom Scene Settings");
			ReorderableListGUI.ListField(data.scenes, CustomListItem);
		}
		
		private CustomSceneSettingsProperties CustomListItem(Rect position, CustomSceneSettingsProperties itemValue) {
			Rect r = new Rect(position);
			r.height = EditorGUIUtility.singleLineHeight;
			r.y += (position.height - r.height) * 0.5f;
			return EditorGUI.ObjectField(r, itemValue, typeof(CustomSceneSettingsProperties), true) as CustomSceneSettingsProperties;
		}
	}
	//		
//		[MenuItem("Edit/Project Settings/Scene Settings")]
//		public static void Init() {
//			EditorWindow.GetWindow<SceneSettingsManagerWindow>("Scene Settings");
//		}
//		
//	public class SceneSettingsManagerWindow: EditorWindow  {
//		
//		[MenuItem("Edit/Project Settings/Scene Settings")]
//		public static void Init() {
//			EditorWindow.GetWindow<SceneSettingsManagerWindow>("Scene Settings");
//		}
//		
//		void OnGUI () {
//			EditorGUILayout.HelpBox("Add Custom Scene Load Events Here.", MessageType.Info);
//			SceneSettingsContainer settings = SceneSettingsManager.settings;
//			if(settings == null) {
//				EditorGUILayout.HelpBox("No Scene Settings Found! This is a bug.", MessageType.Error);
//				return;
//			}
//			EditorGUILayout.ObjectField ("Default Scene Settings", settings.defaultSettings, typeof(SceneSettingsContainer), false);
//			ReorderableListGUI.Title("Custom Scene Settings");
//			ReorderableListGUI.ListField(settings.scenes, CustomListItem);
//		}
//		
//		private SceneSettings CustomListItem(Rect position, SceneSettings itemValue) {
//			Rect r = new Rect(position);
//			r.height = EditorGUIUtility.singleLineHeight;
//			r.y += (position.height - r.height) * 0.5f;
//			return EditorGUI.ObjectField(r, itemValue, typeof(SceneSettings), true) as SceneSettings;
//		}
//	}
}