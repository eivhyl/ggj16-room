﻿public enum UnityEditorPlayModeState {
	Stopped,
	Playing,
	Paused
}