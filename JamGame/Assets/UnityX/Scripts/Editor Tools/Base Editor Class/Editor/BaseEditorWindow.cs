﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public abstract class BaseEditorWindow : EditorWindow
{
	public Dictionary<EventType, Action> EventMap { get; set; }

	/// <summary>
	/// Initializes a new instance of the <see cref="ExtendedEditorWindow"/> class.
	/// </summary>
	protected BaseEditorWindow()
	{
		this.EventMap = new Dictionary<EventType, Action>
		{
			{ EventType.ContextClick, this.OnContext },
			{ EventType.Layout, this.OnLayout },
			{ EventType.Repaint, this.OnRepaint },

			{ EventType.KeyDown, () => {
				this.OnKeyDown(new Keyboard(Event.current));
			}},

			{ EventType.KeyUp, () => {
				this.OnKeyUp(new Keyboard(Event.current));
			}},

			{ EventType.MouseDown, () => {
				this.OnMouseDown((MouseButton)Event.current.button, Event.current.mousePosition);
			}},

			{ EventType.MouseDrag, () => {
				this.OnMouseDrag((MouseButton)Event.current.button, Event.current.mousePosition, Event.current.delta);
			}},

			{ EventType.MouseMove, () => {
				this.OnMouseMove(Event.current.mousePosition, Event.current.delta);
			}},

			{ EventType.ScrollWheel, () => {
				this.OnScrollWheel(Event.current.delta);
			}}
		};
	}

	protected virtual void OnGUI()
	{
		var controlId =
			GUIUtility.GetControlID(FocusType.Passive);

		var controlEvent =
			Event.current.GetTypeForControl(controlId);

		if(this.EventMap.ContainsKey(controlEvent))
		{
			this.EventMap[controlEvent].Invoke();
		}
	}

	protected void OnKeyDown(Keyboard keyboard)
	{
	}

	protected void OnKeyUp(Keyboard keyboard)
	{
	}

	protected virtual void OnMouseDown(MouseButton button, Vector2 position)
	{
	}

	protected virtual void OnMouseDrag(MouseButton button, Vector2 position, Vector2 delta)
	{
	}

	protected virtual void OnMouseMove(Vector2 position, Vector2 delta)
	{
	}
	
	protected virtual void OnContext()
	{
	}

	protected virtual void OnLayout()
	{
	}

	protected virtual void OnRepaint()
	{
	}

	protected virtual void OnScrollWheel(Vector2 delta)
	{
	}
}

/// <summary>
/// Represents the keyboard device.
/// </summary>
public class Keyboard
{
	/// <summary>
	/// Initializes a new instance of the <see cref="Keyboard"/> class.
	/// </summary>
	public Keyboard()
	{	
	}

	/// <summary>
	/// Initializes a new instance of the <see cref="Keyboard"/> class,
	/// from the provided event <paramref name="evt"/>.
	/// </summary>
	/// <param name="evt">An <see cref="Event"/> instance.</param>
	public Keyboard(Event evt)
	{
		this.Code = evt.keyCode;
		this.IsAlt = evt.alt;
		this.IsCapsLock = evt.capsLock;
		this.IsControl = evt.control;
		this.IsFunctionKey = evt.functionKey;
		this.IsNumeric = evt.numeric;
		this.IsShift = evt.shift;
		this.Modifiers = evt.modifiers;
	}

	public KeyCode Code { get; set; }

	/// <summary>
	/// Gets or sets a value indicating whether the alt key is pressed.
	/// </summary>
	/// <value><c>true</c> if the alt key is pressed; otherwise, <c>false</c>.</value>
	public bool IsAlt { get; set; }

	/// <summary>
	/// Gets or sets a value indicating whether the caps lock key is pressed.
	/// </summary>
	/// <value><c>true</c> if the caps lock key is pressed; otherwise, <c>false</c>.</value>
	public bool IsCapsLock { get; set; }

	/// <summary>
	/// Gets or sets a value indicating whether the control key is pressed.
	/// </summary>
	/// <value><c>true</c> if the control key is pressed; otherwise, <c>false</c>.</value>
	public bool IsControl { get; set; }

	/// <summary>
	/// Gets or sets a value indicating whether a function key is pressed.
	/// </summary>
	/// <value><c>true</c> if a function key is pressed; otherwise, <c>false</c>.</value>
	public bool IsFunctionKey { get; set; }

	/// <summary>
	/// Gets or sets a value indicating whether a numeric key is pressed.
	/// </summary>
	/// <value><c>true</c> if a numeric key is pressed; otherwise, <c>false</c>.</value>
	public bool IsNumeric { get; set; }

	/// <summary>
	/// Gets or sets a value indicating whether the control key is pressed.
	/// </summary>
	/// <value><c>true</c> if the control key is pressed; otherwise, <c>false</c>.</value>
	public bool IsShift { get; set; }

	/// <summary>
	/// Gets or sets the modifier keys that are pressed.
	/// </summary>
	/// <value>A combination of <see cref="EventModifiers"/> values.</value>
	public EventModifiers Modifiers { get; set; }
}


/// <summary>
/// Defines values that specify the buttons on a mouse device.
/// </summary>
public enum MouseButton
{
	/// <summary>
	/// The left mouse button.
	/// </summary>
	Left = 0,

	/// <summary>
	/// The right mouse button.
	/// </summary>
	Right = 1,

	/// <summary>
	/// The middle mouse button.
	/// </summary>
	Middle = 2
}