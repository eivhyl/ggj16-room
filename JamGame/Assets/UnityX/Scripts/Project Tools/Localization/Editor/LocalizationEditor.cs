﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Localization))]
public class LocalizationEditor : BaseEditor<Localization> {
	
	public override void OnInspectorGUI() {
		EditorGUILayout.HelpBox("Current Language: "+Localization.language.ToString(), MessageType.None);
		
		Language defaultLanguage = (Language)EditorGUILayout.EnumPopup(new GUIContent("Default Language", "Default Language from the supported languages pool"), Localization.defaultLanguage);
		if(defaultLanguage != Localization.defaultLanguage) Localization.defaultLanguage = defaultLanguage;
		
		EditorUtility.SetDirty(target);
	}
}
