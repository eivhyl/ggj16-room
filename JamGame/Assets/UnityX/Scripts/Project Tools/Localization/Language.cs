﻿/// <summary>
/// The languages supported by the game
/// </summary>
public enum Language {
	English,
	French,
	German,
	Chinese
}