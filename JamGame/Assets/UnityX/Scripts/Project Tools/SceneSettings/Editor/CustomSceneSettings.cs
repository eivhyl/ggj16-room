using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

namespace UnityX.SceneManagement {
	
	public class CustomSceneSettings : ScriptableObject {
		public CustomSceneSettingsProperties defaultSettings;
		public List<CustomSceneSettingsProperties> scenes;
//		
//		[MenuItem("Edit/Project Settings/Scene Settings")]
//		public void Show () {
//			Selection.activeObject = this;
//		}
	}
}