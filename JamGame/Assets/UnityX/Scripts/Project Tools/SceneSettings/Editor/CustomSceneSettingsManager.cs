﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace UnityX.SceneManagement {
	[InitializeOnLoad]
	public static class CustomSceneSettingsManager {
		
		[MenuItem("Edit/Project Settings/Scene Settings")]
		public static void Show () {
			Selection.activeObject = settings;
		}
		
		public static string cachedSettingsPath;
		
		private static CustomSceneSettings _settings;
		public static CustomSceneSettings settings {
			get {
				if(_settings == null) _settings = FindOrCreateSettings();
				return _settings;
			} set {
				_settings = value;
			}
		}
		
		static CustomSceneSettingsManager () {
			DetectEditorSceneManagerChanges.OnChangeActiveScene += OnChangeActiveScene;
			DetectEditorSceneManagerChanges.OnAddScene += OnLoadScene;
			DetectEditorSceneManagerChanges.OnRemoveScene += OnUnloadScene;
			EditorApplication.update += Update;
		}
		
		private static void Update () {
			foreach(Scene scene in SceneManager.GetAllScenes()) {
				CustomSceneSettingsProperties currentSceneSettings = GetSceneSettingsForSceneName(scene.name);
				if(currentSceneSettings != null) currentSceneSettings.OnLevelUpdate();
			}
		}

		static void OnLoadScene (Scene loadedScene) {
			CustomSceneSettingsProperties loadedSceneSettings = GetSceneSettingsForSceneName(loadedScene.name);
			if(loadedSceneSettings != null) loadedSceneSettings.OnLevelLoad();
		}
		
		static void OnUnloadScene (string unloadedSceneName) {
			CustomSceneSettingsProperties unloadedSceneSettings = GetSceneSettingsForSceneName(unloadedSceneName);
			if(unloadedSceneSettings != null) unloadedSceneSettings.OnLevelLoad();
		}

		private static void OnChangeActiveScene (string lastSceneName, Scene currentScene) {
			CustomSceneSettingsProperties lastSceneSettings = GetSceneSettingsForSceneName(lastSceneName);
			if(lastSceneSettings != null) lastSceneSettings.OnUnsetAsActive();

			CustomSceneSettingsProperties currentSceneSettings = GetSceneSettingsForSceneName(currentScene.name);
			if(currentSceneSettings != null) currentSceneSettings.OnSetAsActive();
		}
		
		private static CustomSceneSettings FindOrCreateSettings () {
			CustomSceneSettings tmpSettings = null;
			// Try to create from our cached path.
			if(!cachedSettingsPath.IsNullOrEmpty()) {
				tmpSettings = AssetDatabase.LoadAssetAtPath<CustomSceneSettings>(cachedSettingsPath);
			}
			// If we didn't have a path or the path was invalid, try to find the asset in the project
			if(tmpSettings == null) {
				tmpSettings = FindSettingsInProject();
				// If we couldn't find the asset in the project, create a new one.
				if(tmpSettings == null) {
//					tmpSettings = CreateSettings ();
				}
			}
			return tmpSettings;
		}
		
		private static CustomSceneSettings CreateSettings () {
			return ScriptableObjectUtility.CreateAsset<CustomSceneSettings>(typeof(CustomSceneSettings).Name);
		}
		
		private static CustomSceneSettings FindSettingsInProject () {
			// This stopped working for some reason!
//			string[] GUIDs = AssetDatabase.FindAssets("t:"+typeof(CustomSceneSettings).Name);
//			Debug.Log(GUIDs.IsNullOrEmpty());
//			if(GUIDs.IsNullOrEmpty()) return null;
//			string path = AssetDatabase.GUIDToAssetPath(GUIDs[0]);
//			CustomSceneSettings tmpSettings = AssetDatabase.LoadAssetAtPath<CustomSceneSettings>(path);

			CustomSceneSettings tmpSettings = Resources.FindObjectsOfTypeAll<CustomSceneSettings>().FirstOrDefault();
			if(tmpSettings != null) {
				cachedSettingsPath = AssetDatabase.GetAssetPath(tmpSettings);
			}
			return tmpSettings;
		}
		
		private static CustomSceneSettingsProperties GetSceneSettingsForSceneName (string currentSceneName) {
			if(settings == null) return null;
			if(currentSceneName == null || settings.scenes.IsNullOrEmpty()) return settings.defaultSettings;
			CustomSceneSettingsProperties sceneSettings = settings.scenes.Where(scene => scene != null && scene.sceneNames.Contains(currentSceneName)).FirstOrDefault();
			if(sceneSettings != null) return sceneSettings;
			else return settings.defaultSettings;
		}
		
		private static void DeleteImposterSettingContainers () {
			string[] GUIDs = AssetDatabase.FindAssets("t:"+typeof(CustomSceneSettings).Name);
			if(GUIDs.IsNullOrEmpty() || GUIDs.Length == 1) return;
			SettingsSorter[] settingsSorters = new SettingsSorter[GUIDs.Length];
			for(int i = 0; i < settingsSorters.Length; i++) {
				settingsSorters[i] = new SettingsSorter();
				settingsSorters[i].GUID = GUIDs[i];
				settingsSorters[i].path = AssetDatabase.GUIDToAssetPath(GUIDs[i]);
				settingsSorters[i].settings = AssetDatabase.LoadAssetAtPath<CustomSceneSettings>(settingsSorters[i].path);
			}
			settingsSorters = settingsSorters.OrderBy(o => File.GetCreationTime(o.path)).ToArray();
			for(int i = settingsSorters.Length-1; i > 0; i--) {
				FileUtil.DeleteFileOrDirectory(settingsSorters[i].path);
				FileUtil.DeleteFileOrDirectory(AssetDatabase.GetTextMetaFilePathFromAssetPath(settingsSorters[i].path));
			}
			AssetDatabase.Refresh();
		}
		
		private class SettingsSorter {
			public string GUID;
			public string path;
			public CustomSceneSettings settings;
		}
	}
}