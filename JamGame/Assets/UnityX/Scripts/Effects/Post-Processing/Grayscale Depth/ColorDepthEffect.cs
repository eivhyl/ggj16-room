using UnityEngine;
using System.Collections;

//so that we can see changes we make without having to run the game

[ExecuteInEditMode]
[RequireComponent (typeof(Camera))]
[AddComponentMenu ("UnityX/Image Effects/Camera/Color Depth Effect") ]
public class ColorDepthEffect : MonoBehaviour {
	
	public Color color = Color.white;
	public Color fadeColor = Color.black;
	public float fadeDepth = 10.0f;
	
	public Shader depthShader;
	private Material depthMaterial;

	void OnEnable () {
		GetComponent<Camera>().depthTextureMode |= DepthTextureMode.Depth;
		depthMaterial = CreateMaterial (depthShader,depthMaterial);
	}
	
	protected Material CreateMaterial (Shader s, Material m2Create)
	{
		if (!s)
		{
			Debug.Log ("Missing shader in " + ToString ());
			return null;
		}
		
		if (m2Create && (m2Create.shader == s) && (s.isSupported))
			return m2Create;
		
		if (!s.isSupported)
		{
			return null;
		}
		else
		{
			m2Create = new Material (s);
			m2Create.hideFlags = HideFlags.DontSave;
			if (m2Create)
				return m2Create;
			else return null;
		}
	}
	
	void Update () {
		GetComponent<Camera>().farClipPlane = MathX.Clamp0Infinity(fadeDepth);
	}
	
	void OnRenderImage (RenderTexture source, RenderTexture destination){
		if(depthMaterial == null)
		Graphics.Blit(source, destination);
		
		depthMaterial.SetColor ("_Color", color);
		depthMaterial.SetColor ("_FadeColor", fadeColor);
		
		Graphics.Blit(source, destination, depthMaterial);
	}
}