﻿
//Based on shader from https://www.shadertoy.com/view/ldjGzV#


Shader "TV/VHS" {

	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_StaticTex ("Texture", 2D) = "white" {}
		_StaticIntensity ("Static Intensity", float) = 0.1
		_ScreenFlex ("Screen Flex", float) = 0.1
		_NumLines ("Num Lines", float) = 4
		_Scale ("Scale", float) = 1
		_Resolution ("Resolution", vector) = (1,1,1,1)
		_TestVar ("Test Var", float) = 1
	}
		
            
	SubShader {

		Pass {
       		ColorMask 0
       	}
		Tags {"Queue"="Transparent" "RenderType" = "Transparent" }
		Pass {
			LOD 200
				
			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
	        #pragma fragment frag   

			sampler2D _MainTex;
			sampler2D _StaticTex;
			float _StaticIntensity;
			float _NumLines;
			float _ScreenFlex;
			float _Scale;
			float2 _Resolution;
			float _TestVar;

			struct Input {
				
				half4 customColor;
				float3 worldPos;
			};

			float noise(float2 p) {
				float speed = 50;
				float s = tex2D(_StaticTex, float2(1., 2. * cos((_Time * speed))) * (_Time * speed) * 8. + p).x;
				s *= s;
				return s;
			}

			float onOff(float a, float b, float c)
			{	
				float speed = 50;
				float d = 1;
				return step(c, sin((_Time * speed) + a*cos((_Time * speed)*b)));
				//return 1;
			}

			float ramp(float y, float start, float end)
			{
				float inside = step(start,y) - step(end,y);
				float fact = (y-start)/(end-start)*inside;
				return (1.-fact) * inside;
			}

			float stripes(float2 uv)
			{
				float speed = 50;
				float height1 = 0.5;
				float height2 = 0.1;
				float noi = noise(uv*float2(0.5,1.) + float2(1.,3.));
				return ramp(mod(uv.y * _NumLines + (_Time * speed) / 2. + sin((_Time * speed) + sin((_Time * speed)*0.63)),1.),height1,height1+height2)*noi;
			}

			float2 getShift(float2 uv)
			{
				float speed = 50;
				float2 look = uv;
				float window = 1./(1.+20.*(look.y-mod((_Time * speed)/4.,1.))*(look.y-mod((_Time * speed)/4.,1.)));
				look.x = look.x + sin(look.y*10. + (_Time * speed))/50.*onOff(4.,4.,.3)*(1.+cos((_Time * speed)*80.))*window;
				float vShift = 0.4*onOff(2.,3.,.9)*(sin((_Time * speed))*sin((_Time * speed)*20.) + (0.5 + 0.1*sin((_Time * speed)*200.)*cos((_Time * speed))));
				look.y = mod(look.y + vShift, 1.);
				return look;
			}

			float3 getVignette(float2 uv) 
			{
				//float start = 3.;
				float vigAmt = 3. + .3 * sin(_Time + 5. * cos(_Time * 5.));
				return (1. - vigAmt*(uv.y - .5)*(uv.y - .5)) * (1. - vigAmt * (uv.x - .5) * (uv.x - .5));
			}

			float2 screenDistort(float2 uv)
			{
				uv -= float2(.5,.5);
				//The 1st and 3rd 1's used to be 1.2, but the difference was marginal, and I couldbt really call it better. 
				//This is a sweet turn off TV image!
				//uv = uv * 100000 * (1. / 100000 + _ScreenFlex * uv.x * uv.x * uv.y * uv.y);
				uv = uv * 1 * (1. / 1 + _ScreenFlex * uv.x * uv.x * uv.y * uv.y);
				uv += float2(.5, .5);
				return uv;
			}

			// Input to vertex shader
	        struct vertexInput {
	            float4 vertex : POSITION;
	            float4 texcoord : TEXCOORD0;
	        };

	        // Input to fragment shader
	        struct vertexOutput {
	            float4 pos : SV_POSITION;
	            float4 position_in_world_space : TEXCOORD0;
	            float4 tex : TEXCOORD1;
	        };
	          
	        // VERTEX SHADER
	        vertexOutput vert(vertexInput input) 
	        {
	            vertexOutput output; 
	            output.pos =  mul(UNITY_MATRIX_MVP, input.vertex);
	            output.position_in_world_space = mul(_Object2World, input.vertex);
	            output.tex = input.texcoord;
	            return output;
	        }
	  
			// FRAGMENT SHADER
	        float4 frag(vertexOutput input) : COLOR 
	        {
	        	vec2 uv = input.tex.xy / _Resolution.xy;
				float vignette = getVignette(uv);
				uv = screenDistort(uv);
				vec2 shift = getShift(uv);
				//float3 video = float3(tex2D(_MainTex, uv));
				float3 video = float3(tex2D(_MainTex, shift));
				
				video += stripes(shift);
				//video += noise(shift*2.)/(0.1/_StaticIntensity);
				
				float _noise = noise(shift*2.);

				if(_noise < 0.5) {
					video /= lerp(1, _noise * 2, _StaticIntensity);
				} else {
					_noise-=0.5;
					video *= lerp(1, _noise * 2, _StaticIntensity);
				}
				

				video *= vignette;
				video *= (12.+mod(shift.y*30.+_Time,1.))/13.;
				
				return vec4(video,1.0);
	        }

			ENDCG
		} // End Pass
	}
}

