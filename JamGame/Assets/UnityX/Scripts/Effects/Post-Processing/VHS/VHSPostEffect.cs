using UnityEngine;

namespace UnityX.ImageEffects {
 
	[ExecuteInEditMode]
	[RequireComponent(typeof(Camera))]
	public class VHSPostEffect : MonoBehaviour
	{
	    public Shader shader;
	 	
	    public Texture2D staticTexture;
		[Range(0, 1)] public float staticIntensity = 0.1f;
	    [Range(0, 5)] public float screenFlex = 2.0f;
	    [Range(-6, 6)] public float numLines = 3.0f;
	    [Range(-1, 10)] public float testVar = 0.0f;
	
	    private Material _material;
	    protected Material material
	    {
	        get
	        {
	            if (_material == null)
	            {
	                _material = new Material(shader);
	                _material.hideFlags = HideFlags.HideAndDontSave;
	            }
	            return _material;
	        }
	    }
	 
	    private void OnRenderImage(RenderTexture source, RenderTexture destination)
	    {
	        if (shader == null) return;
	        Material mat = material;
	        mat.SetTexture("_StaticTex", staticTexture);
	        mat.SetFloat("_StaticIntensity", staticIntensity);
	        mat.SetFloat("_ScreenFlex", screenFlex);
	        mat.SetFloat("_NumLines", numLines);
	        mat.SetFloat("_TestVar", testVar);
	        Graphics.Blit(source, destination, mat);
	    }
	 
	    void OnDisable()
	    {
	        if (_material)
	        {
	            DestroyImmediate(_material);
	        }
	    }
	}
}