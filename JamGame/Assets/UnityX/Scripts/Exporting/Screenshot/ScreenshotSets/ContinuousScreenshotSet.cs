﻿using UnityEngine;
using System.Collections;

public class ContinuousScreenshotSet : ScreenshotSet {
	public int frameRate = 24;
	private float captureInterval = 0.2f;
	public bool captureForever = false;
	public float captureTime = 10f;

	//private float burstTime = 0;
	private float timer = 0;

	public override void Begin(){
		captureInterval = 1f/frameRate;
		base.Begin();
	}

	public override void Loop(){
		if(!capturing)return;
		
		StartCoroutine(UpdateCR());

		base.Loop();
	}

	public IEnumerator UpdateCR(){
		timer += Time.deltaTime;
		int currentShot = Mathf.CeilToInt(timer/captureInterval);
		if(currentShot > numScreenshots) {
			yield return StartCoroutine(CreateScreenshotCR());
		}

		if(!captureForever && timer >= captureTime){
			EndScreenshotSet(exportWhenDone);
		}
		yield return null;
	}

	public override void EndScreenshotSet (bool export) {
		base.EndScreenshotSet(export);
	}
}
