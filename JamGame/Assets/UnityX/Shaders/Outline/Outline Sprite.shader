﻿Shader "UnityX/Outline/Outline Sprite" {
	Properties {
		[PerRendererData] _MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	    _GlowColor ("Glow Color", Color) = (1,1,1,1)
	    _GlowIntensity ("Glow", Range(0,1)) = 0.5

	    _OutlineColor ("Outline Color", Color) = (1,1,1,1)
	    _OutlineSize ("Outline Size", Range(0,1)) = 1
	}
	
	SubShader {
	    Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	    LOD 200
	     
		CGPROGRAM
		#pragma surface surf Lambert alpha
		 
		sampler2D _MainTex;
		
		fixed4 _GlowColor;
		fixed _GlowIntensity;
		fixed4 _OutlineColor;
		fixed _OutlineSize;

		struct Input {
		    float2 uv_MainTex;
		};
		
		void surf (Input IN, inout SurfaceOutput o) {
			float _GlowStrength = 1+(_GlowIntensity * (_GlowIntensity*0.2));
			float _GlowSize = _GlowIntensity;
			
		    fixed4 glowC = tex2D(_MainTex, IN.uv_MainTex) * _GlowColor;
		    float clampedGlowSize = clamp(glowC.a-(1-_GlowSize),0,1);
		    float glow = clampedGlowSize + ((_GlowStrength*_GlowStrength)-1)*clampedGlowSize;
		    
		    float outlineStrength = 50;
		    fixed4 outlineC = tex2D(_MainTex, IN.uv_MainTex) * _OutlineColor;
		    float clampedOutlineSize = clamp(outlineC.a-(1-_OutlineSize),0,1);
		    float outline = outlineStrength*clampedOutlineSize;
		    
		    float total = outline + glow;
		    float outlineFraction = outline/total;
		    float glowFraction = glow/total;
		    fixed3 outlineColor = lerp(fixed3(0,0,0), outlineC.rgb, outline);
		    fixed3 glowColor = lerp(fixed3(0,0,0), glowC.rgb, glow);
		    o.Albedo = lerp(outlineC.rgb, glowC.rgb, glowFraction);
//		    o.Albedo = outlineColor + glowColor;
		    o.Alpha = glow + outline;
		    //o.Alpha = glowC.a;
		}
		ENDCG
	}
 
	Fallback "Transparent/Cutout/VertexLit"
 }