﻿Shader "UnityX/Reveal/Linear/Color Reveal" {
	Properties {
		_ForegroundColor ("Foreground Color", Color) = (1,1,1,1)
		_BackgroundColor ("Background Color Color", Color) = (0,0,0,0)
		_Cutoff("Cutoff", Range(0.0,1.0)) = 1
	}
	 
	SubShader {
		Tags { "Queue" = "Transparent" } 
			// draw after all opaque geometry has been drawn
		Pass {
			ZWrite Off // don't write to depth buffer 
				// in order not to occlude other objects
 
			Blend SrcAlpha OneMinusSrcAlpha // use alpha blending
 
			CGPROGRAM 
 
			#pragma vertex vert_img
			#pragma fragment frag

			#include "UnityCG.cginc"

			float4 _ForegroundColor;
            float4 _BackgroundColor;
            fixed _Cutoff;

 
			float4 vert(float4 vertexPos : POSITION) : SV_POSITION {
				return mul(UNITY_MATRIX_MVP, vertexPos);
			}
 
			float4 frag(v2f_img i) : COLOR {
				return float4(i.uv.x > _Cutoff ? _BackgroundColor.rgba : _ForegroundColor.rgba);
			}
 
			ENDCG  
		}
	}
}