Music by TZer0
Check http://underhound.eu/ and https://soundcloud.com/tzer0/ for more cheese.

DAW: Renoise 3.1 or newer (free demo available, costs money for full version)
Synths: DCAM Synth Squad (unavailable for purchase, sorry - replace the one
instrument with a cheesy base) and Strobe 2 (costs money) by fxpansion and Jupiter-8 V2 by (costs money) Arturia.
VST effects: TesslaSE and epicVerb by Variety of Sound (both free)
